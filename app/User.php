<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'mobile','isdCode', 'picture', 'password', 'device_type','device_token','login_by', 'payment_mode',
        'social_unique_id','device_id','wallet_balance','is_student','businessName'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at'
    ];
    public function scopeUserDeviceDetailsIos($query)
    {
        return $query->select('users.id','users.device_token','users.device_id')
                    ->where('users.device_type', '=', 'ios');
    }
    public function scopeUserDeviceDetailsAndroid($query)
    {
        return $query->select('users.id','users.device_token','users.device_id')
                    ->where('users.device_type', '=', 'android');
    }
}
