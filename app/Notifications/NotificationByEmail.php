<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NotificationByEmail extends Notification
{
    use Queueable;

    protected $email_notification1;
    protected $email_notification2;
    protected $email_notification3;
    protected $email_subject;
   
    public function __construct($msg1,$msg2,$msg3,$sub)
    {
        $this->email_notification1 = $msg1;
        $this->email_notification2 = $msg2;
        $this->email_notification3 = $msg3;
        $this->email_subject = $sub;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject($this->email_subject)
                    ->line($this->email_notification1)
                    ->line($this->email_notification2)
                    ->line($this->email_notification3)
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
