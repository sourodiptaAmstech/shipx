<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderWallet extends Model
{
    //

/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'provider_id', 'valid','validation_start','validation_end', 'amount','days','plan_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * The wallets that belong to the user.
     */
    public function provider()
    {
        return $this->belongsTo('App\Provider');
    }

    /**
     * The wallets that belong to the plan.
     */

     public function plan(){
         return $this->belongsTo('App\WalletPlan');
     }

}
