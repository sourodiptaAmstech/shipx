<?php

namespace App\Http\Controllers\Resource;

use App\Locationfee;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Setting;

class LocationfeeResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = LocationFee::orderBy('created_at' , 'desc')->get();
        return view('admin.locationfee.index', compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.locationfee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        
        $this->validate($request, [
            'location_name' => 'required',
            'location_desc' => 'required',
            'fee' => 'required|numeric',
            'latitude' => 'required|numeric|unique:locationfees,latitude',
            'longitude' => 'required|numeric|unique:locationfees,longitude',
            'place_id' => 'required|unique:locationfees,place_id',
        ]);

        try{

            LocationFee::create($request->all());
            return redirect()->route('admin.locationfee.index')->with('flash_success','Location fee Saved Successfully');

        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Location fee Not Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Locationfee  $Locationfee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return LocationFee::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Locationfee  $Locationfee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $location = LocationFee::findOrFail($id);
            return view('admin.locationfee.edit',compact('location'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Locationfee  $Locationfee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'location_name' => 'required',
            'location_desc' => 'required',
            'fee' => 'required|numeric',
            'latitude' => 'required|numeric|unique:locationfees,latitude',
            'longitude' => 'required|numeric|unique:locationfees,longitude',
            'place_id' => 'required|unique:locationfees,place_id',
        ]);

        try {
            LocationFee::where('id',$id)->update([
                    'location_name' => $request->location_name,
                    'location_desc' => $request->location_desc,
                    'fee' => $request->fee,
                    'latitude' => $request->latitude,
                    'longitude' => $request->longitude,
                    'place_id' => $request->place_id,
                ]);
            return redirect()->route('admin.locationfee.index')->with('flash_success', 'Location fee Updated Successfully');    
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'Location fee Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Locationfee  $Locationfee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        try {
            LocationFee::find($id)->delete();
            return back()->with('message', 'Location fee deleted successfully');
        } 
        catch (Exception $e) {
            return back()->with('flash_error', 'Location fee Not Found');
        }
    }
}
