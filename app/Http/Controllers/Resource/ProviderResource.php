<?php

namespace App\Http\Controllers\Resource;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Storage;
use DB;
use Exception;
use Setting;
//use Storage;
use Auth;

use App\Document;
use App\ProviderDocument;
use App\Provider;
use App\UserRequestPayment;
use App\UserRequests;
use App\Helpers\Helper;
use App\Helper\StripeHelper;

class ProviderResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $AllProviders = Provider::with('service','accepted','cancelled')
                    ->orderBy('id', 'DESC');
                   

        if(request()->has('fleet')){
            $providers = $AllProviders->where('fleet',$request->fleet)->get();
        }else{
            $providers = $AllProviders->get();
        }
      
                    
        return view('admin.providers.index', compact('providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        return view('admin.providers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
         if(Setting::get('demo_mode', 0) == 1) {
             return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@shipx.com');
         }
         $this->validate($request, [
             'first_name' => 'required|max:255',
             'last_name' => 'required|max:255',
             'email' => 'required|unique:providers,email|email|max:255',
             'mobile' => 'digits_between:6,13',
             'avatar' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
             'password' => 'required|min:6|confirmed',
         ]);
         try{
             $provider = $request->all();
             $provider['password'] = bcrypt($request->password);
             if($request->hasFile('avatar')) {
                 $provider['avatar'] = $request->avatar->store('public/provider/profile');
                 $provider['avatar']=str_replace("public/", "", $provider['avatar']);                 
             }
             $provider = Provider::create($provider);
             $License = ProviderDocument::where('provider_id', $provider['id'])
             ->where('document_id', 1)
             ->firstOrFail();
             if(isset($request->dl_front_url) && !empty($request->dl_front_url)){
                 $front_url = $request->dl_front_url->store('public/provider/documents');
                 $front_url=str_replace("public/", "", $front_url);
                 $License->update([
                     'front_url' => $front_url,
                     'status' => 'ASSESSING'
                     ]);
             }
             if(isset($request->dl_back_url) && !empty($request->dl_back_url)){
                 $front_url = $request->dl_back_url->store('public/provider/documents');
                 $front_url=str_replace("public/", "", $front_url);
                 $License->update([
                     'back_url' => $front_url,
                     'status' => 'ASSESSING'
                     ]);
             }
             $RoadCertificate = ProviderDocument::where('provider_id', $provider['id'])
             ->where('document_id', 2)
             ->firstOrFail();
             if(isset($request->road_front_url) && !empty($request->road_front_url)){
                 $front_url = $request->road_front_url->store('public/provider/documents');
                 $front_url=str_replace("public/", "", $front_url);
                 $RoadCertificate->update([
                     'front_url' => $front_url,
                     'status' => 'ASSESSING'
                     ]);
             }
             if(isset($request->road_back_url) && !empty($request->road_back_url)){
                 $front_url = $request->road_back_url->store('public/provider/documents');
                 $front_url=str_replace("public/", "", $front_url);
                 $RoadCertificate->update([
                     'back_url' => $front_url,
                     'status' => 'ASSESSING'
                     ]);
             }
             $Decal = ProviderDocument::where('provider_id', $provider['id'])
             ->where('document_id', 5)
             ->firstOrFail();
             if(isset($request->decal_front_url) && !empty($request->decal_front_url)){
                 $front_url = $request->decal_front_url->store('public/provider/documents');
                 $front_url=str_replace("public/", "", $front_url);
                 $Decal->update([
                     'front_url' => $front_url,
                     'status' => 'ASSESSING'
                 ]);
             }
             if(isset($request->decal_back_url) && !empty($request->decal_back_url)){
                 $front_url = $request->decal_back_url->store('public/provider/documents');
                 $front_url=str_replace("public/", "", $front_url);  
                 $Decal->update([
                     'back_url' => $front_url,
                     'status' => 'ASSESSING'
                 ]);
             }
             $InsuranceProof = ProviderDocument::where('provider_id', $provider['id'])
             ->where('document_id', 6)
             ->firstOrFail();
             if(isset($request->poi_front_url) && !empty($request->poi_front_url)){
                 $front_url = $request->poi_front_url->store('public/provider/documents');
                 $front_url=str_replace("public/", "", $front_url);
                 $InsuranceProof->update([
                     'front_url' => $front_url,
                     'status' => 'ASSESSING'
                 ]);
             }
             if(isset($request->poi_back_url) && !empty($request->poi_back_url)){
                 $front_url = $request->poi_back_url->store('public/provider/documents');
                 $front_url=str_replace("public/", "", $front_url);
                 $InsuranceProof->update([
                     'back_url' => $front_url,
                     'status' => 'ASSESSING'
                     ]);
             }
             
             $ComprehensiveInsurance = ProviderDocument::where('provider_id', $provider['id'])
             ->where('document_id', 9)
             ->firstOrFail();
 
             if(isset($request->ci_front_url) && !empty($request->ci_front_url)){
                 $front_url = $request->ci_front_url->store('public/provider/documents');
                 $front_url=str_replace("public/", "", $front_url);
                 $ComprehensiveInsurance->update([
                     'front_url' => $front_url,
                     'status' => 'ASSESSING'
                 ]);
             }
             if(isset($request->ci_back_url) && !empty($request->ci_back_url)){
                 $front_url = $request->ci_back_url->store('public/provider/documents');
                 $front_url=str_replace("public/", "", $front_url);
                 $ComprehensiveInsurance->update([
                     'back_url' => $front_url,
                     'status' => 'ASSESSING'
                 ]);
             }
             $ThirdParty = ProviderDocument::where('provider_id', $provider['id'])
             ->where('document_id', 10)
             ->firstOrFail();
             
             if(isset($request->tpi_front_url) && !empty($request->tpi_front_url)){
 
                 $front_url = $request->tpi_front_url->store('public/provider/documents');
                 $front_url=str_replace("public/", "", $front_url); 
 
                 $ThirdParty->update([
                     'front_url' => $front_url,
                     'status' => 'ASSESSING'
                 ]);
             }
             if(isset($request->tpi_back_url) && !empty($request->tpi_back_url)){
                 $front_url = $request->tpi_back_url->store('public/provider/documents');
                 $front_url=str_replace("public/", "", $front_url); 
                 $ThirdParty->update([
                     'back_url' => $front_url,
                     'status' => 'ASSESSING'
                 ]);
             }
 
             $VehiclImage = ProviderDocument::where('provider_id', $provider['id'])
             ->where('document_id', 8)
             ->firstOrFail();
 
             if(isset($request->vi_front_url) && !empty($request->vi_front_url)){
 
                 $frontURL=$request->vi_front_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
                 
 
                 $VehiclImage->update([
                     'front_url' => $frontURL,
                     'status' => 'ASSESSING'
                 ]);
             }
 
             return back()->with('flash_success','Provider Details Saved Successfully');
 
         }
         catch (ModelNotFoundException $e) {
             if(isset($request->dl_front_url) && !empty($request->dl_front_url) && isset($request->dl_back_url) && !empty($request->dl_back_url)){
                 $frontURL=$request->dl_front_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
                 $back_url=$request->dl_back_url->store('public/provider/documents');
                 $back_url=str_replace("public/", "", $back_url);
                 ProviderDocument::create([
                     'front_url' => $frontURL,
                     'back_url' => $back_url,
                     'status' => 'ASSESSING',
                     'provider_id' => $provider['id'],
                     'document_id' => 1
                     ]);
             }
             if(isset($request->road_front_url) && !empty($request->road_front_url) && isset($request->road_back_url) && !empty($request->road_back_url)){
                     $frontURL=$request->road_front_url->store('public/provider/documents');
                     $frontURL=str_replace("public/", "", $frontURL);
                     $back_url=$request->road_back_url->store('public/provider/documents');
                     $back_url=str_replace("public/", "", $back_url);
                     ProviderDocument::create([
                         'front_url' => $frontURL,
                         'back_url' => $back_url,
                         'status' => 'ASSESSING',
                         'provider_id' => $provider['id'],
                         'document_id' => 2
                         ]);
             }
             if(isset($request->decal_front_url) && !empty($request->decal_front_url) && isset($request->decal_back_url) && !empty($request->decal_back_url)){
                     $frontURL=$request->decal_front_url->store('public/provider/documents');
                     $frontURL=str_replace("public/", "", $frontURL);
                     $back_url=$request->decal_back_url->store('public/provider/documents');
                     $back_url=str_replace("public/", "", $back_url);
                     ProviderDocument::create([
                         'front_url' => $frontURL,
                         'back_url' => $back_url,
                         'status' => 'ASSESSING',
                         'provider_id' => $provider['id'],
                         'document_id' => 5
                         ]);
             }
             if(isset($request->poi_front_url) && !empty($request->poi_front_url) && isset($request->poi_back_url) && !empty($request->poi_back_url)){
                         $frontURL=$request->poi_front_url->store('public/provider/documents');
                         $frontURL=str_replace("public/", "", $frontURL);
                         $back_url=$request->poi_back_url->store('public/provider/documents');
                         $back_url=str_replace("public/", "", $back_url);
                         ProviderDocument::create([
                             'front_url' => $frontURL,
                             'back_url' => $back_url,
                             'status' => 'ASSESSING',
                             'provider_id' => $provider['id'],
                             'document_id' => 6
                         ]);
             }
             if(isset($request->ci_front_url) && !empty($request->ci_front_url) && isset($request->ci_back_url) && !empty($request->ci_back_url)){
                         $frontURL=$request->ci_front_url->store('public/provider/documents');
                         $frontURL=str_replace("public/", "", $frontURL);
                         $back_url=$request->ci_back_url->store('public/provider/documents');
                         $back_url=str_replace("public/", "", $back_url);
                         ProviderDocument::create([
                             'front_url' => $frontURL,
                             'back_url' => $back_url,
                             'status' => 'ASSESSING',
                             'provider_id' => $provider['id'],
                             'document_id' => 9
                             ]);
             }
             if(isset($request->tpi_front_url) && !empty($request->tpi_front_url) && isset($request->tpi_back_url) && !empty($request->tpi_back_url)){
                         $frontURL=$request->tpi_front_url->store('public/provider/documents');
                         $frontURL=str_replace("public/", "", $frontURL);
                         $back_url=$request->tpi_back_url->store('public/provider/documents');
                         $back_url=str_replace("public/", "", $back_url);
                         ProviderDocument::create([
                             'front_url' => $frontURL,
                             'back_url' => $back_url,
                             'status' => 'ASSESSING',
                             'provider_id' => $provider['id'],
                             'document_id' => 10
                             ]);
             }
             if(isset($request->vi_front_url) && !empty($request->vi_front_url)){
                             $frontURL=$request->vi_front_url->store('public/provider/documents');
                             $frontURL=str_replace("public/", "", $frontURL);
                             ProviderDocument::create([
                                 'front_url' => $frontURL,
                                 'status' => 'ASSESSING',
                                 'provider_id' => $provider['id'],
                                 'document_id' => 8
                                 ]);
             }
             return back()->with('flash_success','Provider Details Saved Successfully');
         }
     }
    /**
     * Display the specified resource.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $provider = Provider::findOrFail($id);
            return view('admin.providers.provider-details', compact('provider'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        

            $Document = ProviderDocument::where('provider_id', $id)
                ->get();

            $Provider = Auth::user();
            $provider = Provider::findOrFail($id);

            if(count($Document) === 0){
                return view('admin.providers.edit',compact('provider'));
            }else{
                
                return view('admin.providers.edit',compact('provider','Document'));
            }
            
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
         
         if(Setting::get('demo_mode', 0) == 1) {
             return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@shipx.com');
         }
 
         $this->validate($request, [
             'first_name' => 'required|max:255',
             'last_name' => 'required|max:255',
             'mobile' => 'digits_between:6,13',
             'avatar' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
         ]);
 
         try {
 
             $provider = Provider::findOrFail($id);
             //var_dump($provider);die;
 
             
 
 
             if($request->hasFile('avatar')) {
                 if($provider->avatar) {
                     Storage::delete($provider->avatar);
                 }
 
                 $frontURL=$request->avatar->store('app/public/provider/documents');
               //  $frontURL=str_replace("app/public/", "", $frontURL);
                 $provider->avatar = $frontURL;                    
             }
 
             $provider->first_name = $request->first_name;
             $provider->last_name = $request->last_name;
             $provider->mobile = $request->mobile;
             $provider->save();
 
 
             $License = ProviderDocument::where('provider_id', $provider['id'])
             ->where('document_id', 1)
             ->firstOrFail();
 
             
 
             if(isset($request->dl_front_url) && !empty($request->dl_front_url)){
 
                 $frontURL=$request->dl_front_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
 
 
                 $License->update([
                     'front_url' => $frontURL,
                     'status' => 'ASSESSING'
                 ]);
 
             }if(isset($request->dl_back_url) && !empty($request->dl_back_url)){
 
                 $frontURL=$request->dl_back_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
 
 
                 $License->update([
                     'back_url' => $frontURL,
                     'status' => 'ASSESSING'
                 ]);
             }
 
 
             $RoadCertificate = ProviderDocument::where('provider_id', $provider['id'])
             ->where('document_id', 2)
             ->firstOrFail();
 
             if(isset($request->road_front_url) && !empty($request->road_front_url)){
 
                 $frontURL=$request->road_front_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
 
                 $RoadCertificate->update([
                     'front_url' => $frontURL,
                     'status' => 'ASSESSING'
                 ]);
 
 
             }if(isset($request->road_back_url) && !empty($request->road_back_url)){
 
                 $frontURL=$request->road_back_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
 
 
                 $RoadCertificate->update([
                     'back_url' => $frontURL,
                     'status' => 'ASSESSING'
                 ]);
             }
 
 
             $Decal = ProviderDocument::where('provider_id', $provider['id'])
             ->where('document_id', 5)
             ->firstOrFail();
 
             if(isset($request->decal_front_url) && !empty($request->decal_front_url)){
 
                 $frontURL=$request->decal_front_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
 
                 $Decal->update([
                     'front_url' => $frontURL,
                     'status' => 'ASSESSING'
                 ]);
             }if(isset($request->decal_back_url) && !empty($request->decal_back_url)){
 
                 $frontURL=$request->decal_back_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
 
                 $Decal->update([
                     'back_url' => $frontURL,
                     'status' => 'ASSESSING'
                 ]);
             }
 
             $InsuranceProof = ProviderDocument::where('provider_id', $provider['id'])
             ->where('document_id', 6)
             ->firstOrFail();
 
             if(isset($request->poi_front_url) && !empty($request->poi_front_url)){
 
                 $frontURL=$request->poi_front_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
 
                 $InsuranceProof->update([
                     'front_url' => $frontURL,
                     'status' => 'ASSESSING'
                 ]);
 
             }if(isset($request->poi_back_url) && !empty($request->poi_back_url)){
 
                 $frontURL=$request->poi_back_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
 
                 $InsuranceProof->update([
                     'back_url' => $frontURL,
                     'status' => 'ASSESSING'
                 ]);
             }
 
 
             $ComprehensiveInsurance = ProviderDocument::where('provider_id', $provider['id'])
             ->where('document_id', 9)
             ->firstOrFail();
 
             if(isset($request->ci_front_url) && !empty($request->ci_front_url)){
 
                 $frontURL=$request->ci_front_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
 
 
                 $ComprehensiveInsurance->update([
                     'front_url' => $frontURL,
                     'status' => 'ASSESSING'
                 ]);
 
             }if(isset($request->ci_back_url) && !empty($request->ci_back_url)){
 
                 $frontURL=$request->ci_back_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
 
                 $ComprehensiveInsurance->update([
                     'back_url' => $frontURL,
                     'status' => 'ASSESSING'
                 ]);
             }
 
 
             $ThirdParty = ProviderDocument::where('provider_id', $provider['id'])
             ->where('document_id', 10)
             ->firstOrFail();
 
             if(isset($request->tpi_front_url) && !empty($request->tpi_front_url)){
 
                 $frontURL=$request->tpi_front_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
                 
                 $ThirdParty->update([
                     'front_url' => $frontURL,
                     'status' => 'ASSESSING'
                 ]);
             
             }if(isset($request->tpi_back_url) && !empty($request->tpi_back_url)){
 
                 $frontURL=$request->tpi_back_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
 
                 $ThirdParty->update([
                     'back_url' => $frontURL,
                     'status' => 'ASSESSING'
                 ]);
             }
 
             $VehiclImage = ProviderDocument::where('provider_id', $provider['id'])
             ->where('document_id', 8)
             ->firstOrFail();
 
             if(isset($request->vi_front_url) && !empty($request->vi_front_url)){
 
                 $frontURL=$request->vi_front_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
 
                 $VehiclImage->update([
                     'front_url' => $frontURL,
                     'status' => 'ASSESSING'
                 ]);
             }
 
             
 
             return redirect()->route('admin.provider.index')->with('flash_success', 'Provider Updated Successfully');    
         } 
 
         catch (ModelNotFoundException $e) {
             if(isset($request->dl_front_url) && !empty($request->dl_front_url) && isset($request->dl_back_url) && !empty($request->dl_back_url)){
                 
                 $frontURL=$request->dl_front_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
                 $back_url=$request->dl_back_url->store('public/provider/documents');
                 $back_url=str_replace("public/", "", $back_url);
 
                 ProviderDocument::create([
                     'front_url' => $frontURL,
                     'back_url' => $back_url,
                     'status' => 'ASSESSING',
                         'provider_id' => $provider['id'],
                         'document_id' => 1
                     ]);
                 
             }
             if(isset($request->road_front_url) && !empty($request->road_front_url) && isset($request->road_back_url) && !empty($request->road_back_url)){
                 
                 $frontURL=$request->road_front_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
                 $back_url=$request->road_back_url->store('public/provider/documents');
                 $back_url=str_replace("public/", "", $back_url);
 
                 ProviderDocument::create([
                     'front_url' => $frontURL,
                     'back_url' => $back_url,
                     'status' => 'ASSESSING',
                     'provider_id' => $provider['id'],
                     'document_id' => 2
                     ]);
                 
             }
 if(isset($request->decal_front_url) && !empty($request->decal_front_url) && isset($request->decal_back_url) && !empty($request->decal_back_url)){
 
     $frontURL=$request->decal_front_url->store('public/provider/documents');
     $frontURL=str_replace("public/", "", $frontURL);
     $back_url=$request->decal_back_url->store('public/provider/documents');
     $back_url=str_replace("public/", "", $back_url);
                 
     ProviderDocument::create([
                         'front_url' => $frontURL,
                         'back_url' => $back_url,
                         'status' => 'ASSESSING',
                         'provider_id' => $provider['id'],
                         'document_id' => 5
                         ]);
             }
 if(isset($request->poi_front_url) && !empty($request->poi_front_url) && isset($request->poi_back_url) && !empty($request->poi_back_url)){
     $frontURL=$request->poi_front_url->store('public/provider/documents');
     $frontURL=str_replace("public/", "", $frontURL);
     $back_url=$request->poi_back_url->store('public/provider/documents');
     $back_url=str_replace("public/", "", $back_url);
 
                         ProviderDocument::create([
                             'front_url' => $frontURL,
                             'back_url' => $back_url,
                             'status' => 'ASSESSING',
                             'provider_id' => $provider['id'],
                             'document_id' => 6
                             ]);
 
             }
             if(isset($request->ci_front_url) && !empty($request->ci_front_url) && isset($request->ci_back_url) && !empty($request->ci_back_url)){
                 $frontURL=$request->ci_front_url->store('public/provider/documents');
                 $frontURL=str_replace("public/", "", $frontURL);
                 $back_url=$request->ci_back_url->store('public/provider/documents');
                 $back_url=str_replace("public/", "", $back_url);
 
                         ProviderDocument::create([
                             'front_url' => $frontURL,
                             'back_url' => $back_url,
                             'status' => 'ASSESSING',
                             'provider_id' => $provider['id'],
                             'document_id' => 9
                         ]);
             }
 if(isset($request->tpi_front_url) && !empty($request->tpi_front_url) && isset($request->tpi_back_url) && !empty($request->tpi_back_url)){
     $frontURL=$request->tpi_front_url->store('public/provider/documents');
     $frontURL=str_replace("public/", "", $frontURL);
 
     $back_url=$request->tpi_back_url->store('public/provider/documents');
     $back_url=str_replace("public/", "", $back_url);
     
     ProviderDocument::create([
         'front_url' => $frontURL,
         'back_url' => $back_url,
         'status' => 'ASSESSING',
         'provider_id' => $provider['id'],
         'document_id' => 10
         ]);
     }
 if(isset($request->vi_front_url) && !empty($request->vi_front_url)){
     $frontURL=$request->vi_front_url->store('public/provider/documents');
     $frontURL=str_replace("public/", "", $frontURL);
     ProviderDocument::create([
         'front_url' => $frontURL,
          'status' => 'ASSESSING',
         'provider_id' => $provider['id'],
         'document_id' => 8
         ]);
             }
             return back()->with('flash_success','Profile Updated Successfully');
         }
     }
 

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }

        try {
            Provider::find($id)->delete();
            return back()->with('message', 'Provider deleted successfully');
        } 
        catch (Exception $e) {
            return back()->with('flash_error', 'Provider Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        try {
            $Provider = Provider::findOrFail($id);
           $stipeKey= Setting::get("stripe_secret_key");
           
           
            if($Provider->vStripeCusId!==""){
                // get stripe status
                $StripeHelper=new StripeHelper();
                $status=$StripeHelper->checkstatus($Provider->vStripeCusId,$stipeKey);
               // print_r($status);exit;
                if(!empty($status)){
                    if(array_key_exists("error",$status)){                       
                        if(array_key_exists("code",$status['error'])){
                            if("account_invalid"===$status['error']['code']){
                                return back()->with('flash_error', "Provider account with stripeis not create/activated yet!.");
                            }
                            return back()->with('flash_error', "Provider account with stripeis not create/activated yet!.");
                        }
                        return back()->with('flash_error', "Provider account with stripeis not create/activated yet!.");
                    }
                    else{
                        if(array_key_exists("individual",$status)){
                            if(array_key_exists("verification",$status['individual'])){
                                if(array_key_exists("status",$status['individual']['verification'])){                                    
                                    if($status['individual']['verification']['status']!=="verified"){
                                        return back()->with('flash_error', "Provider account activation with stripeis ".$status['individual']['verification']['status']);
                                    }
                                    else{
                                        if($status['capabilities']['transfers']==="inactive"){
                                            return back()->with('flash_error', "Provider account with stripeis ".$status['capabilities']['transfers']);
                                        }
                                    }
                                }
                                else{
                                    return back()->with('flash_error', "Provider account with stripeis not create/activated yet!.");
                                }
                            }
                            else{
                                return back()->with('flash_error', "Provider account with stripeis not create/activated yet!.");
                            }
                        }
                        else{
                            return back()->with('flash_error', "Provider account with stripeis not create/activated yet!.");
                        }
                    }
                }
                else{
                    return back()->with('flash_error', "Provider account with stripeis not create/activated yet!.");
                }
            }
            else{
                return back()->with('flash_error', "Provider account with stripeis not create/activated yet!.");
            }

            if($Provider->service) {
                $Provider->update(['status' => 'approved']);
                return back()->with('flash_success', "Provider Approved");
            } else {
                return redirect()->route('admin.provider.document.index', $id)->with('flash_error', "Provider has not been assigned a service type!");
            }
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', "Something went wrong! Please try again later.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function disapprove($id)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        
        Provider::where('id',$id)->update(['status' => 'banned']);
        return back()->with('flash_success', "Provider Disapproved");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function request($id){

        try{

            $requests = UserRequests::where('user_requests.provider_id',$id)
                    ->RequestHistory()
                    ->get();

            return view('admin.request.index', compact('requests'));
        } catch (Exception $e) {
            return back()->with('flash_error','Something Went Wrong!');
        }
    }

    /**
     * account statements.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function statement($id){

        try{

            $requests = UserRequests::where('provider_id',$id)
                        ->where('status','COMPLETED')
                        ->with('payment')
                        ->get();

            $rides = UserRequests::where('provider_id',$id)->with('payment')->orderBy('id','desc')->paginate(10);
            $cancel_rides = UserRequests::where('status','CANCELLED')->where('provider_id',$id)->count();
            $Provider = Provider::find($id);
            $revenue = UserRequestPayment::whereHas('request', function($query) use($id) {
                                    $query->where('provider_id', $id );
                                })->select(\DB::raw(
                                   'SUM(ROUND(fixed) + ROUND(distance)) as overall, SUM(ROUND(commision)) as commission' 
                               ))->get();


            $Joined = $Provider->created_at ? '- Joined '.$Provider->created_at->diffForHumans() : '';

            return view('admin.providers.statement', compact('rides','cancel_rides','revenue'))
                        ->with('page',$Provider->first_name."'s Overall Statement ". $Joined);

        } catch (Exception $e) {
            return back()->with('flash_error','Something Went Wrong!');
        }
    }

    public function Accountstatement($id){

        try{

            $requests = UserRequests::where('provider_id',$id)
                        ->where('status','COMPLETED')
                        ->with('payment')
                        ->get();

            $rides = UserRequests::where('provider_id',$id)->with('payment')->orderBy('id','desc')->paginate(10);
            $cancel_rides = UserRequests::where('status','CANCELLED')->where('provider_id',$id)->count();
            $Provider = Provider::find($id);
            $revenue = UserRequestPayment::whereHas('request', function($query) use($id) {
                                    $query->where('provider_id', $id );
                                })->select(\DB::raw(
                                   'SUM(ROUND(fixed) + ROUND(distance)) as overall, SUM(ROUND(commision)) as commission' 
                               ))->get();


            $Joined = $Provider->created_at ? '- Joined '.$Provider->created_at->diffForHumans() : '';

            return view('account.providers.statement', compact('rides','cancel_rides','revenue'))
                        ->with('page',$Provider->first_name."'s Overall Statement ". $Joined);

        } catch (Exception $e) {
            return back()->with('flash_error','Something Went Wrong!');
        }
    }
}
