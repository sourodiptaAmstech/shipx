<?php

namespace App\Http\Controllers\Resource;

use App\ProviderWallet;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Exception;
use Setting;
use App\Account;
use App\Provider;
use App\WalletPlan;
use App\ProviderDevice;
use App\ProviderWalletHistory;
use Illuminate\Support\Facades\Auth;

class ProviderWalletResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $wallets = ProviderWallet::orderBy('created_at' , 'desc')->get();
       // print_r($wallets);die;
        return view('admin.wallet.index', compact('wallets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $providersIds = [];
        $providerWithwallet = ProviderWallet::select('provider_id')->orderBy('created_at', 'desc')->get();
        $walletPlans = WalletPlan::orderBy('created_at', 'desc')->get();
        if($providerWithwallet){
            foreach($providerWithwallet as $walletExist){
                $providersIds[] = $walletExist->provider_id;
            }
            $providers = Provider::whereNotIn('id' , $providersIds)->orderBy('created_at' , 'desc')->get();
        }else{
            $providers = Provider::orderBy('created_at' , 'desc')->get();
        }
        
        return view('admin.wallet.create', compact('providers', 'walletPlans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        try{
            $ProviderWallet = $request->all();

            

            // echo '<pre>';
            // print_r($providerdevice->type);
            // echo '</pre>';
            // die;

            

            $Plan = WalletPlan::findOrFail($request->plan_id);
            
            $ProviderWallet['days'] = $Plan->duration;
            $ProviderWallet['validation_end'] = date('Y-m-d', strtotime($ProviderWallet['validation_start']. $Plan->duration));
            $ProviderWallet['amount'] = $Plan->amount;

           // Updating provider's wallet 
           $ProviderWalletExist = ProviderWallet::where('provider_id', $request->provider_id)->first();

           if($ProviderWalletExist){
            return back()->with('flash_error', 'Wallet Already Exist For This Provider');
           }else{
            $ProviderWalletCreate = ProviderWallet::create($ProviderWallet);

           //echo $ProviderWalletCreate->id;die;
            // 'provider_id', 'status','start_date','end_date','duration','plan_id'

            if($ProviderWalletCreate){
                $walletHistory = [];
                $walletHistory['transaction_id'] = $ProviderWalletCreate->id;
                $walletHistory['provider_id'] = $request->provider_id;
                $walletHistory['plan_id'] = $request->plan_id;
                $walletHistory['start_date'] = $ProviderWallet['validation_start'];
                $walletHistory['end_date'] = $ProviderWallet['validation_end'];
                $walletHistory['duration'] = $ProviderWallet['days'];

                $ProviderWalletHistory = ProviderWalletHistory::create($walletHistory);

                // Send Push to driver after recharge is successfull

                $message = \PushNotification::Message('Wallet Recherge Successfull',array(
                    'badge' => 1,
                    'sound' => 'default',
                    'actionLocKey' => 'Shipx Shipment',
                    'locKey' => 'localized key',
                    'locArgs' => array(
                        'localized args',
                        'localized args',
                    ),
                    'launchImage' => 'image.jpg',
                    'description' => 'Your wallet has been recharged successfuly with the amount 54.4 which is valid upto 0ne month',
                    'custom' => array('custom_data' => array(
                        'shipx' => 'ShipxWallet', 'Wallet Recharge'
                    ))
                ));

                $providerdevice = ProviderDevice::where('provider_id', $request->provider_id)->first();
                if($providerdevice->type == 'ios'){
                    $pushToIosProvider = \PushNotification::app('IOSProvider')
                    ->to($providerdevice->token)
                    ->send($message);
                }if($providerdevice->type == 'android'){
                    $pushToAndroidProvider = \PushNotification::app('AndroidProvider')
                    ->to($providerdevice->token)
                    ->send($message);
                }

                

                return redirect()->route('admin.wallet.index')->with('flash_success', 'Wallet Added Successfully');
            }else{
                return back()->with('flash_error', 'Cannot Add Wallet Amount');
            }
           }
        }
        catch (Exception $e) {
            return back()->with('flash_error', $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dispatcher  $account
     * @return \Illuminate\Http\Response
     */

     // This resouce is used to fetch and display providers wallet history
    public function show($id)
    {
       // echo '<pre>'; print_r($id); echo '</pre>'; die;

       $getWallet = ProviderWallet::where('id',$id)->select('provider_id')->first();

       if($getWallet){
           $getHistory = ProviderWalletHistory::where('provider_id',$getWallet->provider_id)->orderBy('created_at', 'desc')->get();

           return view('admin.wallet.wallet_history',compact('getHistory'));
       }else{
        return back()->with('flash_error', 'Wallet Does Not Exist');
       }

       

       

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        try {
            $walletPlans = WalletPlan::orderBy('created_at', 'desc')->get();
            $wallet = ProviderWallet::findOrFail($id);

            $providers = Provider::orderBy('created_at' , 'desc')->get();

            
            return view('admin.wallet.edit',compact('wallet', 'providers','walletPlans'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $Plan = WalletPlan::findOrFail($request->plan_id);

            $Wallet = ProviderWallet::findOrFail($id);
           
            
            $Wallet['days'] = $Plan->duration;
            $Wallet['validation_end'] = date('Y-m-d', strtotime($Wallet['validation_start']. $Plan->duration));
            $Wallet['amount'] = $Plan->amount;
            $Wallet->save();

            return redirect()->route('admin.wallet.index')->with('flash_success', 'Wallet Updated Successfully');    
        } 

        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Account Manager Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Account  $dispatcher
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        

        try {
            ProviderWallet::find($id)->delete();
            return back()->with('message', 'Wallet deleted successfully');
        } 
        catch (Exception $e) {
            return back()->with('flash_error', 'Account Not Found');
        }
    }


}
