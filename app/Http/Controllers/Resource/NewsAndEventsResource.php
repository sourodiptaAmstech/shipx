<?php

namespace App\Http\Controllers\Resource;

use Auth;
use Setting;
use Exception;

use App\Provider;
use Carbon\Carbon;
use App\NewsAndEvents;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SendPushNotification;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class NewsAndEventsResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$newsandevents = NewsAndEvents::orderBy('created_at' , 'desc')->get();
		 
        
        if($request->has('app')) {
			$newsandevents = NewsAndEvents::where('news_type','1')->orderBy('created_at' , 'desc')->get(); 
            return response()->json([
                'newsandevents' => $newsandevents,
            ]);
        } else {
			$newsandevents = NewsAndEvents::orderBy('created_at' , 'desc')->get();
            return view('admin.newsandevents.index', compact('newsandevents'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.newsandevents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        try{
			
             $newsnevent=new NewsAndEvents();
             $newsnevent->title=$request->input('title');
             $newsnevent->description=$request->input('description');
             if ($request->input('driver') === 'on' && $request->input('rider') === 'on') {
                 $newsnevent->news_type='2';
            } else  if ($request->input('driver') === 'on'){
                $newsnevent->news_type='1';
            }else if ($request->input('rider') === 'on'){
                $newsnevent->news_type='0';
            }else{
				 return back()->with('flash_error', 'Must be checked either Driver or Requester');
            }
            
              $newsnevent->save(); 

 if ($request->input('driver') === 'on' && $request->input('rider') === 'on') {
                 $allDeviceResponse = (new SendPushNotification)->SendToAll($request);
            } else  if ($request->input('driver') === 'on'){
               $allDeviceResponse = (new SendPushNotification)->SendToDriverAll($request);
            }else if ($request->input('rider') === 'on'){
                $allDeviceResponse = (new SendPushNotification)->SendToRiderAll($request);
            }			  
			  
			 
            if($allDeviceResponse){
                NewsAndEvents::where('id',$newsnevent->id)->update([
                    'status' => "SUCCESS"
                ]);
            }
            
            return redirect('admin/newsandevent')->with('flash_success','News or Event Saved Successfully');
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'News or Event Not Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NewsAndEvent  $NewsAndEvent
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return NewsAndEvents::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NewsAndEvent  $NewsAndEvent
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $newsandevent = NewsAndEvents::findOrFail($id);
            return view('admin.newsandevents.edit',compact('newsandevent'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NewsAndEvent  $NewsAndEvent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        try {
            NewsAndEvents::where('id',$id)->update([
                    'title' => $request->title,
                    'description' => $request->description,
                ]);
            return redirect()->route('admin.newsandevents.index')->with('flash_success', 'News or Event Updated Successfully');    
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'News or Event Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NewsAndEvent  $NewsAndEvent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        try {
            NewsAndEvents::find($id)->delete();
            return back()->with('message', 'News or Event deleted successfully');
        } 
        catch (Exception $e) {
            return back()->with('flash_error', 'News or Event Not Found');
        }
    }
}
