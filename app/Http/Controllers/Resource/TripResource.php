<?php

namespace App\Http\Controllers\Resource;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserRequests;
use Auth;
use Setting;
use App\MultipleWayPoints;

class TripResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $requests = UserRequests::RequestHistory()->get();
            return view('admin.request.index', compact('requests'));
        } catch (Exception $e) {
            return back()->with('flash_error','Something Went Wrong!');
        }
    }

    public function Fleetindex()
    {
        try {
            $requests = UserRequests::RequestHistory()
                        ->whereHas('provider', function($query) {
                            $query->where('fleet', Auth::user()->id );
                        })->get();
            return view('fleet.request.index', compact('requests'));
        } catch (Exception $e) {
            return back()->with('flash_error','Something Went Wrong!');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function scheduled()
    {
        try{
            $requests = UserRequests::where('status' , 'SCHEDULED')
                        ->RequestHistory()
                        ->get();

            return view('admin.request.scheduled', compact('requests'));
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Fleetscheduled()
    {
        try{
            $requests = UserRequests::where('status' , 'SCHEDULED')
                         ->whereHas('provider', function($query) {
                            $query->where('fleet', Auth::user()->id );
                        })
                        ->get();

            return view('fleet.request.scheduled', compact('requests'));
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
                's_latitude' => 'required|numeric',
                'd_latitude' => 'required|numeric',
                's_longitude' => 'required|numeric',
                'd_longitude' => 'required|numeric',
                'service_type' => 'required|numeric|exists:service_types,id',
                'promo_code' => 'exists:promocodes,promo_code',
                'distance' => 'required|numeric',
                'use_wallet' => 'numeric',
                'payment_mode' => 'required|in:CASH,CARD,PAYPAL',
            ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $request = UserRequests::findOrFail($id);
//echo "<pre>";
         //   print_r($request); exit;
           // $MultipleWayPoints = MultipleWayPoints::findOrFail($id);
         //  $MultipleWayPoints=MultipleWayPoints::where('user_requests_id',$id)->get()->toArray();
           $MultipleWayPoints=  DB::select( DB::raw("select multiple_way_points.*,
           parcelDetails.id as parcelDetails_id,
           parcelDetails.location_id as parcelDetails_location_id, 
           parcelDetails.order as parcelDetails_order,
           parcelDetails.recipient_name as parcelDetails_recipient_name, 
           COALESCE(parcelDetails.recipient_phone_no,'') as parcelDetails_recipient_phone_no,
           COALESCE(parcelDetails.no_packages,'') as parcelDetails_no_packages,
           COALESCE(parcelDetails.item_description,'') as parcelDetails_item_description,
           COALESCE(parcelDetails.isSignature,'') as parcelDetails_isSignature,
           COALESCE(parcelDetails.upload_signature_path,'') as upload_signature_path,
           COALESCE(parcelDetails.isDestination,'') as parcelDetails_isDestination,
           COALESCE(parcelDetails.picked_on,'') as parcelDetails_picked_on,
           COALESCE(parcelDetails.delivered_on,'') as parcelDetails_delivered_on,
           COALESCE(parcelDetails.recieved_by,'') as parcelDetails_recieved_by,
           COALESCE(parcelDetails.receiver_relationship,'') as parcelDetails_receiver_relationship,
           COALESCE(parcelDetails.receiver_ph,0) as parcelDetails_receiver_ph ,
           COALESCE(parcelDetails.updated_at,'') as parcelDetails_updated_at,
           COALESCE(parcelDetails.created_at,'') as parcelDetails_created_at
           from multiple_way_points 
           left join parcelDetails  on parcelDetails.location_id=multiple_way_points.id and parcelDetails.isDestination='MULTIPLE_WAYPOINTS'
           where 
           multiple_way_points.user_requests_id = ".$id) );


           $DESTINATIONPoints=  DB::select( DB::raw("select  parcelDetails.id as parcelDetails_id,
                    parcelDetails.location_id as parcelDetails_location_id, 
                    parcelDetails.order as parcelDetails_order,
                    parcelDetails.recipient_name as parcelDetails_recipient_name, 
                    COALESCE(parcelDetails.recipient_phone_no,'') as parcelDetails_recipient_phone_no,
                    COALESCE(parcelDetails.no_packages,'') as parcelDetails_no_packages,
                    COALESCE(parcelDetails.item_description,'') as parcelDetails_item_description,
                    COALESCE(parcelDetails.isSignature,'') as parcelDetails_isSignature,
                    COALESCE(parcelDetails.upload_signature_path,'') as upload_signature_path,
                    COALESCE(parcelDetails.isDestination,'') as parcelDetails_isDestination,
                    COALESCE(parcelDetails.picked_on,'') as parcelDetails_picked_on,
                    COALESCE(parcelDetails.delivered_on,'') as parcelDetails_delivered_on,
                    COALESCE(parcelDetails.recieved_by,'') as parcelDetails_recieved_by,
                    COALESCE(parcelDetails.receiver_relationship,'') as parcelDetails_receiver_relationship,
                    COALESCE(parcelDetails.receiver_ph,0) as parcelDetails_receiver_ph ,
                    COALESCE(parcelDetails.updated_at,'') as parcelDetails_updated_at,
                    COALESCE(parcelDetails.created_at,'') as parcelDetails_created_at from parcelDetails as parcelDetails 
                    where parcelDetails.isDestination='DESTINATION' and parcelDetails.request_id = ".$id) );
                    $json  = json_encode($MultipleWayPoints);
                    $MultipleWayPoints = json_decode($json, true);       
                    $json  = json_encode($DESTINATIONPoints);
                    $DESTINATIONPoints = json_decode($json, true);  
     //   print_r($MultipleWayPoints); exit;



            return view('admin.request.show', compact('request','MultipleWayPoints','DESTINATIONPoints'));
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }

    public function Fleetshow($id)
    {
        try {
            $request = UserRequests::findOrFail($id);
            return view('fleet.request.show', compact('request'));
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }

    public function Accountshow($id)
    {
        try {
            $request = UserRequests::findOrFail($id);
            return view('account.request.show', compact('request'));
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        
        try {
            $Request = UserRequests::findOrFail($id);
            $Request->delete();
            return back()->with('flash_success','Request Deleted!');
        } catch (Exception $e) {
            return back()->with('flash_error','Something Went Wrong!');
        }
    }

    public function Fleetdestroy($id)
    {
        try {
            $Request = UserRequests::findOrFail($id);
            $Request->delete();
            return back()->with('flash_success','Request Deleted!');
        } catch (Exception $e) {
            return back()->with('flash_error','Something Went Wrong!');
        }
    }
}
