<?php

namespace App\Http\Controllers\Resource;

use Auth;
use Setting;
use Exception;

use App\Provider;
use Carbon\Carbon;
use App\AirportInstructionPushs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SendPushNotification;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AirportInstructionPushsResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       
        
        if($request->has('app')) {
            // news_type =0 rider 
            // news_type =1 Driver 
            $airportInstructionPushs = AirportInstructionPushs::getAll()->orderBy('created_at' , 'desc')->get();  
            
            return response()->json([
                'airportInstructionPushs' => $airportInstructionPushs,
            ]);
        } else {
            $airportInstructionPushs = AirportInstructionPushs::orderBy('created_at' , 'desc')->get();
            return view('admin.airportinstructionpushs.index', compact('airportInstructionPushs'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $airportInstructionPushs = AirportInstructionPushs::orderBy('created_at' , 'desc')->get(); 
       
        if(count($airportInstructionPushs) > 0)
         return redirect()->route('admin.airportinstructionpush.index')->with('flash_success', 'Airport Instruction Already Exist.');       
        else
            return view('admin.airportinstructionpushs.create');
         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        try{
            // $request->input('camera_video');
           // $newsnevent = NewsAndEvents::create($request->all());
            
             $airportinstructionpush=new AirportInstructionPushs();
             $airportinstructionpush->title=$request->input('title');
             $airportinstructionpush->description=htmlspecialchars($request->input('description'));                    
             $airportinstructionpush->save();          
                    
            // to do for push
            
          /* $allDeviceResponse = (new SendPushNotification)->SendToRiderAll($request);
            
            if($allDeviceResponse){
                AirportInstructionPushs::where('id',$airportinstructionpush->id)->update([
                    'status' => "SUCCESS"
                ]);
            }  */          
            return redirect('admin/airportinstructionpush')->with('flash_success','News or Event Saved Successfully');
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'News or Event Not Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NewsAndEvent  $NewsAndEvent
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return AirportInstructionPushs::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NewsAndEvent  $NewsAndEvent
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            
            
            $airportinstructionpush = AirportInstructionPushs::findOrFail($id);
            return view('admin.airportinstructionpushs.edit',compact('airportinstructionpush'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NewsAndEvent  $NewsAndEvent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        try {
            AirportInstructionPushs::where('id',$id)->update([
                    'title' => $request->title,
                    'description' => htmlspecialchars($request->input('description')),
                ]);
            
           /*  $allDeviceResponse = (new SendPushNotification)->SendToRiderAll($request);
            
            if($allDeviceResponse){
                AirportInstructionPushs::where('id',$id)->update([
                    'status' => "SUCCESS"
                ]);
            }*/
            return redirect()->route('admin.airportinstructionpush.index')->with('flash_success', 'News or Event Updated Successfully');    
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'News or Event Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NewsAndEvent  $NewsAndEvent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        try {
            AirportInstructionPushs::find($id)->delete();
            return back()->with('message', 'News or Event deleted successfully');
        } 
        catch (Exception $e) {
            return back()->with('flash_error', 'News or Event Not Found');
        }
    }
}
