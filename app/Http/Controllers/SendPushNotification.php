<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ProviderDevice;
use App\ProviderService;
use App\Provider;
use App\UserRequests;
use App\Locationfee;
use Exception;
use Setting;
use App\ServiceType;

class SendPushNotification extends Controller
{
	/**
     * New Ride Accepted by a Driver.
     *
     * @return void
     */
    public function RideAccepted($request){
        //$request = UserRequests::with('user')->findOrFail("626");
        if($request->status = "SCHEDULED"){

            $schedule_at = $request->schedule_at;
            $from_place = $request->s_address;
            $to_place = $request->d_address;
            $providers = Provider::where('id',$request->provider_id)->get();
            $providerArr = $providers->toArray()[0];

            $providerName = $providerArr["first_name"] .' '.$providerArr["last_name"] ;
            
            $providerImage = (isset($providerArr["picture"])) ? img($providerArr["picture"]) : "" ;
            $providerMobile = $providerArr["mobile"];
            $ProviderService = ProviderService::where('provider_id',$request->provider_id)
                            ->where('service_type_id',$request->service_type_id)
                            ->get();
            $ProviderServiceArr = $ProviderService->toArray()[0];
            $providerCurrentCarNo = $ProviderServiceArr["service_number"];
            //$providerCurrentCarModel = $ProviderService["service_model"];

            //Fare Calculation Duplicate
            $details = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$request->s_latitude.",".$request->s_longitude."&destinations=".$request->d_latitude.",".$request->d_longitude."&mode=driving&sensor=false&key=".env('GOOGLE_MAP_KEY');

            $json = curl($details);

            $details = json_decode($json, TRUE);

            $meter = $details['rows'][0]['elements'][0]['distance']['value'];
            $time = $details['rows'][0]['elements'][0]['duration']['text'];
            $seconds = $details['rows'][0]['elements'][0]['duration']['value'];

            $kilometer = round(($meter/1000) / 1.609344);
            $minutes = round($seconds/60);

            $tax_percentage = Setting::get('tax_percentage');
                    
            $commission_percentage = Setting::get('commission_percentage');
            
            $service_type = $request->service_type;

            $price = $service_type->fixed;

            $price += $service_type->insure_price;

            if($service_type->calculator == 'MIN') {
                $price += $service_type->minute * $minutes;
            } else if($service_type->calculator == 'HOUR') {
                $price += $service_type->minute * 60;
            } else if($service_type->calculator == 'DISTANCE') {
                $price += ($kilometer * $service_type->price);
            } else if($service_type->calculator == 'DISTANCEMIN') {
                $price += ($kilometer * $service_type->price) + ($service_type->minute * $minutes);
            } else if($service_type->calculator == 'DISTANCEHOUR') {
                $price += ($kilometer * $service_type->price) + ($service_type->minute * $minutes * 60);
            } else {
                $price += ($kilometer * $service_type->price);
            }

            $tax_price = ( $tax_percentage/100 ) * $price;
            $total = $price + $tax_price;

            $ActiveProviders = ProviderService::AvailableServiceProvider($request->service_type)->get()->pluck('provider_id');

            $distance = Setting::get('provider_search_radius', '10');
            $latitude = $request->s_latitude;
            $longitude = $request->s_longitude;

            $Providers = Provider::whereIn('id', $ActiveProviders)
                ->where('status', 'approved')
                ->whereRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
                ->get();

            $surge = 0;
            
            if($Providers->count() <= Setting::get('surge_trigger') && $Providers->count() > 0){
                $surge_price = (Setting::get('surge_percentage')/100) * $total;
                $total += $surge_price;
                $surge = 1;
            }

            if($total < $service_type->min_price){
                $total = $service_type->min_price; // minimum value for service type
                $total_is_minimum = 1;
            }else{
                $total_is_minimum = 0;
            }

            $total += $service_type->insure_price;

            //Airport fee calc
            $locations = LocationFee::orderBy('created_at' , 'desc')->get()->toArray();
            foreach ($locations as $value) {
                $loc_details[] = array(
                    'kmd' => $this->getDistance($latitude, $longitude, $value['latitude'], $value['longitude']),
                    'fee' => $value['fee'],
                );
            }
            // $all_kmd = array_column($loc_details, 'kmd');
            // $min_distance = min($all_kmd);
            usort($loc_details, array('App\Http\Controllers\SendPushNotification','sortByOrder'));
            $user_airport_fee = ($loc_details[0]['kmd'] * 1000 <= 1000) ? $loc_details[0]['fee'] : 0 ;
 
            $total += $user_airport_fee;

            $ffare =    ([
                'estimated_fare' => round($total,2), 
                'distance' => $kilometer,
                'time' => $time,
                'surge' => $surge,
                'surge_value' => '1.4X',
                'tax_price' => $tax_price,
                'base_price' => $service_type->fixed,
                'insure_price' => $service_type->insure_price,
                'min_price' => $service_type->min_price,
                'total_is_minimum' => $total_is_minimum,
                //'wallet_balance' => Auth::user()->wallet_balance
            ]);
            //Fare Calculation Duplicate end
            $RideDetails = [
                "driver_name" => $providerName,
                "driver_mobile" => $providerMobile,
                "car_no" => $providerCurrentCarNo,
                //"car_model" => $providerCurrentCarModel,
                "dirver_image" => $providerImage,
                "schedule_at" => $schedule_at,
                "from_place" => $from_place,
                "to_place" => $to_place,
                "fare" => $ffare["estimated_fare"],
                "insure_price" => $ffare["insure_price"],
                "min_price" => $ffare["min_price"],
                "total_is_minimum" => $ffare["total_is_minimum"],
                'airport_fee' => $user_airport_fee,
            ];
        
        //dd(json_encode($RideDetails));
		
		            $message = \PushNotification::Message($RideDetails,array('badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Shipx',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),
																			'launchImage' => $RideDetails,
																			'description' => $RideDetails
														                   )); 
			
		
            return $this->sendPushToUser($request->user_id,$message );
        }else{
			
			$message = \PushNotification::Message(trans('api.push.request_accepted'),array(
																			'badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Shipx',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),
																			'launchImage' => trans('api.push.request_accepted'),
																			'description' => trans('api.push.request_accepted')
														                   )); 
			
    	return $this->sendPushToUser($request->user_id, $message);
    }
    }
    //Sorting distance array
    private static function sortByOrder($a, $b) { return ($b['kmd']<$a['kmd'])?1:-1; } 
    /**
     * Haversine Formula
     *
     * @return Distance between two points
     */
    public function getDistance( $s_latitude, $s_longitude, $latitude_c, $longitude_c ) {  
        $earth_radius = 6371;
    
        $dLat = deg2rad( $latitude_c - $s_latitude );  
        $dLon = deg2rad( $longitude_c - $s_longitude );  
    
        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($s_latitude)) * cos(deg2rad($latitude_c)) * sin($dLon/2) * sin($dLon/2);  
        $c = 2 * asin(sqrt($a));  
        $d = $earth_radius * $c;  
    
        return $d;  
    }

    /**
     * Send Push Notification For Scheduled Ride To Rider
     *
     * @return void
     */
    public function user_schedule_cron($user){
		
		$message = \PushNotification::Message(trans('api.push.sendscpush_u'),array(
																			'badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Shipx',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),
																			'launchImage' => trans('api.push.sendscpush_u'),
																			'description' => trans('api.push.sendscpush_u')
														                   )); 
			
		

        return $this->sendPushToUser($user, $message);
    }
    
    /**
     * Send Push Notification For Scheduled Ride To Driver
     *
     * @return void
     */
    public function provider_schedule_cron($provider){

	$message = \PushNotification::Message(trans('api.push.sendscpush_d'),array(
																			'badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Shipx',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),
																			'launchImage' => trans('api.push.sendscpush_d'),
																			'description' => trans('api.push.sendscpush_d')
														                   )); 
	
        return $this->sendPushToProvider($provider,$message);
    }

    /**
     * Driver Arrived at your location.
     *
     * @return void
     */
    public function user_schedule($user){
		
		     $message = \PushNotification::Message(trans('api.push.schedule_start'),array(
																			'badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Shipx',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),
																			'launchImage' => trans('api.push.schedule_start'),
																			'description' => trans('api.push.schedule_start')
														                   )); 

		
		

        return $this->sendPushToUser($user,$message);
    }

    /**
     * New Incoming request
     *
     * @return void
     */
    public function provider_schedule($provider){
		
		$message = \PushNotification::Message(trans('api.push.schedule_start'),array(
																			'badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Shipx',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),
																			'launchImage' => trans('api.push.schedule_start'),
																			'description' => trans('api.push.schedule_start')
														                   )); 

        return $this->sendPushToProvider($provider,$message );

    }

    /**
     * New Ride Accepted by a Driver.
     *
     * @return void
     */
    public function UserCancellRide($request){

	$message = \PushNotification::Message(trans('api.push.user_cancelled'),array(
																			'badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Shipx',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),
																			'launchImage' => trans('api.push.user_cancelled'),
																			'description' => trans('api.push.user_cancelled')
														                   )); 
	
        return $this->sendPushToProvider($request->provider_id,$message);
    }


    /**
     * New Ride Accepted by a Driver.
     *
     * @return void
     */
    public function ProviderCancellRide($request){

	$message = \PushNotification::Message(trans('api.push.provider_cancelled'),array(
																			'badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Shipx',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),
																			'launchImage' => trans('api.push.provider_cancelled'),
																			'description' => trans('api.push.provider_cancelled')
														                   )); 
	
        return $this->sendPushToUser($request->user_id,$message );
    }

    /**
     * Driver Arrived at your location.
     *
     * @return void
     */
    public function Arrived($request){

	$message = \PushNotification::Message(trans('api.push.arrived'),array(
																			'badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Shipx',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),
																			'launchImage' => trans('api.push.arrived'),
																			'description' => trans('api.push.arrived')
														                   )); 
	
        return $this->sendPushToUser($request->user_id,$message );
    }
 /**
     * Driver Dropped at your location.
     *
     * @return void
     */
    public function Dropped($request){

        $message = \PushNotification::Message(trans('api.push.dropped_ride_on'),array(
                                                                                'badge' => 1,
                                                                                'sound' => 'default',
                                                                                'actionLocKey' => 'Shipx',
                                                                                'locKey' => 'localized key',
                                                                                'locArgs' => array(
                                                                                    'localized args',
                                                                                    'localized args',
                                                                                ),
                                                                                'launchImage' => trans('api.push.dropped_ride_on'),
                                                                                'description' => trans('api.push.dropped_ride_on')
                                                                               )); 
        
            return $this->sendPushToUser($request->user_id,$message );
        }
    /**
     * Money added to user wallet.
     *
     * @return void
     */
    public function ProviderNotAvailable($user_id){

	$message = \PushNotification::Message(trans('api.push.provider_not_available'),array(
																			'badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Shipx',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),
																			'launchImage' => trans('api.push.provider_not_available'),
																			'description' => trans('api.push.provider_not_available')
                                                                           )); 
                                                                           
	
        return $this->sendPushToUser($user_id,$message);
    }

    /**
     * New Incoming request
     *
     * @return void
     */
    public function IncomingRequest($provider){
		
		$message = \PushNotification::Message(trans('api.push.incoming_request'),array(
																			'badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Shipx',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),																			
																			'description' => trans('api.push.incoming_request')
														                   )); 
																		   
																		   
					
            	/*	
            		return \PushNotification::app('AndroidProvider')
        	            ->to('f-lLoSbJAGQ:APA91bFdx9dFJUY5hN78ErYyRnXXCuAmtT2rUJS0g7lVsgw7eUX85BeX7unlkFshdDbms6dedlLwZnLaKru-6TWMnqVX4HNCDCZtYhoWq0sTgaXZl_MxQpLh0xpiFIsQD_Md8DK02aQD6ExSd6Km2C5HkSDH-Wvelw')
        	            ->send("555555555"); 

	$message = \PushNotification::Message(trans('api.push.incoming_request'),array(
																			'badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Shipx',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),
																			'launchImage' => trans('api.push.incoming_request'),
																			'description' => trans('api.push.incoming_request')
														                   ));  */
	
        return $this->sendPushToProvider($provider,$message);

    }

	    public function pushTest(){


			//$this->IncomingRequest('6');

			//echo "push test";
			//die();

	/*$message = \PushNotification::Message('koushik sarkar',array(
																			'badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Shipx',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),																			
																			'description' => 'koushik sarkar 1234567'
														                   )); */


					try{

            		/*return \PushNotification::app('AndroidProvider')
        	            ->to('f-lLoSbJAGQ:APA91bFdx9dFJUY5hN78ErYyRnXXCuAmtT2rUJS0g7lVsgw7eUX85BeX7unlkFshdDbms6dedlLwZnLaKru-6TWMnqVX4HNCDCZtYhoWq0sTgaXZl_MxQpLh0xpiFIsQD_Md8DK02aQD6ExSd6Km2C5HkSDH-Wvelw')
        	            ->send($message); */
						
						

    	} catch(Exception $e){
    		return $e;
    	}
    }





    

    /**
     * Driver Documents verfied.
     *
     * @return void
     */
    public function DocumentsVerfied($provider_id){

	$message = \PushNotification::Message(trans('api.push.document_verfied'),array(
																			'badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Shipx',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),
																			'launchImage' => trans('api.push.document_verfied'),
																			'description' => trans('api.push.document_verfied')
														                   )); 
	
        return $this->sendPushToProvider($provider_id,$message );
    }


    /**
     * Money added to user wallet.
     *
     * @return void
     */
    public function WalletMoney($user_id, $money){

	$message = \PushNotification::Message($money.' '.trans('api.push.added_money_to_wallet'),array(
																			'badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Shipx',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),
																			'launchImage' => $money.' '.trans('api.push.added_money_to_wallet'),
																			'description' => $money.' '.trans('api.push.added_money_to_wallet')
														                   )); 
	
        return $this->sendPushToUser($user_id,$message );
    }

    /**
     * Money charged from user wallet.
     *
     * @return void
     */
    public function ChargedWalletMoney($user_id, $money){

	$message = \PushNotification::Message($money.' '.trans('api.push.charged_from_wallet'),array(
																			'badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Shipx',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),
																			'launchImage' => $money.' '.trans('api.push.charged_from_wallet'),
																			'description' => $money.' '.trans('api.push.charged_from_wallet')
														                   )); 
	
        return $this->sendPushToUser($user_id,$message );
    }

    /**
     * Sending Push to a user Device.
     *
     * @return void
     */
    public function sendPushToUser($user_id, $push_message){

    	try{

	    	$user = User::findOrFail($user_id);

            if($user->device_token != ""){

    	    	if($user->device_type == 'ios'){

    	    		return \PushNotification::app('IOSUser')
    		            ->to($user->device_token)
    		            ->send($push_message);

    	    	}elseif($user->device_type == 'android'){
    	    		
    	    		return \PushNotification::app('AndroidUser')
    		            ->to($user->device_token)
    		            ->send($push_message);

    	    	}
            }

    	} catch(Exception $e){
    		return $e;
    	}

    }

    /**
     * Sending Push to a user Device.
     *
     * @return void
     */
    public function sendPushToProvider($provider_id, $push_message){

    	try{

	    	$provider = ProviderDevice::where('provider_id',$provider_id)->first();

            if($provider->token != ""){

            	if($provider->type == 'ios'){
            		
            		return \PushNotification::app('IOSProvider')
        	            ->to($provider->token)
        	            ->send($push_message);

            	}elseif($provider->type == 'android'){
            		
            		return \PushNotification::app('AndroidProvider')
        	            ->to($provider->token)
        	            ->send($push_message);

            	}
            }

    	} catch(Exception $e){
    		return $e;
    	}

    }

    /**
     * Sending Push to all users and provider Device.
     *
     * @return void
     */
    // public function SendToAll($request){
    // 	try{
            
			
			
    //         $message = \PushNotification::Message($request['title'],array(
    //             'badge' => 1,
    //             'sound' => 'default',

    //             /*'title' => $request['title'],
    //             'body' => $request['description'], 
    //             'tag' => '1',
    //             'click_action' => 'OPEN_ACTIVITY_1',*/
    //             //'icon' => 'fcm_push_icon'
                
    //             'actionLocKey' => 'Shipx',
    //             'locKey' => 'localized key',
    //             'locArgs' => array(
    //                 'localized args',
    //                 'localized args',
    //             ),
    //             'launchImage' => 'image.jpg',
	// 	        'description' => $request['description'],                
    //             'custom' => array('custom_data' => array(
    //                 'shipx' => 'Shipx', 'News'
    //             ))
    //         ));

    //         $devices = \PushNotification::DeviceCollection(array(
    //             \PushNotification::Device('token', array('badge' => 5)),
    //             \PushNotification::Device('token2')
    //         ));

    //         //IosUsers
	//     	$AllIosUsers = User::UserDeviceDetailsIos()->whereNotNull('device_token')->where('device_token', '<>', '')->get();
			
	// 		$IosUsersDeviceslist = array();
			
    //         foreach($AllIosUsers as $IosUser){
    //             if( 10 <= strlen($IosUser['device_token'])){
	// 				$IosUsersDeviceslist[] = $IosUser['device_token'];
    //                 $IosUsersDeviceslist[] = \PushNotification::Device($IosUser['device_token']);
	// 				 $IosUsersDeviceslist[] = \PushNotification::Device($IosUser->device_token);
    //             }
    //         }
			
                      
	// 		$IosUsersDeviceslist[]=\PushNotification::Device('ad04a056692846bc8d839ff60524af7aadc7db0f1e92ac1a6709d7dacdb35997');
					
	// 		$IosUsersDevices = \PushNotification::DeviceCollection($IosUsersDeviceslist);
    //         $IOSUsersCollection = \PushNotification::app('IOSUser')
    //             ->to($IosUsersDevices)
    //             ->send($message);
				
	// 			$IOSUsersCollection = \PushNotification::app('IOSUser')
    // 		            ->to('ad04a056692846bc8d839ff60524af7aadc7db0f1e92ac1a6709d7dacdb35997')
    // 		            ->send($message);
				
				
    //         // get response for each device push
    //         foreach ($IOSUsersCollection->pushManager as $push) {
    //             $IOSUsersResponse = $push->getAdapter()->getResponse();
    //         }


    //         //IosProviders
    //         $AllIosProviders = ProviderDevice::ProviderDeviceDetailsIos()->whereNotNull('token')->where('token', '<>', '')->get();
    //         foreach($AllIosProviders as $IosProviders){
    //             if( 20 <= strlen($IosProviders->token )){
    //                 $IosProvidersDeviceslist[] = \PushNotification::Device($IosProviders->token);
    //             }
    //         }
			
    //         $IosProvidersDevices = \PushNotification::DeviceCollection($IosProvidersDeviceslist);
    //         $IosProvidersCollection = \PushNotification::app('IOSProvider')
    //             ->to($IosProvidersDevices)
    //             ->send($message);
    //         // get response for each device push
    //         foreach ($IosProvidersCollection->pushManager as $push) {
    //             $IosProvidersDevicesResponse = $push->getAdapter()->getResponse();
    //         }


    //         //AndroidUsers
    //         $AllAndroidUsers = User::UserDeviceDetailsAndroid()->whereNotNull('device_token')->where('device_token', '<>', '')->get();
    //         foreach($AllAndroidUsers as $AndroidUser){
    //             if($AndroidUser->device_token){
    //                 $AndroidUsersDeviceslist[] = \PushNotification::Device($AndroidUser->device_token);
    //             }
    //         }
    //         $AndroidUsersDevices = \PushNotification::DeviceCollection($AndroidUsersDeviceslist);
    //         $AndroidUsersCollection = \PushNotification::app('AndroidUser')
    //             ->to($AndroidUsersDevices)
    //             ->send($message);
            
    //         // access to adapter for advanced settings
    //         $AndroidUsersCollection->adapter->setAdapterParameters(['sslverifypeer' => false]);
    //         // get response for each device push
    //         foreach ($AndroidUsersCollection->pushManager as $push) {
    //             $AndroidUsersResponse = $push->getAdapter()->getResponse();
    //         }


    //         //AndroidProviders
    //         $AllAndroidProviders = ProviderDevice::ProviderDeviceDetailsAndroid()->whereNotNull('token')->where('token', '<>', '')->get();
    //         foreach($AllAndroidProviders as $AndroidProviders){
    //             if($AndroidProviders->token){
    //                 $AndroidProvidersDeviceslist[] = \PushNotification::Device($AndroidProviders->token);
    //             }
    //         }
    //         $AndroidProvidersDevices = \PushNotification::DeviceCollection($AndroidProvidersDeviceslist);
    //         $AndroidProvidersCollection = \PushNotification::app('AndroidProvider')
    //             ->to($AndroidProvidersDevices)
    //             ->send($message);

    //         // access to adapter for advanced settings
    //         $AndroidProvidersCollection->adapter->setAdapterParameters(['sslverifypeer' => false]);
    //         // get response for each device push
    //         foreach ($AndroidProvidersCollection->pushManager as $push) {
    //             $AndroidProvidersDevicesResponse = $push->getAdapter()->getResponse();
    //         }
            
    //         $allDeviceResponse = [
    //             "IOSUsersResponse"  =>  $IOSUsersResponse,
    //             "IosProvidersDevicesResponse"  =>  $IosProvidersDevicesResponse,
    //             "AndroidUsersResponse"  =>  $AndroidUsersResponse,
    //             "AndroidProvidersDevicesResponse"  =>  $AndroidProvidersDevicesResponse,
    //             ];
    //         return $allDeviceResponse; 

    // 	} catch(Exception $e){
    // 		return $e;
    // 	}

    // }

    public function SendToAll($request)
    {
        try{
            $message = \PushNotification::Message($request['title'],array(
                'badge' => 1,
                'sound' => 'default',
                'actionLocKey' => 'Shipx',
                'locKey' => 'localized key',
                'locArgs' => array(
                    'localized args',
                    'localized args',
                ),
                'launchImage' => 'image.jpg',
                'description' => $request['description'],
                'custom' => array('custom_data' => array(
                    'Shipx' => 'Shipx', 'News'
                ))
            ));


            //IosProviders
            $AllIosProviders = ProviderDevice::ProviderDeviceDetailsIos()->whereNotNull('token')->where('token', '<>', '')->get();
            foreach($AllIosProviders as $IosProviders){
                 if( 20 <= strlen($IosProviders->token )){
                    
                    $IosProvidersDeviceslist[] = \PushNotification::Device($IosProviders->token, array('badge' => 5));
                 }
                
            }
            $IosProvidersDevices = \PushNotification::DeviceCollection($IosProvidersDeviceslist);

            // echo '<pre>';
            // print_r($IosProvidersDevices);
            // echo '</pre>';
            // die;

            $IosProvidersCollection = \PushNotification::app('IOSProvider')
                ->to($IosProvidersDevices)
                ->send($message);
            // get response for each device push
            foreach ($IosProvidersCollection->pushManager as $push) {
                $IosProvidersDevicesResponse = $push->getAdapter()->getResponse();
            }

            //AndroidProviders
            $AllAndroidProviders = ProviderDevice::ProviderDeviceDetailsAndroid()->whereNotNull('token')->where('token', '<>', '')->get();
            foreach($AllAndroidProviders as $AndroidProviders){
                if($AndroidProviders->token){
                    $AndroidProvidersDeviceslist[] = \PushNotification::Device($AndroidProviders->token);
                }
            }
            $AndroidProvidersDevices = \PushNotification::DeviceCollection($AndroidProvidersDeviceslist);
            $AndroidProvidersCollection = \PushNotification::app('AndroidProvider')
                ->to($AndroidProvidersDevices)
                ->send($message);

            // access to adapter for advanced settings
            $AndroidProvidersCollection->adapter->setAdapterParameters(['sslverifypeer' => false]);
            // get response for each device push
            foreach ($AndroidProvidersCollection->pushManager as $push) {
                $AndroidProvidersDevicesResponse = $push->getAdapter()->getResponse();
            }

            //IosUsers
	    	$AllIosUsers = User::UserDeviceDetailsIos()->whereNotNull('device_token')->where('device_token', '<>', '')->get();
            foreach($AllIosUsers as $IosUser){
                if(20 <= strlen($IosUser->device_token )){
                    

                    $IosUsersDeviceslist[] = \PushNotification::Device($IosUser->device_token);
                }
                //$IosUsersDeviceslist[] = \PushNotification::Device($IosUser->device_token);
            }
            $IosUsersDevices = \PushNotification::DeviceCollection($IosUsersDeviceslist);
            
            $IOSUsersCollection = \PushNotification::app('IOSUser')
                ->to($IosUsersDevices)
                ->send($message);
            // get response for each device push
            foreach ($IOSUsersCollection->pushManager as $push) {
                $IOSUsersResponse = $push->getAdapter()->getResponse();
            }


           


            //AndroidUsers
            $AllAndroidUsers = User::UserDeviceDetailsAndroid()->whereNotNull('device_token')->where('device_token', '<>', '')->get();
            foreach($AllAndroidUsers as $AndroidUser){
                if($AndroidUser->device_token){
                    $AndroidUsersDeviceslist[] = \PushNotification::Device($AndroidUser->device_token);
                }
            }
            $AndroidUsersDevices = \PushNotification::DeviceCollection($AndroidUsersDeviceslist);
            $AndroidUsersCollection = \PushNotification::app('AndroidUser')
                ->to($AndroidUsersDevices)
                ->send($message);
            
            // access to adapter for advanced settings
            $AndroidUsersCollection->adapter->setAdapterParameters(['sslverifypeer' => false]);
            // get response for each device push
            foreach ($AndroidUsersCollection->pushManager as $push) {
                $AndroidUsersResponse = $push->getAdapter()->getResponse();
            }
            $allDeviceResponse = [
                "IOSUsersResponse"  =>  $IOSUsersResponse,               
                "AndroidUsersResponse"  =>  $AndroidUsersResponse, 
                "IosProvidersDevicesResponse"  =>  $IosProvidersDevicesResponse,               
                "AndroidProvidersDevicesResponse"  =>  $AndroidProvidersDevicesResponse              
                ];
            return $allDeviceResponse;

        } catch(\Exception $e){
            return $e;
        }

    }
     /**
     * Sending Push to all users and provider Device.
     *
     * @return void
     */
    public function SendToDriverAll($request){

    	try{
            
            $message = \PushNotification::Message($request['title'],array(
                'badge' => 1,
                'sound' => 'default',
                'actionLocKey' => 'Shipx',
                'locKey' => 'localized key',
                'locArgs' => array(
                    'localized args',
                    'localized args',
                ),
                'launchImage' => 'image.jpg',
                'description' => $request['description'],
                'custom' => array('custom_data' => array(
                    'shipx' => 'Shipx', 'News'
                ))
            ));


            //IosProviders
            $AllIosProviders = ProviderDevice::ProviderDeviceDetailsIos()->whereNotNull('token')->where('token', '<>', '')->get();
            foreach($AllIosProviders as $IosProviders){
                if( 20 <= strlen($IosProviders->token )){
                    

                    $IosProvidersDeviceslist[] = \PushNotification::Device($IosProviders->token);
                }

                //$IosProvidersDeviceslist[] = \PushNotification::Device($IosProviders->token);
            }
            $IosProvidersDevices = \PushNotification::DeviceCollection($IosProvidersDeviceslist);
            $IosProvidersCollection = \PushNotification::app('IOSProvider')
                ->to($IosProvidersDevices)
                ->send($message);
            // get response for each device push
            foreach ($IosProvidersCollection->pushManager as $push) {
                $IosProvidersDevicesResponse = $push->getAdapter()->getResponse();
            }

            //AndroidProviders
            $AllAndroidProviders = ProviderDevice::ProviderDeviceDetailsAndroid()->whereNotNull('token')->where('token', '<>', '')->get();
            foreach($AllAndroidProviders as $AndroidProviders){
                if($AndroidProviders->token){
                    $AndroidProvidersDeviceslist[] = \PushNotification::Device($AndroidProviders->token);
                }
            }
            $AndroidProvidersDevices = \PushNotification::DeviceCollection($AndroidProvidersDeviceslist);
            $AndroidProvidersCollection = \PushNotification::app('AndroidProvider')
                ->to($AndroidProvidersDevices)
                ->send($message);

            // access to adapter for advanced settings
            $AndroidProvidersCollection->adapter->setAdapterParameters(['sslverifypeer' => false]);
            // get response for each device push
            foreach ($AndroidProvidersCollection->pushManager as $push) {
                $AndroidProvidersDevicesResponse = $push->getAdapter()->getResponse();
            }

            $allDeviceResponse = [             
                "IosProvidersDevicesResponse"  =>  $IosProvidersDevicesResponse,               
                "AndroidProvidersDevicesResponse"  =>  $AndroidProvidersDevicesResponse,
                ];
            return $allDeviceResponse;

    	} catch(Exception $e){
    		return $e;
    	}

    }
    
     /**
     * Sending Push to all users and provider Device.
     *
     * @return void
     */
    public function SendToRiderAll($request){

    	try{
            
            $message = \PushNotification::Message($request['title'],array(
                'badge' => 1,
                'sound' => 'default',

                /*'title' => $request['title'],
                'body' => $request['description'], 
                'tag' => '1',
                'click_action' => 'OPEN_ACTIVITY_1',*/
                //'icon' => 'fcm_push_icon'
                
                'actionLocKey' => 'Shipx',
                'locKey' => 'localized key',
                'locArgs' => array(
                    'localized args',
                    'localized args',
                ),
                'launchImage' => 'image.jpg',
                'description' => $request['description'],
                'custom' => array('custom_data' => array(
                    'shipx' => 'Shipx', 'News'
                ))
            ));

            // $devices = \PushNotification::DeviceCollection(array(
            //     \PushNotification::Device('token', array('badge' => 5)),
            //     \PushNotification::Device('token2')
            // ));

            //IosUsers
	    	$AllIosUsers = User::UserDeviceDetailsIos()->whereNotNull('device_token')->where('device_token', '<>', '')->get();
            foreach($AllIosUsers as $IosUser){
                if(20 <= strlen($IosUser->device_token )){


                    $IosUsersDeviceslist[] = \PushNotification::Device($IosUser->device_token);
                }

                //$IosUsersDeviceslist[] = \PushNotification::Device($IosUser->device_token);
            }
            $IosUsersDevices = \PushNotification::DeviceCollection($IosUsersDeviceslist);
            
            $IOSUsersCollection = \PushNotification::app('IOSUser')
                ->to($IosUsersDevices)
                ->send($message);
            // get response for each device push
            foreach ($IOSUsersCollection->pushManager as $push) {
                $IOSUsersResponse = $push->getAdapter()->getResponse();
            }


           


            //AndroidUsers
            $AllAndroidUsers = User::UserDeviceDetailsAndroid()->whereNotNull('device_token')->where('device_token', '<>', '')->get();
            foreach($AllAndroidUsers as $AndroidUser){
                if($AndroidUser->device_token){
                    $AndroidUsersDeviceslist[] = \PushNotification::Device($AndroidUser->device_token);
                }
            }
            $AndroidUsersDevices = \PushNotification::DeviceCollection($AndroidUsersDeviceslist);
            $AndroidUsersCollection = \PushNotification::app('AndroidUser')
                ->to($AndroidUsersDevices)
                ->send($message);
            
            // access to adapter for advanced settings
            $AndroidUsersCollection->adapter->setAdapterParameters(['sslverifypeer' => false]);
            // get response for each device push
            foreach ($AndroidUsersCollection->pushManager as $push) {
                $AndroidUsersResponse = $push->getAdapter()->getResponse();
            }
            
            $allDeviceResponse = [
                "IOSUsersResponse"  =>  $IOSUsersResponse,               
                "AndroidUsersResponse"  =>  $AndroidUsersResponse,               
                ];
            return $allDeviceResponse;

    	} catch(Exception $e){
    		return $e;
    	}

    }
    public function supports($token)
    {
        return (bool) preg_match('/^[0-9a-zA-Z\-\_]+$/i', $token);
    }

}
