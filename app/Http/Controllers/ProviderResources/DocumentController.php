<?php

namespace App\Http\Controllers\ProviderResources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Auth;
use DB;
use App\Document;
use App\ProviderDocument;
use App\ProviderWallet;
use App\ProviderWalletHistory;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $VehicleDocuments = Document::vehicle()->get();
        $DriverDocuments = Document::driver()->get();

        $Provider = \Auth::guard('provider')->user();

        return view('provider.document.index', compact('DriverDocuments', 'VehicleDocuments', 'Provider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
                'document_front' => 'mimes:jpg,jpeg,png,pdf',
                'document_back' => 'mimes:jpg,jpeg,png,pdf'
            ]);
        if($request->has('app')) {
            $Provider = Auth::user();
        } else {
            $Provider = \Auth::guard('provider')->user();
        }
        
        $provider_id = $Provider->id;

        try {

            //return $request;
            
            $Document = ProviderDocument::where('provider_id', $provider_id)
                ->where('document_id', $id)
                ->firstOrFail();

                if(isset($request->document_front) && !empty($request->document_front)){
                 /*   $Document->update([
                        'front_url' => $request->document_front->store('provider/documents'),
                        'status' => 'ASSESSING'
                    ]);
                    */
                    $front_url = $request->document_front->store('public/provider/documents');
                    $front_url=str_replace("public/", "", $front_url);
                    $Document->update([
                        'front_url' => $front_url,
                        'status' => 'ASSESSING'
                    ]);
                }
                if(isset($request->document_back) && !empty($request->document_back)){
                    $front_url = $request->document_back->store('public/provider/documents');
                    $front_url=str_replace("public/", "", $front_url);  
                    $Document->update([
                        'back_url' => $front_url,
                        'status' => 'ASSESSING'
                    ]);
                }

           

            if($request->has('app')) {
                return response()->json([
                    'done' => "Document updated successfully",
                ]);
            } else {
            return back();
            }

        } catch (ModelNotFoundException $e) {
                // $date = strtotime("2020-03-37 12:01:45");
            // $date = DateTime::createFromFormat('Y-m-d H:i:s', '2020-03-37 12:01:45');
                 // $date->format('Y-m-d H:i:s');
                 if(isset($request->document_back) && !empty($request->document_back)){
                    $front_url = $request->document_back->store('public/provider/documents');
                    $front_url=str_replace("public/", "", $front_url);  
                    ProviderDocument::create([
                    'back_url' => $front_url,
                        'provider_id' => $provider_id,
                        'document_id' => $id,
                        'status' => 'ASSESSING'
                    ]);
                }
                if(isset($request->document_front) && !empty($request->document_front)){
                    $front_url = $request->document_front->store('public/provider/documents');
                    $front_url=str_replace("public/", "", $front_url);  
                    ProviderDocument::create([
                        'front_url' => $front_url,
                        'provider_id' => $provider_id,
                        'document_id' => $id,
                        'status' => 'ASSESSING'
                    ]);
                }
/*    
            if(isset($request->document_back) && !empty($request->document_back)){
                ProviderDocument::create([
                'back_url' => $request->document_back->store('provider/documents'),
                    'provider_id' => $provider_id,
                    'document_id' => $id,
                    'status' => 'ASSESSING'
                    
                ]);
            }if(isset($request->document_front) && !empty($request->document_front)){
               
                ProviderDocument::create([
                    'front_url' => $request->document_front->store('provider/documents'),
                    'provider_id' => $provider_id,
                    'document_id' => $id,
                    'status' => 'ASSESSING'
                    
                ]);
            } */

            
            
        }

        if($request->has('app')) {
            return response()->json([
                'done' => "Document updated successfully",
            ]);
        } else {
        return back();
    }
    }



    public function update_expirydate(Request $request, $id){
       // echo "hello";
      if($request->has('app')) {
            $Provider = Auth::user();
        } else {
            $Provider = \Auth::guard('provider')->user();
        }
        
        $provider_id = $Provider->id;

    try {
            $Document = ProviderDocument::where('provider_id', $provider_id)
                ->where('document_id', $id)
                ->firstOrFail();
               
            
                if($request->has('expired_date')){
                     $expire_date1=$request->expired_date;
                    
                    $Document->update([
                        'expires_at'=> date("Y-m-d", strtotime($expire_date1)),
                        'status' => 'ASSESSING'
                    ]);
                }


        } catch (Exception $e) {
           return back();
        }

          if($request->has('app')) {
            return response()->json([
                'done' => "Document updated successfully",
            ]);
        } else {
        return back();
    }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    /**
     * list provider document.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {

        $VehicleDocuments = DB::table('documents')
            ->select('documents.*',
                        DB::raw('(select provider_documents.status  from provider_documents where documents.id=provider_documents.document_id AND provider_documents.provider_id=' . Auth::user()->id . ') AS provider_document_status'),
                        DB::raw('(select provider_documents.front_url  from provider_documents where documents.id=provider_documents.document_id AND provider_documents.provider_id=' . Auth::user()->id . ') AS provider_document_front_url'),
                        DB::raw('(select provider_documents.back_url  from provider_documents where documents.id=provider_documents.document_id AND provider_documents.provider_id=' . Auth::user()->id . ') AS provider_document_back_url'),
                        DB::raw('(select provider_documents.expires_at  from provider_documents where documents.id=provider_documents.document_id AND provider_documents.provider_id=' . Auth::user()->id . ') AS provider_document_expires_at')
                        
                    )
            ->where('type', 'VEHICLE')
            ->get();


            

        


        $DriverDocuments =  DB::table('documents')
        ->select('documents.*',
                        DB::raw('(select provider_documents.status  from provider_documents where documents.id=provider_documents.document_id AND provider_documents.provider_id=' . Auth::user()->id . ') AS provider_document_status'),
                        DB::raw('(select provider_documents.front_url  from provider_documents where documents.id=provider_documents.document_id AND provider_documents.provider_id=' . Auth::user()->id . ') AS provider_document_front_url'),
                        DB::raw('(select provider_documents.back_url  from provider_documents where documents.id=provider_documents.document_id AND provider_documents.provider_id=' . Auth::user()->id . ') AS provider_document_back_url'),
                        DB::raw('(select provider_documents.expires_at  from provider_documents where documents.id=provider_documents.document_id AND provider_documents.provider_id=' . Auth::user()->id . ') AS provider_document_expires_at')
                        
                    )
        ->where('type', 'DRIVER')
        ->get();

            $VehicleImage = DB::table('documents')
            ->select('documents.*',
                        DB::raw('(select provider_documents.status  from provider_documents where documents.id=provider_documents.document_id AND provider_documents.provider_id=' . Auth::user()->id . ') AS provider_document_status'),
                        DB::raw('(select provider_documents.front_url   from provider_documents where documents.id=provider_documents.document_id AND provider_documents.provider_id=' . Auth::user()->id . ') AS provider_document_url')
                    )
            ->where('type', 'VEHICLE IMAGE')
            ->get();

        $Provider = Auth::user();

        return response()->json([
            'VehicleDocuments' => $VehicleDocuments,
            'DriverDocuments' => $DriverDocuments,
            'VehicleImage' => $VehicleImage,
            'id' => Auth::user()->id
        ]);
    }

    /*
    *For fteching the providers wallet history
     */

     public function getWalletHistory()
     {
         $providerId = Auth::user()->id;

         

         $getWallet = ProviderWallet::where('provider_id',$providerId)->select('provider_id')->first();

       if($getWallet){
           $result = [];
           $getHistory = ProviderWalletHistory::where('provider_id',$getWallet->provider_id)->orderby('created_at', 'desc')->get();
           
            foreach($getHistory as $key => $wallet){
                $wallet['amount'] = $wallet->plan->amount;
                
                $result[] = $wallet;
            }
           return response()->json([
               'WaletHistory' => $result
           ]);
       }else{
        return response()->json(['error' => 'This wallet does not exist']);
       }


     }

     /*
     * For Cheking provider Wallet validation
      */

      public function checkWallet()
      {
        $providerId = Auth::user()->id;

        try {
            $wallExist = ProviderWallet::where('provider_id', $providerId)->firstOrFail();

        $startDate = $wallExist->validation_start;
        $endDate = date_create($wallExist->validation_end);
        $todayDate = date_create(date("Y-m-d")); 

        $duration = $wallExist->days;

        $diff= date_diff($todayDate,$endDate);
        $currentduration = $diff->format("%a");

        $daysValid = explode(' ', $duration);
        if($wallExist->valid == 1){
            if(($currentduration > 0) && ($currentduration == $daysValid[0] || $currentduration < $daysValid[0])){
                return response()->json([
                    'success' => 'Your Wallet has ' .$diff->format("%a Days"). 'of validity',
                    'valid' =>1
                    ]);
            }else{
                $WalletHistory = ProviderWalletHistory::where('transaction_id',$wallExist->id)->first();
                $WalletHistory['status'] = 0;
                $WalletHistory->save();
                ProviderWallet::find($wallExist->id)->delete();
                return response()->json([
                    'error' => 'Your Wallet has expired',
                    'valid' =>0
                    ]);
            }
        }
        }catch(ModelNotFoundException $e){
            return response()->json([
                'error' => 'Your Wallet is Empty',
                'valid' => 0
                ]);
        }

      }
}
