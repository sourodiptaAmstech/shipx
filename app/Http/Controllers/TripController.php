<?php

namespace App\Http\Controllers\ProviderResources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use DB;
use Auth;
use Setting;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\Http\Controllers\SendPushNotification;

use App\User;
use App\Admin;
use App\Promocode;
use App\Provider;
use App\Locationfee;
use App\UserRequests;
use App\RequestFilter;
use App\PromocodeUsage;
use App\PromotionUsages;
use App\ProviderService;
use App\UserRequestRating;
use App\UserRequestPayment;
use App\DriverAirportQueue;
use App\DriverTowardsDestination;
use App\ServiceType;
 use Davibennun\LaravelPushNotification\Facades\PushNotification;
use function GuzzleHttp\json_encode;
use App\ParcelDetails;
use Storage;

class TripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $loc_details=[];
    
        try{
            //return json_encode(['status' => 'ok', 'message' => 'yes']);
            if($request->ajax()) {
                $Provider = Auth::user();
            } else {
                $Provider = Auth::guard('provider')->user();
            }

            $provider = $Provider->id;

            /***************** Call for Airport Request for **************koushik********************/
       //     $AirportQueue = $this->riding_driver_airport_queue($request);
            $AfterAssignProvider = RequestFilter::with(['request.user', 'request.payment', 'request'])
                ->where('provider_id', $provider)
                ->whereHas('request', function($query) use ($provider) {
                        $query->where('status','<>', 'CANCELLED');
                        $query->where('status','<>', 'SCHEDULED');
                        $query->where('provider_id', $provider );
                        $query->where('current_provider_id', $provider);
                    });

            $BeforeAssignProvider = RequestFilter::with(['request.user', 'request.payment', 'request'])
                ->where('provider_id', $provider)
                ->whereHas('request', function($query) use ($provider){
                        $query->where('status','<>', 'CANCELLED');
                        $query->where('status','<>', 'SCHEDULED');
                        $query->where('current_provider_id',$provider);
                    });

            $IncomingRequests = $BeforeAssignProvider->union($AfterAssignProvider)->get();

            if(!empty($request->latitude)) {
                $Provider->update([
                        'latitude' => $request->latitude,
                        'longitude' => $request->longitude,
                ]);
            }

            $Timeout = Setting::get('provider_select_timeout', 180);
                if(!empty($IncomingRequests)){
					
                    for ($i=0; $i < sizeof($IncomingRequests); $i++) {
                        $IncomingRequests[$i]->time_left_to_respond = $Timeout - (time() - strtotime($IncomingRequests[$i]->request->assigned_at));
                        if($IncomingRequests[$i]->request->status == 'SEARCHING' && $IncomingRequests[$i]->time_left_to_respond < 0) {
                            
							$this->assign_next_provider($IncomingRequests[$i]->request->id);
							
                        }
                    }
                }

            $AutoacceptedRiding = UserRequests::where('status','STARTED')
                            ->orWhere('status','ARRIVED')
                            ->orWhere('status','PICKEDUP')
                            
                            ->orWhere(function($q){
                                $q->where('status','COMPLETED');
                                $q->where('auto_accepted','2');
                                //$q->where('auto_accepted','<>', '0');
                            })
                                //->where('status','<>', 'CANCELLED')
                                //->where('status','<>', 'SCHEDULED')
                            ->where('auto_accepted_provider_id', Auth::user()->id)->get();
                            
            $ProviderServiceStat = (isset(Auth::user()->service->status)) ? Auth::user()->service->status : 'none' ;
            if($ProviderServiceStat == 'riding' && $AutoacceptedRiding->isNotEmpty()){
                $IncomingRequests = UserRequests::with(['user', 'payment'])
                            ->where('status','STARTED')
                            ->orWhere('status','ARRIVED')
                            ->orWhere('status','PICKEDUP')
                            ->orWhere(function($q){
                                $q->where('status','COMPLETED');
                                $q->where('auto_accepted','2');
                                //$q->where('auto_accepted','<>', '0');
                            })
                            ->where('provider_id', Auth::user()->id)->get();
                            //->where('auto_accepted_provider_id', Auth::user()->id)->get();
                if($IncomingRequests->isNotEmpty()){
                    $RidingArr = $IncomingRequests->toArray()[0];
                }
            }

            if($IncomingRequests->isNotEmpty()){
				
				
                if(Auth::user()->service->status == 'riding' && $AutoacceptedRiding->isNotEmpty()){
                    $requestArr["request"] = $RidingArr;
                    //$IncomingRequests = [$IncomingRequests1->first()];
                    
                    $IncomingRequests = [
                            [
                                'id' => 0,
                                'status' => 0,
                                'time_left_to_respond' => 0,
                                'provider_id' => $RidingArr['provider_id'],
                                'request_id' => $RidingArr['id'],
                                'request' => $RidingArr,
                            ],
                        ];
                }else {
                    $requestArr = $IncomingRequests->toArray()[0];
                }
            //Fare Calculation Duplicate
            $details = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$requestArr["request"]["s_latitude"].",".$requestArr["request"]["s_longitude"]."&destinations=".$requestArr["request"]["d_latitude"].",".$requestArr["request"]["d_longitude"]."&mode=driving&sensor=false&key=".env('GOOGLE_MAP_KEY');

            $json = curl($details);

            $details = json_decode($json, TRUE);

            $meter = $details['rows'][0]['elements'][0]['distance']['value'];
            $time = $details['rows'][0]['elements'][0]['duration']['text'];
            $seconds = $details['rows'][0]['elements'][0]['duration']['value'];

            $kilometer = round(($meter/1000) / 1.609344);
            $minutes = round($seconds/60);

            $tax_percentage = Setting::get('tax_percentage');
                    
            $commission_percentage = Setting::get('commission_percentage');
            
            $service_type = ServiceType::findOrFail($requestArr["request"]["service_type_id"]);

            $price = $service_type->fixed;

            if($service_type->calculator == 'MIN') {
                $price += $service_type->minute * $minutes;
            } else if($service_type->calculator == 'HOUR') {
                $price += $service_type->minute * 60;
            } else if($service_type->calculator == 'DISTANCE') {
                $price += ($kilometer * $service_type->price);
            } else if($service_type->calculator == 'DISTANCEMIN') {
                $price += ($kilometer * $service_type->price) + ($service_type->minute * $minutes);
            } else if($service_type->calculator == 'DISTANCEHOUR') {
                $price += ($kilometer * $service_type->price) + ($service_type->minute * $minutes * 60);
            } else {
                $price += ($kilometer * $service_type->price);
            }

            $tax_price = ( $tax_percentage/100 ) * $price;
            $total = $price + $tax_price;

            $ActiveProviders = ProviderService::AvailableServiceProvider($requestArr["request"]["service_type_id"])->get()->pluck('provider_id');

            $distance = Setting::get('provider_search_radius', '10');
            $latitude = $requestArr["request"]["s_latitude"];
            $longitude = $requestArr["request"]["s_longitude"];

            $Providers = Provider::whereIn('id', $ActiveProviders)
                ->where('status', 'approved')
                ->whereRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
                ->get();

            $surge = 0;
            
            if($Providers->count() <= Setting::get('surge_trigger') && $Providers->count() > 0){
                $surge_price = (Setting::get('surge_percentage')/100) * $total;
                $total += $surge_price;
                $surge = 1;
            }

            if($total < $service_type->min_price){
                $total = $service_type->min_price; // minimum value for service type
                $total_is_minimum = 1;
            }else{
                $total_is_minimum = 0;
            }

            $total += $service_type->insure_price;

            //Airport fee calc
            $locations = LocationFee::orderBy('created_at' , 'desc')->get()->toArray();
            foreach ($locations as $value) {
                $loc_details[] = array(
                    'kmd' => $this->getDistance($latitude, $longitude, $value['latitude'], $value['longitude']),
                    'fee' => $value['fee'],
                );
            }
            // $all_kmd = array_column($loc_details, 'kmd');
            // $min_distance = min($all_kmd);
            $user_airport_fee=0;
            if(!empty($loc_details)){
            usort($loc_details, array('App\Http\Controllers\ProviderResources\TripController','sortByOrder'));
            $user_airport_fee = ($loc_details[0]['kmd'] * 1000 <= 3218) ? $loc_details[0]['fee'] : 0 ;
             }
           echo $kilometer; exit;
            $total += $user_airport_fee;

            $ffare =    ([
                'estimated_fare' => round($total,2), 
                'distance' => $kilometer,
                'time' => $time,
                'surge' => $surge,
                'surge_value' => '1.4X',
                'tax_price' => $tax_price,
                'base_price' => $service_type->fixed,
                'insure_price' => $service_type->insure_price,
                'airport_fee' => $user_airport_fee,
                'min_price' => $service_type->min_price,
                'total_is_minimum' => $total_is_minimum,
                //'wallet_balance' => Auth::user()->wallet_balance
            ]);
            $e_fare = $ffare["estimated_fare"];
            $insure_price = $ffare["insure_price"];
            $min_price = $ffare["min_price"];
            $total_is_minimum = $ffare["total_is_minimum"];
            }else{
                $e_fare = "";
                $insure_price = "";
                $min_price = "";
                $total_is_minimum = "";
            }
            //Fare Calculation Duplicate end
                $Response = [
                    'account_status' => $Provider->status,
                    'service_status' => $Provider->service ? Auth::user()->service->status : 'offline',
                    'requests' => $IncomingRequests,
                    "fare" => $e_fare,
                    "insure_price" => $insure_price,
                    "min_price" => $min_price,
                    "total_is_minimum" => $total_is_minimum,
                ];

            return $Response;
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Something went wrong']);
        }
    }
    //Sorting distance array
    private static function sortByOrder($a, $b) { return ($b['kmd']<$a['kmd'])?1:-1; } 
    /**
     * Haversine Formula
     *
     * @return Distance between two points
     */
    public function getDistance( $s_latitude, $s_longitude, $latitude_c, $longitude_c ) {  
        $earth_radius = 6371;
    
        $dLat = deg2rad( $latitude_c - $s_latitude );  
        $dLon = deg2rad( $longitude_c - $s_longitude );  
    
        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($s_latitude)) * cos(deg2rad($latitude_c)) * sin($dLon/2) * sin($dLon/2);  
        $c = 2 * asin(sqrt($a));  
        $d = $earth_radius * $c;  
    
        return $d;  
    }

    /**
     * Cancel given request.
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel(Request $request)
    {
        $this->validate($request, [
            'cancel_reason'=> 'max:255',
        ]);
        try{

            $UserRequest = UserRequests::findOrFail($request->id);
            $Cancellable = ['SEARCHING', 'ACCEPTED', 'ARRIVED', 'STARTED', 'CREATED','SCHEDULED'];

            if(!in_array($UserRequest->status, $Cancellable)) {
                return back()->with(['flash_error' => 'Cannot cancel request at this stage!']);
            }

            $UserRequest->status = "CANCELLED";
            $UserRequest->cancel_reason = $request->cancel_reason;
            $UserRequest->cancelled_by = "PROVIDER";
            $UserRequest->save();

             RequestFilter::where('request_id', $UserRequest->id)->delete();

             ProviderService::where('provider_id',$UserRequest->provider_id)->update(['status' =>'active']);

             // Send Push Notification to User
            (new SendPushNotification)->ProviderCancellRide($UserRequest);

            return $UserRequest;

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Something went wrong']);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function rate(Request $request, $id)
    {

        $this->validate($request, [
                'rating' => 'required|integer|in:1,2,3,4,5',
                'comment' => 'max:255',
            ]);
    
        try {

            $UserRequest = UserRequests::where('id', $id)
                ->where('status', 'COMPLETED')
                ->firstOrFail();

            if($UserRequest->rating == null) {
                UserRequestRating::create([
                        'provider_id' => $UserRequest->provider_id,
                        'user_id' => $UserRequest->user_id,
                        'request_id' => $UserRequest->id,
                        'provider_rating' => $request->rating,
                        'provider_comment' => $request->comment,
                    ]);
            } else {
                $UserRequest->rating->update([
                        'provider_rating' => $request->rating,
                        'provider_comment' => $request->comment,
                    ]);
            }

            $UserRequest->update(['provider_rated' => 1]);
            $UserRequest->update(['auto_accepted' => 3]);

            // Delete from filter so that it doesn't show up in status checks.
            RequestFilter::where('request_id', $id)->delete();

            ProviderService::where('provider_id',$UserRequest->provider_id)->update(['status' =>'active']);

            // Send Push Notification to Provider 
            $average = UserRequestRating::where('provider_id', $UserRequest->provider_id)->avg('provider_rating');

            $UserRequest->user->update(['rating' => $average]);

            return response()->json(['message' => 'Request Completed!']);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Request not yet completed!'], 500);
        }
    }

    /**
     * Get the scheduled rides for the provider
     *
     * @return \Illuminate\Http\Response
     */
    public function scheduled(Request $request)
    {
        
        try{

            $Jobs = UserRequests::where('provider_id', Auth::user()->id)
                    ->where('status', 'SCHEDULED')
                    ->with('service_type')
                    ->get();

            if(!empty($Jobs)){
                $map_icon = asset('asset/img/marker-end.png');
                foreach ($Jobs as $key => $value) {
                    $Jobs[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=640x260".
                            "&maptype=terrain".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x000000|weight:2|enc:".$value->route_key.
                            "&key=".env('GOOGLE_MAP_KEY');
                }
            }

            return $Jobs;
            
        } catch(Exception $e) {
            return response()->json(['error' => "Something Went Wrong"]);
        }
    }

    /**
     * Get the trip history of the provider
     *
     * @return \Illuminate\Http\Response
     */
    public function history(Request $request)
    { 
       
        if($request->ajax()) {

            $Jobs = UserRequests::where('provider_id', Auth::user()->id)
                    ->orderBy('created_at','desc')
                    ->with('payment')
                    ->get();

            if(!empty($Jobs)){
                $map_icon = asset('asset/img/marker-end.png');
                foreach ($Jobs as $key => $value) {
                    $Jobs[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=640x260".
                            "&maptype=terrain".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x000000|weight:2|enc:".$value->route_key.
                            "&key=".env('GOOGLE_MAP_KEY');
                }
            }
            return $Jobs;
        }
        
            $Jobs = UserRequests::where('provider_id', Auth::user()->id)->with('user', 'service_type', 'payment', 'rating')->get();
            //return $Jobs;
         return view('provider.trip.index', compact('Jobs'));
        
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function accept(Request $request, $id)
    {
        try {

            $UserRequest = UserRequests::findOrFail($id);

            if($UserRequest->status != "SEARCHING" && $UserRequest->status==0) {
                return response()->json(['error' => 'Request already under progress!']);
            }
            
            $UserRequest->provider_id = Auth::user()->id;

            if($UserRequest->schedule_at != ""){

                $beforeschedule_time = strtotime($UserRequest->schedule_at."- 1 hour");
                $afterschedule_time = strtotime($UserRequest->schedule_at."+ 1 hour");

                $CheckScheduling = UserRequests::where('status','SCHEDULED')
                            ->where('provider_id', Auth::user()->id)
                            ->whereBetween('schedule_at',[$beforeschedule_time,$afterschedule_time])
                            ->count();

                if($CheckScheduling > 0 ){
                    if($request->ajax()) {
                        return response()->json(['error' => trans('api.ride.request_already_scheduled')]);
                    }else{
                        return redirect('dashboard')->with('flash_error', 'If the shipment is already scheduled then we cannot schedule/request another shipment for the after 1 hour or before 1 hour');
                    }
                }

                RequestFilter::where('request_id',$UserRequest->id)->where('provider_id',Auth::user()->id)->update(['status' => 2]);

                $UserRequest->status = "SCHEDULED";
                $UserRequest->save();
                //return response()->json(['error' => "Scheduled Ride Accepted"]);

            }else{


                $UserRequest->status = "STARTED";
                $UserRequest->save();


                ProviderService::where('provider_id',$UserRequest->provider_id)->update(['status' =>'riding']);

                $Filters = RequestFilter::where('request_id', $UserRequest->id)->where('provider_id', '!=', Auth::user()->id)->get();
                // dd($Filters->toArray());
                foreach ($Filters as $Filter) {
                    $Filter->delete();
                }
                //return response()->json(['error' => "Ride Accepted"]);
            }

            $UnwantedRequest = RequestFilter::where('request_id','!=' ,$UserRequest->id)
                                ->where('provider_id',Auth::user()->id )
                                ->whereHas('request', function($query){
                                    $query->where('status','<>','SCHEDULED');
                                });

            if($UnwantedRequest->count() > 0){
                $UnwantedRequest->delete();
            }  

            // Send Push Notification to User
            (new SendPushNotification)->RideAccepted($UserRequest);

            //if($request->ajax()|true) {
                return response()->json(['error' => "Ride Accepted"]);
            /*}else{
            return $UserRequest->with('user')->get();
            }*/

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Unable to accept, Please try again later']);
        } catch (Exception $e) {
            return response()->json(['error' => 'Connection Error']);
        }finally{
            return response()->json(['error' => 'Connection Error']);
        }
    }
    
    
    
    public function autoaccept(Request $request, $id)
    {
        try {

            $UserRequest = UserRequests::findOrFail($id);
//dd($UserRequest);
            if($UserRequest->status != "SEARCHING" && $UserRequest->auto_accepted = 1) {
                return response()->json(['error' => 'Request already under progress!']);
            }
//dd($UserRequest->toArray()[0]['auto_accepted_provider_id']);
            //if($UserRequest->auto_accepted = 1){
                $UserRequest->provider_id = $UserRequest->auto_accepted_provider_id;
            //}else{
                //$UserRequest->provider_id = $UserRequest->current_provider_id;
            //}
            //dd($UserRequest);
            $UserRequest->status = "STARTED";
            $UserRequest->save();

            ProviderService::where('provider_id',$UserRequest->provider_id)->update(['status' =>'riding']);

            $Filters = RequestFilter::where('request_id', $UserRequest->id)->where('provider_id', '!=', Auth::user()->id)->get();
            foreach ($Filters as $Filter) {
                $Filter->delete();
            }
            //return response()->json(['error' => "Ride Accepted"]);

            $UnwantedRequest = RequestFilter::where('request_id','!=' ,$UserRequest->id)
                                ->where('provider_id',Auth::user()->id )
                                ->whereHas('request', function($query){
                                    $query->where('status','<>','SCHEDULED');
                                });

            if($UnwantedRequest->count() > 0){
                $UnwantedRequest->delete();
            }  

            // Send Push Notification to User
            (new SendPushNotification)->RideAccepted($UserRequest);

            //if($request->ajax()|true) {
                return response()->json(['error' => "Ride Accepted"]);
            /*}else{
            	return $UserRequest->with('user')->get();
            }*/

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Unable to accept, Please try again later']);
        } catch (Exception $e) {
            return response()->json(['error' => 'Connection Error']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        //$id=$request->id;
        $this->validate($request, [
              'status' => 'required|in:ACCEPTED,STARTED,ARRIVED,PICKEDUP,DROPPED,PAYMENT,COMPLETED',
           ]);
return response()->json(['error' => 'Unable to update, Please try again later']);
        try{

            $UserRequest = UserRequests::with('user')->findOrFail($id);

            if($request->status == 'DROPPED' && $UserRequest->payment_mode != 'CASH') {
                $UserRequest->status = 'COMPLETED';
            } else if ($request->status == 'COMPLETED' && $UserRequest->payment_mode == 'CASH') {
            	 // ProviderService::where('provider_id',2)->update(['status' =>'active']);
                $UserRequest->status = $request->status;
                $UserRequest->paid = 1;
                 ProviderService::where('provider_id',$UserRequest->provider_id)->update(['status' =>'active']);
                // (new SendPushNotification)->COMPLETED($UserRequest);

            } else {
                $UserRequest->status = $request->status;

                if($request->status == 'ARRIVED'){
                    (new SendPushNotification)->Arrived($UserRequest);
                }
            }

            if($request->status == 'PICKEDUP'){
                $UserRequest->started_at = Carbon::now();
             
            }

            $UserRequest->save();
            if($request->status == 'ARRIVED'){
                $UserRequest->invoice = $this->invoice($id);
            }
            if($request->status == 'ARRIVED'){
                $UserRequest->invoice = $this->invoice($id);
            }

            if($request->status == 'DROPPED') {
                $UserRequest->finished_at = Carbon::now();
                $UserRequest->save();
                $UserRequest->with('user')->findOrFail($id);
                (new SendPushNotification)->Dropped($UserRequest);
               
            }

            if($UserRequest->auto_accepted = '1') {
                $UserRequest->auto_accepted = '2';
                $UserRequest->save();
            }
           
            // Send Push Notification to User
       
            return $UserRequest;

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Unable to update, Please try again later']);
        } catch (Exception $e) {
            return response()->json(['error' => 'Connection Error']);
        }finally{
            return response()->json(['error' => 'Connection Error']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $UserRequest = UserRequests::find($id);

        try {
            $this->assign_next_provider($UserRequest->id);
            return $UserRequest->with('user')->get();

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Unable to reject, Please try again later']);
        } catch (Exception $e) {
            return response()->json(['error' => 'Connection Error']);
        }finally{
             return response()->json(['error' => 'Successfully rejected.']);
        }
    }

    public function assign_next_provider($request_id) {

        try {
            $UserRequest = UserRequests::findOrFail($request_id);
        } catch (ModelNotFoundException $e) {
            // Cancelled between update.
            return false;
        }
        $RequestFilter = RequestFilter::where('provider_id', $UserRequest->current_provider_id)
            ->where('request_id', $UserRequest->id)
            ->delete();

        try {

            $next_provider = RequestFilter::where('request_id', $UserRequest->id)
                            ->orderBy('id')
                            ->firstOrFail();

             // incoming request push to provider
            (new SendPushNotification)->IncomingRequest($next_provider->provider_id);
			
			
			
            $UserRequest->current_provider_id = $next_provider->provider_id;
            $UserRequest->assigned_at = Carbon::now();
            $UserRequest->save();

            // incoming request push to provider
            //(new SendPushNotification)->IncomingRequest($next_provider->provider_id);
            
        } catch (ModelNotFoundException $e) {

            UserRequests::where('id', $UserRequest->id)->update(['status' => 'CANCELLED']);

            // No longer need request specific rows from RequestMeta
            RequestFilter::where('request_id', $UserRequest->id)->delete();

            //  request push to user provider not available
            (new SendPushNotification)->ProviderNotAvailable($UserRequest->user_id);
        }
    }

    public function invoice($request_id)
    {
        try {
            $loc_details=[]; 
            $UserRequest = UserRequests::findOrFail($request_id);
            $tax_percentage = Setting::get('tax_percentage',10);
            $commission_percentage = Setting::get('commission_percentage',10);
            $service_type = ServiceType::findOrFail($UserRequest->service_type_id);
            // $details = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$UserRequest->s_latitude.",".$UserRequest->s_longitude."&destinations=".$UserRequest->d_latitude.",".$UserRequest->d_longitude."&mode=driving&sensor=false&key=".env('GOOGLE_MAP_KEY');
            // $json = curl($details);
            // $details = json_decode($json, TRUE);
            // $est_seconds = $details['rows'][0]['elements'][0]['duration']['value'];
            // $est_minutes = round($est_seconds/60);

            $seconds = strtotime($UserRequest->finished_at) - strtotime($UserRequest->started_at);
            $minutes = round($seconds/60);
            
            $kilometer = $UserRequest->distance;
            $Fixed = $service_type->fixed;
            $Distance = 0;
            //$minutes = 0;
            $Discount = 0; // Promo Code discounts should be added here.
            $Wallet = 0;
            $Surge = 0;

            if($service_type->calculator == 'MIN') {
                $Distance = $service_type->minute * $minutes;
            } else if($service_type->calculator == 'HOUR') {
                $Distance = $service_type->minute * 60;
            } else if($service_type->calculator == 'DISTANCE') {
                $Distance = ($kilometer * $service_type->price);
            } else if($service_type->calculator == 'DISTANCEMIN') {
                $Distance = ($kilometer * $service_type->price) + ($service_type->minute * $minutes);
            } else if($service_type->calculator == 'DISTANCEHOUR') {
                $Distance = ($kilometer * $service_type->price) + ($service_type->minute * $minutes * 60);
            } else {
                $Distance = ($kilometer * $service_type->price);
            }

            if($PromocodeUsage = PromocodeUsage::where('user_id',$UserRequest->user_id)->where('status','ADDED')->first()){
                if($Promocode = Promocode::find($PromocodeUsage->promocode_id)){
                    $Discount = $Promocode->discount;
                    $PromocodeUsage->status ='USED';
                    $PromocodeUsage->save();
                }
            }

            $Commision = ($Distance + $Fixed) * ( $commission_percentage/100 );
            $Tax = ($Distance + $Fixed) * ( $tax_percentage/100 );
            $Total = $Fixed + $Distance + $Tax;
			$DiscountTotal=($Total*$Discount)/100; // sarwesh
			$Total = $Total-$DiscountTotal;

            if($UserRequest->surge){
                $Surge = (Setting::get('surge_percentage')/100) * $Total;
                $Total += $Surge;
            }
            if($Total < $service_type->min_price){
                $Total = $service_type->min_price; // minimum value for service type
            }

            $Total += $service_type->insure_price;

            //Airport fee calc
            $latitude = $UserRequest->s_latitude;
            $longitude = $UserRequest->s_longitude;
            $locations = LocationFee::orderBy('created_at' , 'desc')->get()->toArray();
            foreach ($locations as $value) {
                $loc_details[] = array(
                    'kmd' => $this->getDistance($latitude, $longitude, $value['latitude'], $value['longitude']),
                    'fee' => $value['fee'],
                );
            }
            // $all_kmd = array_column($loc_details, 'kmd');
            // $min_distance = min($all_kmd);
            $user_airport_fee=0;
            if(!empty($loc_details)){
             usort($loc_details, array('App\Http\Controllers\ProviderResources\TripController','sortByOrder'));
            $user_airport_fee = ($loc_details[0]['kmd'] * 1000 <= 3218) ? $loc_details[0]['fee'] : 0 ;
             }

            
            $Total += $user_airport_fee;

            if($Total < 0){
                $Total = 0.00; // prevent from negative value
            }

            $Payment = new UserRequestPayment;
            $Payment->request_id = $UserRequest->id;
            $Payment->fixed = $Fixed;
            $Payment->distance = $Distance;
            $Payment->commision = $Commision;
            $Payment->insurance_fee = $service_type->insure_price;
            $Payment->airport_fee = $user_airport_fee;
            $Payment->surge = $Surge;
            if($Discount != 0 && $PromocodeUsage){
                $Payment->promocode_id = $PromocodeUsage->promocode_id;
            }
            $Payment->discount = $Discount;

            if($UserRequest->use_wallet == 1 && $Total > 0){

                $User = User::find($UserRequest->user_id);

                $Wallet = $User->wallet_balance;

                if($Wallet != 0){

                    if($Total > $Wallet) {

                        $Payment->wallet = $Wallet;
                        $Payable = $Total - $Wallet;
                        User::where('id',$UserRequest->user_id)->update(['wallet_balance' => 0 ]);
                        $Payment->total = abs($Payable);

                        // charged wallet money push 
                        (new SendPushNotification)->ChargedWalletMoney($UserRequest->user_id,currency($Wallet));

                    } else {

                        $Payment->total = 0;
                        $WalletBalance = $Wallet - $Total;
                        User::where('id',$UserRequest->user_id)->update(['wallet_balance' => $WalletBalance]);
                        $Payment->wallet = $Total;
                        
                        $Payment->payment_id = 'WALLET';
                        $Payment->payment_mode = $UserRequest->payment_mode;
                        $Payment->paid = 1;

                        $UserRequest->paid = 1;
                        $UserRequest->status = 'COMPLETED';
                        $UserRequest->save();

                        // charged wallet money push 
                        (new SendPushNotification)->ChargedWalletMoney($UserRequest->user_id,currency($Total));
                    }

                }

            } else {
                $Payment->total = abs($Total);
            }

            $Payment->tax = $Tax;
            $Payment->promo_discont=$DiscountTotal; //Koushik
            $Payment->save();

            //$UserRequest->update(['auto_accepted' => 3]);
            $Promotion = $this->check_driver_promotion($UserRequest);
            //$AirportQueue = $this->riding_driver_airport_queue($UserRequest);
            return $Payment;

        } catch (ModelNotFoundException $e) {
            return false;
        }
    }

    public function check_driver_promotion($UserRequest) {

        $ride_finished_at = $UserRequest->finished_at;
        $seconds = strtotime($UserRequest->finished_at) - strtotime($UserRequest->started_at);
        $hours = $seconds/3600;

        //distance
        $details = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$UserRequest->s_latitude.",".$UserRequest->s_longitude."&destinations=".$UserRequest->d_latitude.",".$UserRequest->d_longitude."&mode=driving&sensor=false&key=".env('GOOGLE_MAP_KEY');
        $json = curl($details);
        $details = json_decode($json, TRUE);
        $meter = $details['rows'][0]['elements'][0]['distance']['value'];
        $kilometer = ($meter/1000) / 1.609344;

        try{
            $Promocode = $this->check_expiry();
            
            $promocode_distance = $Promocode->driver_discout_criteria_distance;                
            $promocode_time = $Promocode->driver_discout_criteria_time;

            $PromotionUsages = PromotionUsages::Active()
                ->where('promocode_id', $Promocode->id)
                ->where('provider_id', Auth::user()->id)
                ->first();

            $end_time_driver = ($PromotionUsages) ? Carbon::parse($PromotionUsages->created_at)->addHour($Promocode->driver_discout_criteria_time) : 0 ;

            if (Carbon::parse($ride_finished_at)->between(Carbon::parse($Promocode->created_at), Carbon::parse($Promocode->expiration))){
                if (Carbon::parse($ride_finished_at)->between(Carbon::parse($PromotionUsages->created_at), Carbon::parse($end_time_driver))){
                    $totalduration = $PromotionUsages->duration + $hours;
                    $totaldistance = $PromotionUsages->distance + $kilometer;
                    PromotionUsages::where('promocode_id', $Promocode->id)
                    ->where('provider_id', Auth::user()->id)
                    ->update(['duration' => $totalduration, 'distance' => $totaldistance]);
                    if ($totaldistance >= $promocode_distance && $totalduration >= $promocode_time) {
                        PromotionUsages::where('promocode_id', $Promocode->id)
                            ->where('provider_id', Auth::user()->id)
                            ->update(['status' => "USED"]);
                    }
                }
            }

            return PromotionUsages::Active()
                    ->where('provider_id', Auth::user()->id)
                    ->with('promocode')
                    ->get();

        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    } 

    public function check_expiry(){
        try{
            $Promocode = Promocode::where('user_type', 'DRIVER')
                ->where('activated_fordriver', '1')
                ->where('status', '<>', 'USED')
                ->first();
                if(date("Y-m-d") > $Promocode->expiration){
                    $Promocode->status = 'EXPIRED';
                    $Promocode->save();
                    PromotionUsages::where('promocode_id', $Promocode->id)->update(['status' => 'EXPIRED']);
                }else{
                    PromotionUsages::where('promocode_id', $Promocode->id)->update(['status' => 'ADDED']);
                }

            return $Promocode;
        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

    public function riding_driver_airport_queue($UserRequest){

        try{
            $loc_details=[];
            $latitude = $UserRequest->latitude;
            $longitude = $UserRequest->longitude;
			
			if($UserRequest->has('deviceType'))
			 $deviceType = $UserRequest->deviceType;
			 else
			$deviceType ="";
		
            $locations = LocationFee::orderBy('created_at' , 'desc')->get();

            foreach ($locations as $value) {
                $loc_details[] = array(
                    'kmd' => $this->getDistance($latitude, $longitude, $value['latitude'], $value['longitude']),
                    'location_id' =>$value['id']
                );
            }
            if(!empty($loc_details)){
                usort($loc_details, array('App\Http\Controllers\ProviderResources\TripController','sortByOrder'));
            }
            $oldQueue = DriverAirportQueue::where('provider_id', Auth::user()->id)->get();

			/*********************************** Redius Driver in 2 miles=3218 metter ********************************/
			//$deviceType==='IOS' &&
            if($loc_details[0]['kmd'] * 1000 <= 3218){ //3862 ,3218
           
                if ($oldQueue->isEmpty()) {
                    $DriverQueued = new DriverAirportQueue;
                    $DriverQueued->provider_id = Auth::user()->id;
                    $DriverQueued->location_id = $loc_details[0]['location_id'];
                    $DriverQueued->status = 'QUEUED';
                    $DriverQueued->save();
                    
                          $queno = DriverAirportQueue::where('location_id', $DriverQueued->location_id)->orderBy('updated_at', 'DESC')->get();
                    
                     $message = \PushNotification::Message('You are in airport queue, Your Serial no :'.count($queno),array(
                                    'badge' => 1,
                                    'sound' => 'default',

                                    /*'title' => $request['title'],
                                    'body' => $request['description'], 
                                    'tag' => '1',
                                    'click_action' => 'OPEN_ACTIVITY_1',*/
                                    //'icon' => 'fcm_push_icon'

                                    'actionLocKey' => 'Shipx',
                                    'locKey' => 'localized key',
                                    'locArgs' => array(
                                        'localized args',
                                        'localized args',
                                    ),
                                    'launchImage' => 'image.jpg',
                                    'description' => 'You are in airport queue and your Serial no '.count($queno),
                                    'custom' => array('custom_data' => array(
                                        'shipx' => 'ShipxQueue', 'Queue'
                                    ))
                                ));
                     (new SendPushNotification)->sendPushToProvider(Auth::user()->id,$message);
                    return response()->json(['success' => "Driver added to airport queue"]);
                }else{
                    return response()->json(['success' => "Driver already in a queue"]);
                }
            }else if (($oldQueue->isNotEmpty() && ($loc_details[0]['kmd'] * 1000) >= 3540)){ //4184 ,3540
                // if ($oldQueue->isNotEmpty() && $loc_details[0]['kmd'] * 1000 >= 3540) {
				//if(($deviceType==='IOS' && ($loc_details[0]['kmd'] * 1000 <= 3862 ))||($loc_details[0]['kmd'] * 1000 <= 3218)){
                
                    DriverAirportQueue::where('provider_id', Auth::user()->id)->delete();
                    return response()->json(['success' => "Driver removed from airport queue"]);                
            } //$deviceType==='IOS' &&
        }
        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }
    
    
    /* Airport Quee Check */
    
    public function getairportqueeno(Request $UserRequest){
          try{
            $loc_details=[];
           // $AirportQueue = $this->riding_driver_airport_queue($UserRequest);
            $latitude = $UserRequest->latitude;
            $longitude = $UserRequest->longitude;
            $accuracy=$UserRequest->accuracy;
			
            $locations = LocationFee::orderBy('created_at' , 'desc')->get();
            foreach ($locations as $value) {
                $loc_details[] = array(
                    'kmd' => $this->getDistance($latitude, $longitude, $value['latitude'], $value['longitude']),
                    'location_id' =>$value['id']
                );
            }
            if(!empty($loc_details)){
                usort($loc_details, array('App\Http\Controllers\ProviderResources\TripController','sortByOrder'));
            }
            $oldQueue = DriverAirportQueue::where('provider_id', Auth::user()->id)->get();
                    if ($oldQueue->isNotEmpty()) {
                          $DriverQueued = new DriverAirportQueue;
                            $DriverQueued->provider_id = Auth::user()->id;
                           // $DriverQueued->location_id = $loc_details[0]['location_id'];
                           // $DriverQueued->status = 'QUEUED';
                          $queno = DriverAirportQueue::select('provider_id')->where('location_id', $loc_details[0]['location_id'])->orderBy('updated_at', 'ASC')->get();

                        //$arr = array ($queno);
                        $totalSizeofArray=count($queno);
                        $arr=json_decode($queno, true);
                        $value=json_decode($DriverQueued, true);
                        $key = array_search ($value, $arr);
                       
                        
                        $key = intval($key)+1;
                       
                        //$indexno=array_search('102', $queno);
                       // echo $queno;
                        //echo $DriverQueued;
                       // echo $indexno;
                        return response()->json(['success' => "You are in airport queue. \n \n Your serial no : ".$key." of ".$totalSizeofArray."loc distance: ".$loc_details[0]['kmd']]);
						//return response()->json(['success' => "You are in airport queue. \n \n Your serial no : ".$key." of ".$totalSizeofArray]);
                      
                       
                    }else
                    {

                            // DriverAirportQueue::where('provider_id', Auth::user()->id)->delete();
                            return response()->json(['success' => "You are not in airport queue"."location distance: ".$loc_details[0]['kmd']]);
                        //return response()->json(['success' => $queno]);
                    }

                    // else  if($loc_details[0]['kmd'] * 1000 >= 4023)
                    

                    // {
                    //         DriverAirportQueue::where('provider_id', Auth::user()->id)->delete();
                    //         return response()->json(['success' => "You are not in airport queue"]);
                    // }
                    
                 /*    $message = \PushNotification::Message('You are in airport quee and your Serial no '.count($queno),array(
                                    'badge' => 1,
                                    'sound' => 'default',

                                    'actionLocKey' => 'Shipx title!',
                                    'locKey' => 'localized key',
                                    'locArgs' => array(
                                        'localized args',
                                        'localized args',
                                    ),
                                    'launchImage' => 'image.jpg',
                                    'description' => 'You are in airport quee and your Serial no '.count($queno),
                                    'custom' => array('custom_data' => array(
                                        'Shipx' => 'Shipx', 'Test Quee'
                                    ))
                                ));
              */
              
                    
                
           
        }
        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }
    
    
    
    
    
    

    public function active_driver_airport_queue(Request $request){

        try{
            $loc_details=[]; 
            $latitude = $request->latitude;
            $longitude = $request->longitude;
            $locations = LocationFee::orderBy('created_at' , 'desc')->get();

            foreach ($locations as $value) {
                $loc_details[] = array(
                    'kmd' => $this->getDistance($latitude, $longitude, $value['latitude'], $value['longitude']),
                    'location_id' =>$value['id']
                );
            }
            if(!empty($loc_details)){
                  usort($loc_details, array('App\Http\Controllers\ProviderResources\TripController','sortByOrder'));
            }          
            
            $oldQueue = DriverAirportQueue::where('provider_id', Auth::user()->id)->get();

            if($loc_details[0]['kmd'] * 1000 <= 3218){
                if ($oldQueue->isEmpty()) {
                    $DriverQueued = new DriverAirportQueue;
                    $DriverQueued->provider_id = Auth::user()->id;
                    $DriverQueued->location_id = $loc_details[0]['location_id'];
                    $DriverQueued->status = 'QUEUED';
                    $DriverQueued->save();
                    
                        
                    return response()->json(['success' => "Driver added to airport queue"]);
                }else{
                    // DriverAirportQueue::where('provider_id', Auth::user()->id)->delete();
                    // $DriverQueued = new DriverAirportQueue;
                    // $DriverQueued->provider_id = Auth::user()->id;
                    // $DriverQueued->location_id = $loc_details[0]['location_id'];
                    // $DriverQueued->status = 'QUEUED';
                    // $DriverQueued->save();
                    return response()->json(['error' => "Driver already in a queue"], 500);
                }
            }else{
                if ($oldQueue->isNotEmpty()) {
                    DriverAirportQueue::where('provider_id', Auth::user()->id)->delete();
                    return response()->json(['success' => "Driver removed airport queue"]);
                }else{
                    return response()->json(['error' => "Driver are not in a listed airport area"], 500);
                }
            }
        }
        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }

    /**
     * Get the trip history details of the provider
     *
     * @return \Illuminate\Http\Response
     */
    public function history_details(Request $request)
    {
        $this->validate($request, [
                'request_id' => 'required|integer|exists:user_requests,id',
            ]);

        if($request->ajax()) {
            
            $Jobs = UserRequests::where('id',$request->request_id)
                                ->where('provider_id', Auth::user()->id)
                                ->with('payment','service_type','user','rating')
                                ->get();
            if(!empty($Jobs)){
                $map_icon = asset('asset/img/marker-end.png');
                foreach ($Jobs as $key => $value) {
                    $Jobs[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=640x260".
                            "&maptype=roadmap".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x000000|weight:2|enc:".$value->route_key.
                            "&key=".env('GOOGLE_MAP_KEY');
                }
            }

            return $Jobs;
        }

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function upcoming_trips() {
    
        try{
            $UserRequests = UserRequests::ProviderUpcomingRequest(Auth::user()->id)->get();
            if(!empty($UserRequests)){
                $map_icon = asset('asset/img/marker-end.png');
                foreach ($UserRequests as $key => $value) {
                    $UserRequests[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                                    "autoscale=1".
                                    "&size=640x260".
                                    "&maptype=terrain".
                                    "&format=png".
                                    "&visual_refresh=true".
                                    "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                                    "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                                    "&path=color:0x000000|weight:2|enc:".$value->route_key.
                                    "&key=".env('GOOGLE_MAP_KEY');
                }
            }
            return $UserRequests;
        }

        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }

    /**
     * Get the trip history details of the provider
     *
     * @return \Illuminate\Http\Response
     */
    public function upcoming_details(Request $request)
    {
        $this->validate($request, [
                'request_id' => 'required|integer|exists:user_requests,id',
            ]);

        if($request->ajax()) {
            
            $Jobs = UserRequests::where('id',$request->request_id)
                                ->where('provider_id', Auth::user()->id)
                                ->with('service_type','user')
                                ->get();
            if(!empty($Jobs)){
                $map_icon = asset('asset/img/marker-end.png');
                foreach ($Jobs as $key => $value) {
                    $Jobs[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=640x260".
                            "&maptype=roadmap".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x000000|weight:2|enc:".$value->route_key.
                            "&key=".env('GOOGLE_MAP_KEY');
                }
            }

            return $Jobs;
        }

    }

    /**
     * Get the trip history details of the provider
     *
     * @return \Illuminate\Http\Response
     */
    public function summary(Request $request)
    {
        try{
            if($request->ajax()) {
                $rides = UserRequests::where('provider_id', Auth::user()->id)->count();
                $revenue = UserRequestPayment::whereHas('request', function($query) use ($request) {
                                $query->where('provider_id', Auth::user()->id);
                            })
                        ->sum('total');
                $cancel_rides = UserRequests::where('status','CANCELLED')->where('provider_id', Auth::user()->id)->count();
                $scheduled_rides = UserRequests::where('status','SCHEDULED')->where('provider_id', Auth::user()->id)->count();

                return response()->json([
                    'rides' => $rides, 
                    'revenue' => $revenue,
                    'cancel_rides' => $cancel_rides,
                    'scheduled_rides' => $scheduled_rides,
                ]);
            }

        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }

    }


    /**
     * help Details.
     *
     * @return \Illuminate\Http\Response
     */

    public function help_details(Request $request){

        try{

            if($request->ajax()) {
                return response()->json([
                    'contact_number' => Setting::get('contact_number',''), 
                    'contact_email' => Setting::get('contact_email','')
                     ]);
            }

        }catch (Exception $e) {
            if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')]);
            }
        }
    }
    /**
     * Confirm upcoming scheduled request after push.
     *
     * @return \Illuminate\Http\Response
     */
    public function scride_confirm(Request $request)
    {
        $Jobs = UserRequests::where('id',$request->request_id)
                                ->where('provider_id', Auth::user()->id)
                                ->with('service_type','user')
                                ->get();
        $JobsArr = $Jobs->toArray()[0];
        $this->validate($request, [
            'request_id' => 'required|integer|exists:user_requests,id',
        ]);
        try{

            DB::table('user_requests')
                        ->where('id',$JobsArr["id"])
                        ->update(['status' => 'STARTED', 'assigned_at' =>Carbon::now() , 'schedule_at' => null ]);

            DB::table('provider_services')
                        ->where('provider_id',$JobsArr["provider_id"])
                        ->update(['status' =>'riding']);
            //scehule start request push to user
            (new SendPushNotification)->user_schedule_cron($JobsArr["user_id"]);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Something went wrong']);
        }

    }

    /**
     * Show ride requests nearby the current destination.
     *
     * @return \Illuminate\Http\Response
     */

    public function ride_demands(Request $request) {
    
        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $ProviderService = ProviderService::where('provider_id', Auth::user()->id)->get();
        $service_type = $ProviderService->toArray()[0]['service_type_id'];
        $distance = Setting::get('provider_demand_search_radius', '1');
        try{
            $ProviderCheckRideDemands = UserRequests::select('*', DB::Raw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(s_latitude) ) * cos( radians(s_longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(s_latitude) ) ) ) AS distance"))
            //->having('distance', '<', $distance)
            //->groupBy('distance')
            ->havingRaw("distance < ?", [$distance])
			->whereIn('status' , ['SEARCHING', 'SCHEDULED'])
			->where('service_type_id',$service_type)
			->orderBy('distance', 'asc')
			->limit(10)
            ->get();
            
            if(!empty($ProviderCheckRideDemands)){
                //return ($ProviderCheckRideDemands);
                //return response()->json(['ProviderCheckRideDemands' => (object)(($ProviderCheckRideDemands->toArray()))]);
                return response()->json(['ProviderCheckRideDemands' => $ProviderCheckRideDemands]);
            }
        }

        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }
    

    /*  **********************  Driver Destination Towards Point Check ***********************/
    
    
    public function GetTowardsDestination(Request $request)
    {
         $oldexist = DriverTowardsDestination::select('d_lat','d_lng','d_address','isenable')->where('provider_id', Auth::user()->id)->get();   
         if ($oldexist->isEmpty()) {
             return response()->json(['success' => false,'message' => "No Data Avilable",'data'=>$oldexist
                                    ]);
         }else
         {
             //print_r($oldexist);
            // die();
             return response()->json(['success' => true,'message' => "Data Avilable",'data'=>$oldexist]);
            //  return response()->json(['success' => "sucess",
            //                          'data'=>"" ]);
            
         }
    }
    


    public function deleteTowards(Request $request)
    {
         DriverTowardsDestination::where('provider_id', Auth::user()->id)->delete();
          return response()->json(['success' => "Your Towards Destination Delete Successfully."]);
    }
    





    public function update_towards_destination(Request $request)
    {
         try{
            // $s_latitude = $request->s_latitude;
            // $s_longitude = $request->s_longitude;
            // $s_address=$request->s_address;
             
            $d_latitude = $request->d_latitude;
            $d_longitude = $request->d_longitude;
            $d_address = $request->d_address;
             // $polygonpath=$request->polygonpath;
             
           
            $oldexist = DriverTowardsDestination::where('provider_id', Auth::user()->id)->get();
                if ($oldexist->isNotEmpty()) {
                    DriverTowardsDestination::where('provider_id', Auth::user()->id)->delete();
                     $Drivertowards = new DriverTowardsDestination;
                    $Drivertowards->provider_id = Auth::user()->id;
                    // $Drivertowards->s_lat = $s_latitude;
                    // $Drivertowards->s_lng = $s_longitude;
                    $Drivertowards->d_lat = $d_latitude;
                    $Drivertowards->d_lng = $d_longitude;
                    // $Drivertowards->s_address = $s_address;
                    // $Drivertowards->poligons=DB::raw("ST_GeomFromText('$polygonpath')");
                    $Drivertowards->d_address = $d_address;
                    $Drivertowards->isenable = false;
                    $Drivertowards->save();
                    return response()->json(['success' => "Driver information updated."]);
                }else
                     if ($oldexist->isEmpty()) {
                         // $plo = 'POLYGON((22.572872 88.437170),(22.576524 88.479634))';
                       //  $polygons='ST_GeomFromText(POLYGON((22.572872 88.437170),(22.576524 88.479634)))';
                        
                         
                    $Drivertowards = new DriverTowardsDestination;
                    $Drivertowards->provider_id = Auth::user()->id;
                    // $Drivertowards->s_lat = $s_latitude;
                    // $Drivertowards->s_lng = $s_longitude;
                    $Drivertowards->d_lat = $d_latitude;
                    $Drivertowards->d_lng = $d_longitude;
                    // $Drivertowards->s_address = $s_address;
                    // $Drivertowards-> poligons=DB::raw("ST_GeomFromText('$polygonpath')");
                    $Drivertowards->d_address = $d_address;
                    $Drivertowards->isenable = false;
                    $Drivertowards->save();
   
                    return response()->json(['success' => "Driver desitnation updated"]);
                }
            
        }
        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }
    
        /*  **********************  END Driver Destination Towards Point Check ***********************/

     public function UpdateDeviceToken(Request $request)
    {

        $device_token=$request->token;
        
        
         try {
              if($request->has('token')){ 
                $user = DB::table('provider_devices')->where('provider_id', Auth::user()->id)->update(['token' => $device_token]);
              
                 
               
                 return response()->json(['message' => "Device Token Updated"]);
            }
         } catch (Exception $e) {
                         return response()->json(['error' => trans('api.something_went_wrong')], 500);

         }
         
            
       
    }
    public function testPushAndroidProvider(Request $request)
    {
        echo 'lujughd';die;
         
            
       
    }


    public function signature(Request $request){
        //echo 10; exit;
        try{
            $this->validate($request,[
            'request_id' => 'required',
            'parcel_id' => 'required',
            'signature_file' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
        ]);
        if($request->hasFile('signature_file')) {
           // echo 11; exit;
            $upload_signature_path =$request->signature_file->store('public/provider/signature');
            $upload_signature_path= str_replace("public/", "", $upload_signature_path);
          
            DB::update('update parcelDetails set upload_signature_path = ? where id = ?',[$upload_signature_path,$request->parcel_id]);
        }
        else{
            return response()->json(['message' => "File Not Found"],404);
        }
    }
    catch (Exception $e) {
        return response()->json(['error' => trans('api.something_went_wrong')], 500);
    }
}



   
}
