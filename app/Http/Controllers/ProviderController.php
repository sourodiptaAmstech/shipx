<?php

namespace App\Http\Controllers;

use Auth;
use App\Provider;
use App\Promocode;
use Carbon\Carbon;
use App\UserRequests;
use App\RequestFilter;
use App\PromotionUsages;

use Illuminate\Http\Request;
use App\Http\Controllers\ProviderResources\TripController;

class ProviderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('provider', ['except' => ['earnings']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('provider.index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function incoming(Request $request)
    {
        return (new TripController())->index($request);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function accept(Request $request, $id)
    {
        return (new TripController())->accept($request, $id);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function reject($id)
    {
        return (new TripController())->destroy($id);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        return (new TripController())->update($request, $id);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function rating(Request $request, $id)
    {
        return (new TripController())->rate($request, $id);
    }

    public function check_expiry(){
        try{
            $Promocode = Promocode::where('user_type', 'DRIVER')
                ->where('activated_fordriver', '1')
                ->first();
                if($Promocode != null){

                    if(date("Y-m-d") > $Promocode->expiration){
                        $Promocode->status = 'EXPIRED';
                        $Promocode->save();
                        PromotionUsages::where('promocode_id', $Promocode->id)->where('status', '<>', 'USED')->update(['status' => 'EXPIRED']);
                    }else{
                        PromotionUsages::where('promocode_id', $Promocode->id)->where('status', '<>', 'USED')->update(['status' => 'ADDED']);
                    }
                
                    return $Promocode;

                }else{
                    return false;
                }
        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function earnings(Request $request)
    {
        $this->validate($request, [
            'range' => 'in:weekly,monthly,daily,all',
        ]);
 //var_dump($request);die;
        if($request->has('app')) {
            $Provider = Auth::user();
        } else {
            $Provider = \Auth::guard('provider')->user();
        }
        
        $Promocode = $this->check_expiry();
        if($Promocode){
            $PromotionUsages = PromotionUsages::with('promocode')
                    //->where('promocode_id', $Promocode->id)
                    ->where('provider_id', Auth::user()->id)
                    ->where('status', 'USED')
                    ->get();
        }else{
            $PromotionUsages = [];
        }
        $provider_id = $Provider->id;

        $provider = Provider::where('id',$provider_id)
                    ->with('service','accepted','cancelled')
                    ->get();

        $weekly = UserRequests::where('provider_id',$provider_id)
                    ->with('payment','service_type')
                    ->where('created_at', '>=', Carbon::now()->subWeekdays(7))
                    ->get();

        $today = UserRequests::where('provider_id',$provider_id)
                    ->with('payment','service_type')
                    ->where('created_at', '>=', Carbon::today())
                    ->get();
        $todayridecount = $today->count();

        $fully = UserRequests::where('provider_id',$provider_id)
                    ->with('payment','service_type')
                    ->get();

        if($request->has('app')) {

            switch ($request->range) {
                case 'weekly':
                    $weeklyridecount = $weekly->count();
                    $weekly_earning = 0;
                    foreach($weekly as $key => $each){
                        $map_icon = asset('asset/marker.png');
                        $weekly[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$each->s_latitude.",".$each->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$each->d_latitude.",".$each->d_longitude.
                            "&path=color:0x000000|weight:3|enc:".$each->route_key.
                            "&key=".env('GOOGLE_MAP_KEY');
                        
                        if($each->payment != ""){
                            $each_sum = $each->payment->tax + $each->payment->fixed + $each->payment->distance ;//+ $each->payment->commision;
                            $weekly_earning += $each_sum;
                        }
                    }
                    return response()->json([
                        'weeklyridecount' => $weeklyridecount,
                        'weekly_earning' => currency($weekly_earning),
                        'weekly' => $weekly,
                        'PromotionUsages' => $PromotionUsages,
                    ]);
                    break;
                case 'monthly':
                    $monthly = UserRequests::where('provider_id',$provider_id)
                        ->with('payment')
                        ->where('created_at', '>=', Carbon::now()->subMonth())
                        ->get();
                    $monthlyridecount = $monthly->count();
                    $monthly_earning = 0;
                    foreach($monthly as $k1 => $each){

                        $map_icon = asset('asset/marker.png');
                        $monthly[$k1]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$each->s_latitude.",".$each->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$each->d_latitude.",".$each->d_longitude.
                            "&path=color:0x000000|weight:3|enc:".$each->route_key.
                            "&key=".env('GOOGLE_MAP_KEY');
                        
                        if($each->payment != ""){
                            $each_sum = $each->payment->tax + $each->payment->fixed + $each->payment->distance ;//+ $each->payment->commision;
                            $monthly_earning += $each_sum;
                        }
                    }
                    return response()->json([
                        'monthlyridecount' => $monthlyridecount,
                        'monthly_earning' => currency($monthly_earning),
                        'monthly' => $monthly,
                        'PromotionUsages' => $PromotionUsages,
                    ]);
                    break;
                case 'daily':
                    
                    $today_earning = 0;
                    foreach($today as $k2=>$each){
                        
                        $map_icon = asset('asset/marker.png');
                        $today[$k2]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$each->s_latitude.",".$each->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$each->d_latitude.",".$each->d_longitude.
                            "&path=color:0x000000|weight:3|enc:".$each->route_key.
                            "&key=".env('GOOGLE_MAP_KEY');

                        if($each->payment != ""){
                            $each_sum = $each->payment->tax + $each->payment->fixed + $each->payment->distance;// + $each->payment->commision;
                            $today_earning += $each_sum;
                        }
                    }
                    return response()->json([
                        'todayridecount' => $todayridecount,
                        'today_earning' => currency($today_earning),
                        'today' => $today,
                        'PromotionUsages' => $PromotionUsages,
                    ]);
                    break;
                case 'all':
                    $totalridecount = $fully->count();
                    $total_earning = 0;
                    foreach($fully as $k3=>$each){
                        

                        $map_icon = asset('asset/marker.png');
                        $fully[$k3]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=320x130".
                            "&maptype=terrian".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$each->s_latitude.",".$each->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$each->d_latitude.",".$each->d_longitude.
                            "&path=color:0x000000|weight:3|enc:".$each->route_key.
                            "&key=".env('GOOGLE_MAP_KEY');

                        if($each->payment != ""){
                            $each_sum = $each->payment->tax + $each->payment->fixed + $each->payment->distance;// + $each->payment->commision;
                            $total_earning += $each_sum;
                        }
                    }
                    return response()->json([
                        'totalridecount' => $totalridecount,
                        'total_earning' => currency($total_earning),
                        'all' => $fully,
                        'PromotionUsages' => $PromotionUsages,
                    ]);
                    break;

                default:
                    return response()->json([
                        'error' => "No data found",
                    ]);
                    break;
            }

        } else {
            return view('provider.payment.earnings',compact('provider','weekly','fully','todayridecount'));
        }
    }

    /**
     * available.
     *
     * @return \Illuminate\Http\Response
     */
    public function available(Request $request)
    {
        (new ProviderResources\ProfileController)->available($request);
        return back();
    }

    /**
     * Show the application change password.
     *
     * @return \Illuminate\Http\Response
     */
    public function change_password()
    {
        return view('provider.profile.change_password');
    }

    /**
     * Change Password.
     *
     * @return \Illuminate\Http\Response
     */
    public function update_password(Request $request)
    {
        $this->validate($request, [
                'password' => 'required|confirmed',
                'old_password' => 'required',
            ]);

        $Provider = \Auth::user();

        if(password_verify($request->old_password, $Provider->password))
        {
            $Provider->password = bcrypt($request->password);
            $Provider->save();

            return back()->with('flash_success','Password changed successfully!');
        } else {
            return back()->with('flash_error','Please enter correct password');
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function location_edit()
    {
        return view('provider.location.index');
    }

    /**
     * Update latitude and longitude of the user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function location_update(Request $request)
    {
        $this->validate($request, [
                'latitude' => 'required|numeric',
                'longitude' => 'required|numeric',
            ]);

        if($Provider = \Auth::user()){

            $Provider->latitude = $request->latitude;
            $Provider->longitude = $request->longitude;
            $Provider->save();

            return back()->with(['flash_success' => 'Location Updated successfully!']);

        } else {
            return back()->with(['flash_error' => 'Provider Not Found!']);
        }
    }

    /**
     * upcoming history.
     *
     * @return \Illuminate\Http\Response
     */
    public function upcoming_trips()
    {
        $fully = (new ProviderResources\TripController)->upcoming_trips();
        return view('provider.payment.upcoming',compact('fully'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function cancel(Request $request) {
        try{
            (new TripController)->cancel($request);
            return back();
        } catch (ModelNotFoundException $e) {
            return back()->with(['flash_error' => "Something Went Wrong"]);
        }
    }
}