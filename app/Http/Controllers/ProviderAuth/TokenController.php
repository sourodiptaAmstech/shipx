<?php

namespace App\Http\Controllers\ProviderAuth;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Http\Controllers\Controller;

use Tymon\JWTAuth\Exceptions\JWTException;
use App\Notifications\ResetPasswordOTP;

use DB;
use Auth;
use Config;
use JWTAuth;
use Setting;
use Notification;
use Validator;
use Socialite;

use App\Provider;
use App\ProviderDevice;
use App\ProviderService;
use App\ServiceType;


class TokenController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
 public function terms_conditions(Request $require )
    {
        //echo 10; exit;
        $page_privacy = DB::table('settings')->select('value')->where('key', 'page_privacy')->first();
        $condition_privacy = DB::table('settings')->select('value')->where('key', 'condition_privacy')->first();
       
       //print_r($user);
       return response()->json(['response' => 'success','privecy'=>$page_privacy->value,'terms_conditions'=>$condition_privacy->value]); 
    }

    public function register(Request $request)
    {
        $this->validate($request, [
                'device_id' => 'required',
                'device_type' => 'required|in:android,ios,web',
                'device_token' => 'required',
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:providers',
                'mobile' => 'required',
                'password' => 'required|min:6|confirmed',
                'service_type.*' => 'required',
                'service_number' => 'required',
                'service_model' => 'required'
            ]);

        try{
            $Provider = $request->all();
            $Provider['password'] = bcrypt($request->password);

            $Provider = Provider::create($Provider);

            $service_id = explode(',', $request->service_type);

            //return $service_id;

            foreach ($service_id as $value) {
            $Service = new ProviderService;
            $Service->provider_id = $Provider->id;
            $Service->service_type_id = $value;
            $Service->status = 'active';
            $Service->service_number = $request->service_number;
            $Service->service_model = $request->service_model;

            $Service->save();

            }

            if(Setting::get('demo_mode', 0) == 1) {
                $Provider->update(['status' => 'approved']);
                
            }

            ProviderDevice::create([
                    'provider_id' => $Provider->id,
                    'udid' => $request->device_id,
                    'token' => $request->device_token,
                    'type' => $request->device_type,
                ]);
            
            return response()->json(['message' => 'Provider Register Successfully' ,'provider' =>$Provider , 
                             'service' =>$Service]);

        } catch (QueryException $e) {
            if ($request->ajax() || $request->wantsJson()) {
                return response()->json(['error' => 'Something went wrong, Please try again later!'], 500);
            }
            return abort(500);
        }
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function authenticate(Request $request)
    {
        $this->validate($request, [
                'device_id' => 'required',
                'device_type' => 'required|in:android,ios,web',
                'device_token' => 'required',
                'email' => 'required|email',
                'password' => 'required|min:6',
            ]);

        Config::set('auth.providers.users.model', 'App\Provider');

        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'The email address or password you entered is incorrect.'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'Something went wrong, Please try again later!'], 500);
        }

        $User = Provider::with('service', 'device')->find(Auth::user()->id);

        $User->access_token = $token;
        $User->currency = Setting::get('currency', '$');
        $User->sos = Setting::get('sos_number', '911');

        if($User->device) {
            ProviderDevice::where('id',$User->device->id)->update([
        
                'udid' => $request->device_id,
                'token' => $request->device_token,
                'type' => $request->device_type,
            ]);
            
        } else {
            ProviderDevice::create([
                    'provider_id' => $User->id,
                    'udid' => $request->device_id,
                    'token' => $request->device_token,
                    'type' => $request->device_type,
                ]);
        }
		
		if($User->avatar===null || $User->avatar===""){
            $User->avatar="";
        }
        if($User->latitude===null || $User->latitude===""){
            $User->latitude="";
        }
        if($User->longitude===null || $User->longitude===""){
            $User->longitude="";
        }
        if($User->social_unique_id===null || $User->social_unique_id===""){
            $User->social_unique_id="";
        }

        return response()->json($User);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function logout(Request $request)
    {
        try {
            ProviderDevice::where('provider_id', $request->id)->update(['udid'=> '', 'token' => '']);
            ProviderService::where('provider_id',$request->id)->update(['status' => 'offline']);
            return response()->json(['message' => trans('api.logout_success')]);
        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

/**
     * Select services.
     *
     * @return \Illuminate\Http\Response
     */

    public function service(Request $request)
    {
        try {
            $service = ServiceType::get();
            return response()->json(['data' => $service , 'message' => trans('api.service_success')]);
        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }
 /**
     * Forgot Password.
     *
     * @return \Illuminate\Http\Response
     */


    public function forgot_password(Request $request){
		
    //    return response()->json(['error' => trans('api.something_went_wrong')], 500);
        $this->validate($request, [
                'email' =>'required|email',
            ]);
           
        
        try{  
            
            $provider = Provider::where('email' , $request->email)->first();
            if(empty($provider)){
                return response()->json(['error' => "User not found."], 500);
            
            }
            $otp = mt_rand(100000, 999999);

            $provider->otp = $otp;
            $provider->save();
         
            //Notification::send($provider, new ResetPasswordOTP($otp));

			$provider->notify(new ResetPasswordOTP($otp));
    
			
			
			
			
			
			

            return response()->json([
                'message' => 'OTP sent to your email!',
                'provider' => $provider
            ]);

        }catch(Exception $e){
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }


    /**
     * Reset Password.
     *
     * @return \Illuminate\Http\Response
     */

    public function reset_password(Request $request){

        $this->validate($request, [
                'password' => 'required|confirmed|min:6',
                'id' => 'required|numeric|exists:providers,id'
            ]);

        try{

            $Provider = Provider::findOrFail($request->id);
            $Provider->password = bcrypt($request->password);
            $Provider->save();

            if($request->ajax()) {
                return response()->json(['message' => 'Password Updated']);
            }

        }catch (Exception $e) {
            if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')]);
            }
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function facebookViaAPI(Request $request) { 
     //   echo 285; exit;
        $validator = Validator::make(
            $request->all(),
            [
                'device_type' => 'required|in:android,ios,web',
                'device_token' => 'required',
                'accessToken'=>'required',
                'device_id' => 'required',
                'login_by' => 'required|in:manual,facebook,google',
                'avatar' => 'required',
                'social_unique_id' => 'required',
                'email' => 'required',
                'name' => 'required'
            ]
        );
        
        if($validator->fails()) {
            return response()->json(['status'=>false,"ujd"=>"100",'message' => $validator->messages()->all()]);
        }
        // $user = Socialite::driver('facebook')->stateless();
        // $FacebookDrive = $user->userFromToken( $request->accessToken);
       
        try{
            $FacebookSql = Provider::where('social_unique_id',$request->social_unique_id);
            if($request->email !=""){
                $FacebookSql->orWhere('email',$request->email);
            }
            $AuthUser = $FacebookSql->first();
            if($AuthUser){ 
                $AuthUser->social_unique_id=$request->social_unique_id;
                $AuthUser->login_by="facebook";
                $AuthUser->save();  
            }else{   
                $AuthUser["email"]=$request->email;
                $name = explode(' ', $request->name, 2);
                $AuthUser["first_name"]=$name[0];
                $AuthUser["last_name"]=isset($name[1]) ? $name[1] : '';
                $AuthUser["password"]=bcrypt($request->social_unique_id);
                $AuthUser["social_unique_id"]=$request->social_unique_id;
                $AuthUser["avatar"]=$request->avatar;
                $AuthUser["login_by"]="facebook";

                // Optional Params 
                if(isset($request->stripe_cust_id) && !empty($request->stripe_cust_id)){
                    $AuthUser["stripe_cust_id"]=$request->stripe_cust_id;
                }
                if(isset($request->mobile) && !empty($request->mobile)){
                    $AuthUser["mobile"]=$request->mobile;
                }
                if(isset($request->longitude) && !empty($request->longitude)){
                    $AuthUser["longitude"]=$request->longitude;
                }
                if(isset($request->latitude) && !empty($request->latitude)){
                    $AuthUser["latitude"]=$request->latitude;
                }
                $AuthUser = Provider::create($AuthUser);

                if(Setting::get('demo_mode', 0) == 1) {
                    $AuthUser->update(['status' => 'approved']);
                    ProviderService::create([
                        'provider_id' => $AuthUser->id,
                        'service_type_id' => '1',
                        'status' => 'active',
                        'service_number' => '4pp03ets',
                        'service_model' => 'Audi R8',
                    ]);
                }
            }    
            if($AuthUser){ 
                $userToken = JWTAuth::fromUser($AuthUser);
                $User = Provider::with('service', 'device')->find($AuthUser->id);
                if($User->device) {
                    ProviderDevice::where('id',$User->device->id)->update([
                        
                        'udid' => $request->device_id,
                        'token' => $request->device_token,
                        'type' => $request->device_type,
                    ]);
                    
                } else {
                    ProviderDevice::create([
                        'provider_id' => $User->id,
                        'udid' => $request->device_id,
                        'token' => $request->device_token,
                        'type' => $request->device_type,
                    ]);
                }
                return response()->json([
                            "status" => true,
                            "token_type" => "Bearer",
                            "access_token" => $userToken,
                            'currency' => Setting::get('currency', '$'),
                            'sos' => Setting::get('sos_number', '911')
                        ]);
            }else{
                return response()->json(['status'=>false,'message' => "Invalid credentials!"]);
            }  
        } catch (Exception $e) {
           
            return response()->json(['status'=>false,"ujd"=>"1001",'message' => trans('api.something_went_wrong')]);
        }
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function appleViaAPI(Request $request) { 
        //   echo 285; exit;
           $validator = Validator::make(
               $request->all(),
               [
                   'device_type' => 'required|in:android,ios,web',
                   'device_token' => 'required',
                 //'accessToken'=>'required',
                   'device_id' => 'required',
                   'login_by' => 'required|in:apple',
                 //  'avatar' => 'required',
                   'social_unique_id' => 'required',
                   'email' => 'required',
                   'name' => 'required'
               ]
           );
           
           if($validator->fails()) {
               return response()->json(['status'=>false,"ujd"=>"100",'message' => $validator->messages()->all()]);
           }
           // $user = Socialite::driver('facebook')->stateless();
           // $FacebookDrive = $user->userFromToken( $request->accessToken);
          
           try{
               $FacebookSql = Provider::where('social_unique_id',$request->social_unique_id);
               if($request->email !=""){
                   $FacebookSql->orWhere('email',$request->email);
               }
               $AuthUser = $FacebookSql->first();
               if($AuthUser){ 
                   $AuthUser->social_unique_id=$request->social_unique_id;
                   $AuthUser->login_by="apple";
                   $AuthUser->save();  
               }else{   
                   $AuthUser["email"]=$request->email;
                   $name = explode(' ', $request->name, 2);
                   $AuthUser["first_name"]=$name[0];
                   $AuthUser["last_name"]=isset($name[1]) ? $name[1] : '';
                   $AuthUser["password"]=bcrypt($request->social_unique_id);
                   $AuthUser["social_unique_id"]=$request->social_unique_id;
                   $AuthUser["avatar"]="";
                   $AuthUser["login_by"]="apple";
   
                   // Optional Params 
                   if(isset($request->stripe_cust_id) && !empty($request->stripe_cust_id)){
                       $AuthUser["stripe_cust_id"]=$request->stripe_cust_id;
                   }
                   if(isset($request->mobile) && !empty($request->mobile)){
                       $AuthUser["mobile"]=$request->mobile;
                   }
                   if(isset($request->longitude) && !empty($request->longitude)){
                       $AuthUser["longitude"]=$request->longitude;
                   }
                   if(isset($request->latitude) && !empty($request->latitude)){
                       $AuthUser["latitude"]=$request->latitude;
                   }
                   $AuthUser = Provider::create($AuthUser);
   
                   if(Setting::get('demo_mode', 0) == 1) {
                       $AuthUser->update(['status' => 'approved']);
                       ProviderService::create([
                           'provider_id' => $AuthUser->id,
                           'service_type_id' => '1',
                           'status' => 'active',
                           'service_number' => '4pp03ets',
                           'service_model' => 'Audi R8',
                       ]);
                   }
               }    
               if($AuthUser){ 
                   $userToken = JWTAuth::fromUser($AuthUser);
                   $User = Provider::with('service', 'device')->find($AuthUser->id);
                   if($User->device) {
                       ProviderDevice::where('id',$User->device->id)->update([
                           
                           'udid' => $request->device_id,
                           'token' => $request->device_token,
                           'type' => $request->device_type,
                       ]);
                       
                   } else {
                       ProviderDevice::create([
                           'provider_id' => $User->id,
                           'udid' => $request->device_id,
                           'token' => $request->device_token,
                           'type' => $request->device_type,
                       ]);
                   }
                   return response()->json([
                               "status" => true,
                               "token_type" => "Bearer",
                               "access_token" => $userToken,
                               'currency' => Setting::get('currency', '$'),
                               'sos' => Setting::get('sos_number', '911')
                           ]);
               }else{
                   return response()->json(['status'=>false,'message' => "Invalid credentials!"]);
               }  
           } catch (Exception $e) {
              
               return response()->json(['status'=>false,"ujd"=>"1001",'message' => trans('api.something_went_wrong')]);
           }
       }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function googleViaAPI(Request $request) { 

        $validator = Validator::make(
            $request->all(),
            [
                'device_type' => 'required|in:android,ios,web',
                'device_token' => 'required',
                'accessToken'=>'required',
                'device_id' => 'required',
                'login_by' => 'required|in:manual,facebook,google',
                'avatar' => 'required',
                'social_unique_id' => 'required',
                'email' => 'required',
                'name' => 'required'
            ]
        );
        
        if($validator->fails()) {
            return response()->json(['status'=>false,'message' => $validator->messages()->all()]);
        }
        // $user = Socialite::driver('google')->stateless();
        // $GoogleDrive = $user->userFromToken( $request->accessToken);
       
        try{
            $GoogleSql = Provider::where('social_unique_id',$request->social_unique_id);
            if($request->email !=""){
                $GoogleSql->orWhere('email',$request->email);
            }
            $AuthUser = $GoogleSql->first();
            if($AuthUser){
                $AuthUser->social_unique_id=$request->social_unique_id;  
                $AuthUser->login_by="google";
                $AuthUser->save();
            }else{   
                $AuthUser["email"]=$request->email;
                $name = explode(' ', $request->name, 2);
                $AuthUser["first_name"]=$name[0];
                $AuthUser["last_name"]=isset($name[1]) ? $name[1] : '';
                $AuthUser["password"]=($request->social_unique_id);
                $AuthUser["social_unique_id"]=$request->social_unique_id;
                $AuthUser["avatar"]=$request->avatar;
                $AuthUser["login_by"]="google";

                // Optional Params 
                if(isset($request->stripe_cust_id) && !empty($request->stripe_cust_id)){
                    $AuthUser["stripe_cust_id"]=$request->stripe_cust_id;
                }
                if(isset($request->mobile) && !empty($request->mobile)){
                    $AuthUser["mobile"]=$request->mobile;
                }
                if(isset($request->longitude) && !empty($request->longitude)){
                    $AuthUser["longitude"]=$request->longitude;
                }
                if(isset($request->latitude) && !empty($request->latitude)){
                    $AuthUser["latitude"]=$request->latitude;
                }
                
                $AuthUser = Provider::create($AuthUser);

                if(Setting::get('demo_mode', 0) == 1) {
                    $AuthUser->update(['status' => 'approved']);
                    ProviderService::create([
                        'provider_id' => $AuthUser->id,
                        'service_type_id' => '1',
                        'status' => 'active',
                        'service_number' => '4pp03ets',
                        'service_model' => 'Audi R8',
                    ]);
                }
            }    
            if($AuthUser){
                $userToken = JWTAuth::fromUser($AuthUser);
                $User = Provider::with('service', 'device')->find($AuthUser->id);
                if($User->device) {
                    ProviderDevice::where('id',$User->device->id)->update([
                        
                        'udid' => $request->device_id,
                        'token' => $request->device_token,
                        'type' => $request->device_type,
                    ]);
                    
                } else {
                    ProviderDevice::create([
                        'provider_id' => $User->id,
                        'udid' => $request->device_id,
                        'token' => $request->device_token,
                        'type' => $request->device_type,
                    ]);
                }
                return response()->json([
                            "status" => true,
                            "token_type" => "Bearer",
                            "access_token" => $userToken,
                            'currency' => Setting::get('currency', '$'),
                            'sos' => Setting::get('sos_number', '911')
                        ]);
            }else{
                return response()->json(['status'=>false,'message' => "Invalid credentials!"]);
            }  
        } catch (Exception $e) {
            return response()->json(['status'=>false,'message' => trans('api.something_went_wrong')]);
        }
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function refresh_token(Request $request)
    {

        Config::set('auth.providers.users.model', 'App\Provider');

        $Provider = Provider::with('service', 'device')->find(Auth::user()->id);

        try {
            if (!$token = JWTAuth::fromUser($Provider)) {
                return response()->json(['error' => 'Unauthenticated'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'Something went wrong'], 500);
        }

        $Provider->access_token = $token;

        return response()->json($Provider);
    }
}
