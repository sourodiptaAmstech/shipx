<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\SendPushNotification;

use Stripe\Charge;
use Stripe\Stripe;
use Stripe\StripeInvalidRequestError;

use Auth;
use Setting;
use Exception;

use App\Card;
use App\Provider;
use App\User;
use App\UserRequests;
use App\UserRequestPayment;
use App\ServiceType;

class PaymentController extends Controller
{
       /**
     * payment for user.
     *
     * @return \Illuminate\Http\Response
     */
    public function payment(Request $request)
    {
       
       

        $this->validate($request, [
                'tips' => 'numeric',
                'request_id' => 'required|exists:user_request_payments,request_id|exists:user_requests,id,paid,0,user_id,'.Auth::user()->id
            ]);


        $UserRequest = UserRequests::find($request->request_id);
        $service_type_id=$UserRequest->service_type_id;
        $distance=$UserRequest->distance;
        $serviceType=ServiceType::where("id", $service_type_id)->first();
        $driverPerMiles=$serviceType->minute;
        $userperMiles=$serviceType->price;
        $Provide=Provider::find($UserRequest->provider_id);
       
      //  print_r($Provide); exit;

        if($UserRequest->payment_mode == 'CARD') {

            $RequestPayment = UserRequestPayment::where('request_id',$request->request_id)->first();
           

            if($request->has('tips')){
              $RequestPayment->tips = (float)$request->tips;
              $RequestPayment->total += (float)$request->tips;
              $RequestPayment->save();
            }
           
            $StripeCharge = $RequestPayment->total * 100;

            try {

                $Card = Card::where('user_id',Auth::user()->id)->where('is_default',1)->first();
                Stripe::setApiKey(Setting::get('stripe_secret_key'));
               
              /*  $Charge = Charge::create(array(
                      "amount" => $StripeCharge,
                      "currency" => "usd",
                      "customer" => Auth::user()->stripe_cust_id,
                      "card" => $Card->card_id,
                      "description" => "Payment Charge for ".Auth::user()->email,
                      "receipt_email" => Auth::user()->email
                    ));
*/

// driver decuction of stripe fee 

  $stripeFee =Setting::get('stripe_deduction_percentage');
  $stripeFixFee=Setting::get('stripe_flat_deduction_fee');
  $Taxable=0;
  $tax=Setting::get('tax_percentage');
  if($tax>0){
       $Taxable= round(((($StripeCharge/100)*$tax)/100),2)*100; 
    } 
  $adminCommition=Setting::get('commission_percentage');
  $stripeFee= round(((($StripeCharge/100)*$stripeFee)/100)+$stripeFixFee,2)*100; 
  $adminCommitionPerMiles=round(((($StripeCharge/100)*$adminCommition)/100),2)*100;
  $decution=$stripeFee+$adminCommitionPerMiles+ $Taxable;

  $Charge = Charge::create(array(
                        "amount" => $StripeCharge,
                        "currency" => "usd",
                        "customer" => Auth::user()->stripe_cust_id,
                        "description" => "Payment Charge for ".Auth::user()->email,
                        "receipt_email" => Auth::user()->email,
                        "transfer_data" => [
                            "amount" => $StripeCharge-$decution,
                            "destination" => $Provide->vStripeCusId
                          ],
                    ));
                $RequestPayment->payment_id = $Charge["id"];
                $RequestPayment->payment_mode = 'CARD';
                $RequestPayment->save();

                $UserRequest->paid = 1;
              //  $UserRequest->status = 'COMPLETED';
                $UserRequest->save();

                if($request->ajax()) {
                   return response()->json(['message' => trans('api.paid')]); 
                } else {
                    return redirect('dashboard')->with('flash_success','Paid');
                }

            } catch(StripeInvalidRequestError $e){
                if($request->ajax()){
                    return response()->json(['error' => $e->getMessage()], 500);
                } else {
                    return back()->with('flash_error', $e->getMessage());
                }
            } catch(Exception $e) {
                if($request->ajax()){
                    return response()->json(['error' => $e->getMessage()], 500);
                } else {
                    return back()->with('flash_error', $e->getMessage());
                }
            }
        }
    }


    /**
     * add wallet money for user.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_money(Request $request){

        $this->validate($request, [
                'amount' => 'required|integer',
                'card_id' => 'required|exists:cards,card_id,user_id,'.Auth::user()->id
            ]);

        try{
            
            $StripeWalletCharge = $request->amount * 100;

            Stripe::setApiKey(Setting::get('stripe_secret_key'));

            $Charge = Charge::create(array(
                  "amount" => $StripeWalletCharge,
                  "currency" => "usd",
                  "customer" => Auth::user()->stripe_cust_id,
                  "card" => $request->card_id,
                  "description" => "Adding Money for ".Auth::user()->email,
                  "receipt_email" => Auth::user()->email
                ));

            $update_user = User::find(Auth::user()->id);
            $update_user->wallet_balance += $request->amount;
            $update_user->save();

            Card::where('user_id',Auth::user()->id)->update(['is_default' => 0]);
            Card::where('card_id',$request->card_id)->update(['is_default' => 1]);

            //sending push on adding wallet money
            (new SendPushNotification)->WalletMoney(Auth::user()->id,currency($request->amount));

            if($request->ajax()){
                return response()->json(['message' => currency($request->amount).trans('api.added_to_your_wallet'), 'user' => $update_user]); 
            } else {
                return redirect('wallet')->with('flash_success',currency($request->amount).' added to your wallet');
            }

        } catch(StripeInvalidRequestError $e) {
            if($request->ajax()){
                 return response()->json(['error' => $e->getMessage()], 500);
            }else{
                return back()->with('flash_error',$e->getMessage());
            }
        } catch(Exception $e) {
            if($request->ajax()) {
                return response()->json(['error' => $e->getMessage()], 500);
            } else {
                return back()->with('flash_error', $e->getMessage());
            }
        }
    }

    /**
     * pay tips to provider by cash 
     * @return \Illuminate\Http\Response
     */
    public function cashtippayment(Request $request){
        $return=['message' => trans('api.tips_paid_cash_fails')] ; 

      //  print($request->request_id); echo("yyutfh=============");
      //  print($request->tips); exit;
        $UserRequest = UserRequests::find($request->request_id);
        if($UserRequest->payment_mode == 'CASH') {
            $RequestPayment = UserRequestPayment::where('request_id',$request->request_id)->first();
            if($request->has('tips')){
                $RequestPayment->tips = $request->tips;
                $RequestPayment->total += $request->tips;
                $RequestPayment->save();
                $return =['message' => trans('api.tips_paid_cash')]; 
              }
        }
        return response()->json($return); 
    }
}
