<?php

namespace App\Http\Middleware;

use App\Provider;
use App\ProviderWallet;
use App\ProviderWalletHistory;

use Illuminate\Support\Facades\Auth;

use Closure;

class ProviderWalletExist
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $providerId = Auth::user()->id;
        if(1==1){
            return response()->json(['error' => 'Your Wallet Has Expired, Kindly Recharge']);
        }
        return $next($request);
    }
}
