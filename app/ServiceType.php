<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'provider_name',
        'image',
        'insure_price',
        'price',
        'fixed',
        'min_price',
        'description',
        'status',
        'minute',
        'distance',
        'calculator',
        'capacity',
        'min_waiting_time',
        'min_waiting_charge'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'created_at', 'updated_at'
    ];
}
