<?php 
namespace App\Helper;
class StripeHelper
{
    private $STRIPE_SERVICE_URL;//accounts';
    private $data;
    private $key;
    private $headers;
    private $urlToCall;
    private $document_front;
    private $document_back;
    private $additional_document_front;
    private function setKey($key){
        $this->key=$key;
        return true;
    }
    private function getKey($ContentType){
        //sk_test_6DJLS4SC0j23CNLv04AheTTJ005VOFqYvM
        $this->headers= array('Authorization: Bearer '.$this->key);
        if($ContentType=="multipart"){
            $this->headers[]='Content-Type: multipart/form-data';
        }
        return true;
    }
    private function stripeCURL($isMultipart,$Method){
        //echo $this->headers;exit;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_URL, $this->STRIPE_SERVICE_URL.$this->urlToCall);
        if($Method==="POST")
        curl_setopt($ch, CURLOPT_POST, true);
        else
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        if(!empty($this->data)){
            if($isMultipart==="multipart")
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->data);
            else
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->data));
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        curl_close($ch);
        return json_decode($output, true);
    }
    public function checkstatus( $stripe_account_id,$stripeKey){
        $this->STRIPE_SERVICE_URL='https://api.stripe.com/v1/';
        $this->urlToCall='accounts/'.$stripe_account_id;
        $this->setKey($stripeKey);
        $this->getKey("");
        $this->data=array();
        return $this->stripeCURL("","GET");
    }
}