<?php 

namespace App\Helpers;

use File;
use Setting;
 

class Helper
{

    public static function upload_picture($picture)
    {
        $file_name = time();
        $file_name .= rand();
        $file_name = sha1($file_name);
        if ($picture) {
            $ext = $picture->getClientOriginalExtension();
            //echo base_path(); exit;
            //$picture->move(public_path() . "/uploads", $file_name . "." . $ext);
            $picture->move(base_path() . "/public/uploads", $file_name . "." . $ext);
            $local_url = $file_name . "." . $ext;

            $s3_url = url('/').'/public/uploads/'.$local_url;
            
            return $s3_url;
        }
        return "";
    }
    public static function upload_signature($picture)
    {
        $file_name = time();
        $file_name .= rand();
        $file_name = sha1($file_name);
        if ($picture) {
            $ext = $picture->getClientOriginalExtension();
            //echo base_path(); exit;
            //$picture->move(public_path() . "/uploads", $file_name . "." . $ext);
            $picture->move(base_path() . "/public/uploads/signature/", $file_name . "." . $ext);
            $local_url = $file_name . "." . $ext;

            $s3_url = url('/').'/public/uploads/signature/'.$local_url;
            
            return $s3_url;
        }
        return "";
    }


    public static function delete_picture($picture) {
        //File::delete( public_path() . "/uploads/" . basename($picture));
        File::delete( base_path() . "/uploads/" . basename($picture));
        return true;
    }

    public static function generate_booking_id() {
        return Setting::get('booking_prefix').mt_rand(100000, 999999);
    }

}
