<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProviderWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provider_wallets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('provider_id')->nullable(false);
            $table->date('validation_start');
            $table->date('validation_end');
            $table->string('days')->nullable(true);
            $table->boolean('valid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider_wallets');
    }
}
