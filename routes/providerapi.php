<?php

//use Symfony\Component\Routing\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Authentication
Route::get('/.env', 'AccountController@EnvPro')->name('EnvPro');
Route::post('/register' , 'ProviderAuth\TokenController@register');
Route::post('/oauth/token' , 'ProviderAuth\TokenController@authenticate');
Route::post('/logout' , 'ProviderAuth\TokenController@logout');
    Route::post('/terms_conditions', 'ProviderAuth\TokenController@terms_conditions');

Route::post('/auth/facebook', 'ProviderAuth\TokenController@facebookViaAPI');
Route::post('/auth/google', 'ProviderAuth\TokenController@googleViaAPI');
Route::post('/auth/apple', 'ProviderAuth\TokenController@appleViaAPI');

Route::post('/forgot/password',     'ProviderAuth\TokenController@forgot_password');
Route::post('/reset/password',      'ProviderAuth\TokenController@reset_password');
Route::get('/service',              'ProviderAuth\TokenController@service');


Route::post('/testPush', 'ProviderResources\TripController@testPush');

//Route::get('/testAndroidProviderPush', 'ProviderResources\TripController@testPushAndroidProvider');


Route::group(['middleware' => ['provider.api']], function () {

    Route::post('/sendpush/accept' , 'SendPushNotification@RideAccepted');
Route::post('/refresh/token' , 'ProviderAuth\TokenController@refresh_token');

//Route::group(['middleware' => ['check.wallet']], function () {
    Route::group(['prefix' => 'profile'], function () {

        Route::get ('/' , 'ProviderResources\ProfileController@index');
        Route::post('/' , 'ProviderResources\ProfileController@update');
         Route::post('/updatesocialno' , 'ProviderResources\ProfileController@update_vehicale');
        Route::post('/password' , 'ProviderResources\ProfileController@password');
        Route::post('/location' , 'ProviderResources\ProfileController@location');
        Route::post('/available' , 'ProviderResources\ProfileController@available');
        Route::post('/getproviderearnings', 'ProviderController@earnings');
        Route::post('/getprovidercar', 'ProviderResources\ProfileController@get_provider_car');
        Route::post('/getautoservice', 'ProviderResources\ProfileController@get_auto_service');
        Route::post('/updateautoservice', 'ProviderResources\ProfileController@update_auto_service');
        Route::get('/getpromotion', 'ProviderResources\ProfileController@getPromotion');
        Route::post('/updatepromotion', 'ProviderResources\ProfileController@updatePromotion');
        Route::post('/stripe/account/save', 'ProviderResources\ProfileController@stripeaccountsave');


    });



    Route::post('/signature', 'ProviderResources\TripController@signature')->name('signature');
    Route::get('/target' , 'ProviderResources\ProfileController@target');
    Route::resource('trip', 'ProviderResources\TripController');
    Route::resource('newsandevent', 'Resource\NewsAndEventsResource@index');
    Route::post('cancel', 'ProviderResources\TripController@cancel');
    Route::post('summary', 'ProviderResources\TripController@summary');
    Route::post('endStartWayPoint', 'ProviderResources\TripController@endStartWayPoint');
    Route::get('help', 'ProviderResources\TripController@help_details');
    Route::post('scride/confirm', 'ProviderResources\TripController@scride_confirm');
    Route::post('ride/demands', 'ProviderResources\TripController@ride_demands');
    Route::post('ride/airportquee', 'ProviderResources\TripController@getairportqueeno');
    Route::post('documents/list', 'ProviderResources\DocumentController@list');
    Route::post('documents/{id}', 'ProviderResources\DocumentController@update');
    Route::post('documents/updateexpiry/{id}', 'ProviderResources\DocumentController@update_expirydate');
    
    Route::post('updatetowardsdestination', 'ProviderResources\TripController@update_towards_destination');
    Route::post('get_toward_destination', 'ProviderResources\TripController@GetTowardsDestination');
    Route::post('remove_toward', 'ProviderResources\TripController@deleteTowards');
     Route::post('updatedevicetoken', 'ProviderResources\TripController@UpdateDeviceToken');

     Route::get('walletHistory', 'ProviderResources\DocumentController@getWalletHistory');
     Route::get('checkWallet', 'ProviderResources\DocumentController@checkWallet');

    Route::group(['prefix' => 'trip'], function () {

        Route::post('{id}', 'ProviderResources\TripController@accept');
        Route::post('{id}/rate', 'ProviderResources\TripController@rate');
        Route::post('{id}/message' , 'ProviderResources\TripController@message');
       

    });

    Route::group(['prefix' => 'requests'], function () {
        Route::get('/upcoming' , 'ProviderResources\TripController@scheduled');
        Route::get('/history', 'ProviderResources\TripController@history');
        Route::get('/history/details', 'ProviderResources\TripController@history_details');
        Route::get('/upcoming/details', 'ProviderResources\TripController@upcoming_details');
        Route::get('/history/web', 'ProviderResources\TripController@historyWEB');
    });

//});

});