<?php
//use Symfony\Component\Routing\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/.env', 'AccountController@EnvPro')->name('EnvPro');
Route::post('/mobile/number/verification','UserApiController@sendMobileVerificationOTP');
Route::post('/signup' , 'UserApiController@signup');
Route::post('/logout' , 'UserApiController@logout');
Route::get('/terms_conditions', 'UserApiController@terms_conditions');

Route::post('/auth/facebook', 'Auth\SocialLoginController@facebookViaAPI');
Route::post('/auth/google', 'Auth\SocialLoginController@googleViaAPI');
Route::post('/auth/apple', 'Auth\SocialLoginController@gappleViaAPI');

Route::get('auth/google/callback', 'Auth\SocialLoginController@handleGoogleCallback');

Route::post('/forgot/password',     'UserApiController@forgot_password');
Route::post('/reset/password',      'UserApiController@reset_password');

Route::post('/subscribe/newsletter','UserApiController@SubscribeNewsLetter');

Route::post('/contact/us','UserApiController@ContactUs');

Route::post('/testPush' , 'UserApiController@send_test_push');

Route::get('/testAndroidProviderPush', 'UserApiController@testPushAndroidProvider');


Route::group(['middleware' => ['auth:api']], function () {

	// user profile
	Route::get('/trips' , 'UserApiController@trips');
	Route::post('/change/password' , 'UserApiController@change_password');

	Route::post('/update/location' , 'UserApiController@update_location');

	Route::get('/details' , 'UserApiController@details');

	Route::post('/update/profile' , 'UserApiController@update_profile');

	Route::post('/add/favourite' , 'UserApiController@add_favourite');

	Route::post('/remove/favourite' , 'UserApiController@remove_favourite');
	// services

	Route::get('/services' , 'UserApiController@services');
	Route::post('/services' , 'UserApiController@services');

	// provider

	Route::post('/rate/provider' , 'UserApiController@rate_provider');

	// request

	Route::post('/send/request' , 'UserApiController@send_request');

	// feedback by customer/passanger 
	Route::post('/send/feedback' , 'UserApiController@send_feedback');

	Route::post('/cancel/request' , 'UserApiController@cancel_request');
	
	Route::get('/request/check' , 'UserApiController@request_status_check');
	Route::get('/show/providers' , 'UserApiController@show_providers');

	// history

	
	Route::get('upcoming/trips' , 'UserApiController@upcoming_trips');
	
	Route::get('/trip/details' , 'UserApiController@trip_details');
	Route::get('upcoming/trip/details' , 'UserApiController@upcoming_trip_details');

	// payment

	Route::post('/payment' , 'PaymentController@payment');
	// cash tip payment by the customer to provider
	Route::post('/tips/payment/by/cash' , 'PaymentController@cashtippayment');

	Route::post('/add/money' , 'PaymentController@add_money');

	// estimated

	Route::get('/estimated/fare' , 'UserApiController@estimated_fare');
	Route::post('/estimated/fare' , 'UserApiController@estimated_fare');

	// help

	Route::get('/help' , 'UserApiController@help_details');

	// promocode

	Route::get('/promocodes' , 'UserApiController@promocodes');

	Route::post('/promocode/add' , 'UserApiController@add_promocode');
	// news & event
	
	Route::post('/newsandevent' , 'UserApiController@news_events');

	// card payment

    Route::resource('card', 'Resource\CardResource');
    //  UPDATE DEVICE TOKEN
    Route::post('/updatetoken','UserApiController@UpdateDeviceToken');

});
