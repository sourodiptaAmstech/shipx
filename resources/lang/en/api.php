<?php 

return array (

	'user' => [
		'incorrect_password' => 'Incorrect Password',
		'password_updated' => 'Password Updated',
		'location_updated' => 'Location Updated',
		'profile_updated' => 'Profile Updated',
		'user_not_found' => 'User Not Found',
		'not_paid' => 'User Not Paid',

	],
	'ride' => [
		'request_inprogress' => 'Already Request in Progress',
		'no_providers_found' => 'No Drivers Found',
		'request_cancelled' => 'Your Shipment Cancelled',
		'already_cancelled' => 'Already Shipment Cancelled',
		'already_onride' => 'Already You are Onshipment',
		'provider_rated' => 'Driver Rated',
		'request_scheduled' => 'Shipment Scheduled',
		'request_already_scheduled' => 'Shipment Already Scheduled',
	],
	'something_went_wrong' => 'Something Went Wrong',

	'logout_success' => 'Logged out Successfully',
	'service_success' => 'Get Service Successfully',
	'services_not_found' => 'Services Not Found',
	'promocode_applied' => 'Promocode Applied',
	'promocode_expired' => 'Promocode Expired',
	'promocode_already_in_user' => 'Promocode Already in Use',
	'paid' => 'Paid',
	'added_to_your_wallet' => 'Added to your Wallet',
	'tips_paid_cash'=>'Thanks you for your tip',
	'tips_paid_cash_fails'=>'Something Went Wrong',
	'push' => [
		'request_accepted' => 'Your Shipment Accepted by a Driver',
		'sendscpush_u' => 'Your scheduled shipment is about to start',
		'sendscpush_d' => 'Accepted scheduled shipment is about to start',
		'arrived' => 'Driver Arrived at your Location',
		'dropped_ride_on' => 'Your Shipment Is Dropped At Your Location',
		'incoming_request' => 'New Incoming shipment',
		'added_money_to_wallet' => ' Added to your Wallet',
		'charged_from_wallet' => ' Charged from your Wallet',
		'document_verfied' => 'Your Documents are verified, Now you are ready to Start your Business',
		'provider_not_available' => 'Sorry for inconvience time, Our partner or busy. Please try after some time',
		'user_cancelled' => 'User Cancelled the shipment',
		'provider_cancelled' => 'Driver Cancelled the shipment',
		'schedule_start' => 'Your schedule shipment has been started',
	],
);