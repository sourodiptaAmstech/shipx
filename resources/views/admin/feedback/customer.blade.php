@extends('admin.layout.base')

@section('title', $page)

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            <div class="box box-block bg-white">
            	<h3>{{$page}}</h3>

            	<div style="text-align: center;padding: 20px;color: blue;font-size: 24px;">
            
            	</div>

            	<div class="row">

	            {{-- 	<div class="col-lg-4 col-md-6 col-xs-12">
						<div class="box box-block bg-white tile tile-1 mb-2">
							<div class="t-icon right"><span class="bg-danger"></span><i class="ti-rocket"></i></div>
							<div class="t-content">
								<h6 class="text-uppercase mb-1">Total No. of Shipments</h6>
								<h1 class="mb-1">{{$rides->count()}}</h1>
								<span class="text-muted font-90">% down from cancelled Request</span>
							</div>
						</div>
					</div>
 --}}

				{{-- 	<div class="col-lg-4 col-md-6 col-xs-12">
						<div class="box box-block bg-white tile tile-1 mb-2">
							<div class="t-icon right"><span class="bg-success"></span><i class="ti-bar-chart"></i></div>
							<div class="t-content">
								<h6 class="text-uppercase mb-1">Revenue</h6>
								<h1 class="mb-1">{{currency($revenue[0]->overall)}}</h1>
								<i class="fa fa-caret-up text-success mr-0-5"></i><span>from {{$rides->count()}} Shipments</span>
							</div>
						</div>
					</div> --}}
{{-- 
					<div class="col-lg-4 col-md-6 col-xs-12">
						<div class="box box-block bg-white tile tile-1 mb-2">
							<div class="t-icon right"><span class="bg-warning"></span><i class="ti-archive"></i></div>
							<div class="t-content">
								<h6 class="text-uppercase mb-1">Cancelled Shipments</h6>
								<h1 class="mb-1">{{$cancel_rides}}</h1>
								<i class="fa fa-caret-down text-danger mr-0-5"></i><span>for @if($cancel_rides == 0) 0.00 @else {{round($cancel_rides/$rides->count(),2)}}% @endif Shipments</span>
							</div>
						</div>
					</div> --}}

						<div class="row row-md mb-2" style="padding: 15px;">
							<div class="col-md-12">
									<div class="box bg-white">
										<div class="box-block clearfix">
											<h5 class="float-xs-left">Feedback</h5>
										
										</div>

										@if(count($customerFeedback) != 0)
								            <table class="table table-striped table-bordered dataTable" id="table-2">
								                <thead>
								                   <tr>
														
														<td>Name</td>
														<td>Subject</td>
                                                        <td>Description</td>
                                                       
														
													</tr>
								                </thead>
								                <tbody>
								             
														@foreach($customerFeedback as $index => $cf)
															<tr>
															{{-- 	<td>{{$cf->id}}</td> --}}
																<td>
                                                                    
                                                                    {{$cf->first_name}} {{$cf->last_name}}
                                                                </td>
																<td class="truncate">
																	{{$cf->subject}}
																</td>
																<td class="truncate">                                                                    
                                                                    {{$cf->description}}								
                                                                </td>
                                                              
                                                            </tr>
														@endforeach
															
								                <tfoot>
								                    <tr>
													
														<td>Name</td>
														<td>Subject</td>
                                                        <td>Description</td>
                                                       
													</tr>
								                </tfoot>
								            </table>
								            @else
								            <h6 class="no-result">No results found</h6>
								            @endif 

									</div>
								</div>

							</div>

            	</div>

            </div>
        </div>
    </div>

    
<!-- Trigger/Open The Modal -->
<button id="myBtn" style="display:none;">Open Modal</button>

<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content" style="height: 500px;">
        <div style="width:100%;float: left;">
            <div class="" style="width: 60%;float: left;text-align: right;font-size: 21px;font-weight: bold;">Customer Feedback</div>
            <div class="" style="width: 40%;float: left;">
                <span class="close">×</span>
            </div>
        </div>
        <div style="width:100%;float: left;height: auto;padding: 50px;">
            <div style="width:100%;float: left;">
        <div style="float: left;font-weight:bold;width:130px;">Customer Name:</div> <div id="name"></div> </div>
        <div style="width:100%;float: left;padding-top: 20px;">
        <div style="float: left;font-weight:bold;width:130px;">Subject:</div> <div id="subj"></div> </div>
        <div style="width:100%;float: left;padding-top: 20px;">
        <div style="float: left; font-weight:bold;">Description:</div> </div>
        <div style="width:100%;float: left;padding-top: 20px;">
        <div id="desc"  style="float: left;overflow-y: auto;height: 100px;"></div>
    </div>
    </div>
    </div>
  
  </div>

    @endsection

@section('extra-script')
<script>
    $(function() {
        if ($.fn.DataTable.isDataTable("#table-2")) {
  $('#table-2').DataTable().destroy();
}
        var table = jQuery('#table-2').DataTable({
            responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
            columnDefs: [ {
                targets: 1,
                render: function ( data, type, row ) {
                    return data.substr( 0, 50 );
                }
            },{
                targets: 2,
                render: function ( data, type, row ) {
                    return data.substr( 0, 50 );
                }
            } ]
        });
        jQuery('#table-2 tbody').on( 'click', 'tr', function () {
            var d = table.row( this ).data();
            console.log(d);
            $("#name").html(d[0]);
            $("#subj").html(d[1]);
            $("#desc").html(d[2]);

            jQuery("#myBtn").click();
            });

    });

    // Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
    </script>
    <style>
        body {font-family: Arial, Helvetica, sans-serif;}
        
        /* The Modal (background) */
        .modal {
          display: none; /* Hidden by default */
          position: fixed; /* Stay in place */
          z-index: 1; /* Sit on top */
          padding-top: 100px; /* Location of the box */
          left: 0;
          top: 0;
          width: 100%; /* Full width */
          height: 100%; /* Full height */
          overflow: auto; /* Enable scroll if needed */
          background-color: rgb(0,0,0); /* Fallback color */
          background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        
        /* Modal Content */
        .modal-content {
          background-color: #fefefe;
          margin: auto;
          padding: 20px;
          border: 1px solid #888;
          width: 50%;
        }
        
        /* The Close Button */
        .close {
          color: #aaaaaa;
          float: right;
          font-size: 28px;
          font-weight: bold;
        }
        
        .close:hover,
        .close:focus {
          color: #000;
          text-decoration: none;
          cursor: pointer;
        }
        
        </style>
@endsection

