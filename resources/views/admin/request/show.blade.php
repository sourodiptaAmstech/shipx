@extends('admin.layout.base')

@section('title', 'Request details ')

@section('content')
<style>

table{
   overflow-y:scroll;
   height:100px;
   display:block;
   height: auto;
}
</style>
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h4>Request details</h4>
            <a href="{{ route('admin.requests.index') }}" class="btn btn-default pull-right">
                <i class="fa fa-angle-left"></i> Back
            </a>
            <div class="row">
                <div class="col-md-6">
                    <dl class="row">
                        <dt class="col-sm-4">User Name :</dt>
                        <dd class="col-sm-8">{{ $request->user->first_name }}</dd>

                        <dt class="col-sm-4">Provider Name :</dt>
                        @if($request->provider)
                        <dd class="col-sm-8">{{ $request->provider->first_name }}</dd>
                        @else
                        <dd class="col-sm-8">Provider not yet assigned!</dd>
                        @endif

                        <dt class="col-sm-4">Total Distance :</dt>
                        <dd class="col-sm-8">{{ $request->distance ? $request->distance : '-' }}</dd>

                        @if($request->status == 'SCHEDULED')
                        <dt class="col-sm-4">Shipment Scheduled Time :</dt>
                        <dd class="col-sm-8">
                            @if($request->schedule_at != "0000-00-00 00:00:00")
                                {{ date('jS \of F Y h:i:s A', strtotime($request->schedule_at)) }} 
                            @else
                                - 
                            @endif
                        </dd>
                        <dt class="col-sm-4">Flight Information :</dt>
                        <dd class="col-sm-8">
                            {{ $request->flight_info ? $request->flight_info : '--' }}
                        </dd>

                        <dt class="col-sm-4">Gratuity :</dt>
                        <dd class="col-sm-8">
                            {{ $request->gratuity ? $request->gratuity : '--' }}
                        </dd>

                        <dt class="col-sm-4">Comment :</dt>
                        <dd class="col-sm-8">
                            {{ $request->comment_spinfo ? $request->comment_spinfo : '--' }}
                        </dd>
                        @else
                        <dt class="col-sm-4">Shipment Start Time :</dt>
                        <dd class="col-sm-8">
                            @if($request->started_at != "0000-00-00 00:00:00")
                                {{ date('jS \of F Y h:i:s A', strtotime($request->started_at)) }} 
                            @else
                                - 
                            @endif
                         </dd>

                        <dt class="col-sm-4">Shipment End Time :</dt>
                        <dd class="col-sm-8">
                            @if($request->finished_at != "0000-00-00 00:00:00") 
                                {{ date('jS \of F Y h:i:s A', strtotime($request->finished_at)) }}
                            @else
                                - 
                            @endif
                        </dd>
                        @endif

                        <dt class="col-sm-4">Pickup Address :</dt>
                        <dd class="col-sm-8">{{ $request->s_address ? $request->s_address : '-' }}</dd>

                       

                        @if(!empty($MultipleWayPoints))

                        @for ($i = 0; $i < count($MultipleWayPoints); $i++)
                        <dt class="col-sm-4">Parcel Drop Address:</dt>
                        <dd class="col-sm-8">{{ $MultipleWayPoints[$i]['w_address'] }}</dd>
                        <dt class="col-sm-4">Parcel Details:</dt>

                        <table class="table">
                            <thead>
                              <tr>
                                <th>Recipient Name</th>
                                <th>Recipient Phone No</th>
                                <th>No of Packages</th>
                                <th>Description</th>
                                <th>Pick Up Date</th>
                                <th>Delivered Date</th>
                                <th>Recieved By</th>
                                <th>Receiver Relationship</th>
                                <th>Receiver Phone No</th>
                                <th>Receiver Signature</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>{{ $MultipleWayPoints[$i]['parcelDetails_recipient_name'] }}</td>
                                <td>{{ $MultipleWayPoints[$i]['parcelDetails_recipient_phone_no'] }}</td>
                                <td>{{ $MultipleWayPoints[$i]['parcelDetails_no_packages'] }}</td>
                                <td>{{ $MultipleWayPoints[$i]['parcelDetails_item_description'] }}</td>
                                <td>{{ $MultipleWayPoints[$i]['parcelDetails_picked_on'] }}</td>
                                <td>{{ $MultipleWayPoints[$i]['parcelDetails_delivered_on'] }}</td>
                                <td>{{ $MultipleWayPoints[$i]['parcelDetails_recieved_by'] }}</td>
                                <td>{{ $MultipleWayPoints[$i]['parcelDetails_receiver_relationship'] }}</td>
                                <td>{{ $MultipleWayPoints[$i]['parcelDetails_receiver_ph'] }}</td>  
                                @if($MultipleWayPoints[$i]['upload_signature_path'] =='')
                                <td></td> 
                                @else
                                <td><a   href="/public/storage/{{ $MultipleWayPoints[$i]['upload_signature_path']  }}" target="_blank">View Signature</a></td>       
                                @endif                              
                                
                                
                              </tr>
                             
                            </tbody>
                          </table>





                        @endfor
                        
                        
                        @endif








                        <dt class="col-sm-4">Parcel Drop Address :</dt>
                        <dd class="col-sm-8">{{ $request->d_address ? $request->d_address : '-' }}</dd>
                        @if(!empty($DESTINATIONPoints))

                        @for ($i = 0; $i < count($DESTINATIONPoints); $i++)
                        
                        <dt class="col-sm-4">Parcel Details:</dt>

                        <table class="table">
                            <thead>
                              <tr>
                                <th>Recipient Name</th>
                                <th>Recipient Phone No</th>
                                <th>No of Packages</th>
                                <th>Description</th>
                                <th>Pick Up Date</th>
                                <th>Delivered Date</th>
                                <th>Recieved By</th>
                                <th>Receiver Relationship</th>
                                <th>Receiver Phone No</th>
                                <th>Receiver Signature</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>{{ $DESTINATIONPoints[$i]['parcelDetails_recipient_name'] }}</td>
                                <td>{{ $DESTINATIONPoints[$i]['parcelDetails_recipient_phone_no'] }}</td>
                                <td>{{ $DESTINATIONPoints[$i]['parcelDetails_no_packages'] }}</td>
                                <td>{{ $DESTINATIONPoints[$i]['parcelDetails_item_description'] }}</td>
                                <td>{{ $DESTINATIONPoints[$i]['parcelDetails_picked_on'] }}</td>
                                <td>{{ $DESTINATIONPoints[$i]['parcelDetails_delivered_on'] }}</td>
                                <td>{{ $DESTINATIONPoints[$i]['parcelDetails_recieved_by'] }}</td>
                                <td>{{ $DESTINATIONPoints[$i]['parcelDetails_receiver_relationship'] }}</td>
                                <td>{{ $DESTINATIONPoints[$i]['parcelDetails_receiver_ph'] }}</td>
                                @if($DESTINATIONPoints[$i]['upload_signature_path'] =='')
                                <td></td> 
                                @else
                                <td><a    href="/public/storage/{{ $DESTINATIONPoints[$i]['upload_signature_path'] }}" target="_blank">View Signature</a></td>
                                @endif   
                                
                                
                              </tr>
                             
                            </tbody>
                          </table>





                        @endfor
                        
                        
                        @endif
                        

                        @if($request->payment)
                        <dt class="col-sm-4">Base Price :</dt>
                        <dd class="col-sm-8">{{ currency($request->payment->fixed) }}</dd>
                        <!--
                        <dt class="col-sm-4">Waiting Time :</dt>
                        <dd class="col-sm-8">{{ ($request->payment->waiting_time) }} minutes</dd>

                        <dt class="col-sm-4">Waiting Cost :</dt>
                        <dd class="col-sm-8">{{ currency($request->payment->waiting_cost) }}</dd>
                        -->
                        <dt class="col-sm-4">Tax Price :</dt>
                        <dd class="col-sm-8">{{ currency($request->payment->tax) }}</dd>

                        <dt class="col-sm-4">Total Amount :</dt>
                        <dd class="col-sm-8">{{ currency($request->payment->paymentGatewayFee) }}</dd>
                        @endif

                        <dt class="col-sm-4">Shipment Status : </dt>
                        <dd class="col-sm-8">
                            {{ $request->status }}
                        </dd>

                        <dt class="col-sm-4">Is Insured : </dt>
                        <dd class="col-sm-8">
                            {{ $request->isInsured }}
                        </dd>

                    </dl>
                </div>
                <div class="col-md-6">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
<style type="text/css">
    #map {
        height: 450px;
    }
</style>
@endsection

@section('scripts')
<script type="text/javascript">
    var map;
    var zoomLevel = 11;

    function initMap() {

        map = new google.maps.Map(document.getElementById('map'));
        var base_url = window.location.origin;
        var marker = new google.maps.Marker({
            map: map,
            icon: base_url+'/laraval/shipx/asset/img/marker-start.png',
            anchorPoint: new google.maps.Point(0, -29)
        });
        


         var markerSecond = new google.maps.Marker({
            map: map,
            icon: base_url+'/laraval/shipx/asset/img/marker-end.png',
            anchorPoint: new google.maps.Point(0, -29)
        });

        var bounds = new google.maps.LatLngBounds();
        var waypoints=[];





        <?php
        echo "var javascript_array = '".json_encode($MultipleWayPoints)."';"; 
       // echo "var javascript_array = ". $js_array . ";\n";
        ?>
        console.log("------------------------------------");
       // console.log(JSON.parse(javascript_array));
        javascript_array=JSON.parse(javascript_array);
        for(var i=0; i<Object.keys(javascript_array).length;i++){
           // console.log(javascript_array[i].latitude);
            waypoints.push({
                location:new google.maps.LatLng(javascript_array[i].w_latitude, javascript_array[i].w_longitude),
                stopover:true
            });
        /*    new google.maps.Marker({
            map: map,
            icon: base_url+'/laraval/shipx/asset/img/marker-end.png',
           anchorPoint: new google.maps.Point(0, -29),
            position:new google.maps.LatLng(javascript_array[i].w_latitude, javascript_array[i].w_longitude)
            });*/
            }
        
     
        source = new google.maps.LatLng({{ $request->s_latitude }}, {{ $request->s_longitude }});




        destination = new google.maps.LatLng({{ $request->d_latitude }}, {{ $request->d_longitude }});

        marker.setPosition(source);
        markerSecond.setPosition(destination);

        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true, preserveViewport: true});
        directionsDisplay.setMap(map);

        directionsService.route({
            origin: source,
            waypoints:waypoints,
            destination: destination,
            travelMode: google.maps.TravelMode.DRIVING
        }, function(result, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                console.log(result);
                directionsDisplay.setDirections(result);

                marker.setPosition(result.routes[0].legs[0].start_location);
                markerSecond.setPosition(result.routes[0].legs[0].end_location);
            }
        });

        @if($request->provider && $request->status != 'COMPLETED')
        var markerProvider = new google.maps.Marker({
            map: map,
            icon: base_url+"/laraval/shipx/asset/img/marker-car.png",
            anchorPoint: new google.maps.Point(0, -29)
        });

        provider = new google.maps.LatLng({{ $request->provider->latitude }}, {{ $request->provider->longitude }});
        markerProvider.setVisible(true);
        markerProvider.setPosition(provider);
        console.log('Provider Bounds', markerProvider.getPosition());
        bounds.extend(markerProvider.getPosition());
        @endif

        bounds.extend(marker.getPosition());
        bounds.extend(markerSecond.getPosition());
        map.fitBounds(bounds);
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_KEY') }}&libraries=places&callback=initMap" async defer></script>
@endsection