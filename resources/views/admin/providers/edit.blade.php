@extends('admin.layout.base')

@section('title', 'Update Provider ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.provider.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Update Provider</h5>

            <form class="form-horizontal" action="{{route('admin.provider.update', $provider->id )}}" method="POST" enctype="multipart/form-data" role="form">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PATCH">
                <div class="form-group row">
                    <label for="first_name" class="col-xs-2 col-form-label">First Name</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $provider->first_name }}" name="first_name" required id="first_name" placeholder="First Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="last_name" class="col-xs-2 col-form-label">Last Name</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $provider->last_name }}" name="last_name" required id="last_name" placeholder="Last Name">
                    </div>
                </div>


                <div class="form-group row">
                    
                    <label for="picture" class="col-xs-2 col-form-label">Picture</label>
                    <div class="col-xs-10">
                    @if(isset($provider->avatar))
                        <img style="height: 90px; margin-bottom: 15px; border-radius:2em;" src="../../../storage/{{$provider->avatar}}">
                    @endif
                        <input type="file" accept="image/*" name="avatar" class="dropify form-control-file" id="picture" aria-describedby="fileHelp">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="mobile" class="col-xs-2 col-form-label">Mobile</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="number" value="{{ $provider->mobile }}" name="mobile" required id="mobile" placeholder="Mobile">
                    </div>
                </div>

                <div class="documents" style="display:none;">
				<div style="float:right;">
				<button type="button" class="btn btn-sm btn-info" onclick="hide_docs();">Less</button>
				</div>
				<h5>Driver Document's</h5>
					
					
				<div class="form-group row">
					<label for="picture" class="col-xs-4 col-form-label">Driver's License</label>
					<div class="row">
						
						<input type="file" name="dl_front_url" id="dl_front_url" style="display:none;"/>
						<label for="dl_front_url">
							@if(isset($Document) && isset($Document['0']->front_url) && $Document['0']->front_url != '')
							<img src="{{ asset('storage/'.$Document['0']->front_url) }}" alt="" height="100" width="100">
							@else
							<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
							@endif
						</label>
						
						<input type="file" name="dl_back_url" id="dl_back_url" style="display:none;"/>
						<label for="dl_back_url">
							@if(isset($Document) && isset($Document['0']->back_url) && $Document['0']->back_url != '')
							<img src="{{ asset('storage/'.$Document['0']->back_url) }}" alt="" height="100" width="100">
							@else
							<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
							@endif
							</label>
					
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-4 col-form-label">Road Worthy Certificate</label>
					<div class="row">
						
						<input type="file" name="road_front_url" id="road_front_url" style="display:none;"/>
						<label for="road_front_url">
							@if(isset($Document) && isset($Document['1']->front_url) && $Document['1']->front_url != '')
							<img src="{{ asset('storage/'.$Document['1']->front_url) }}" alt="" height="100" width="100">
							@else
							<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
							@endif
						</label>
						
						<input type="file" name="road_back_url" id="road_back_url" style="display:none;"/>
						<label for="road_back_url">
							@if(isset($Document) && isset($Document['1']->back_url) && $Document['1']->back_url != '')
							<img src="{{ asset('storage/'.$Document['1']->back_url) }}" alt="" height="100" width="100">
							@else
							<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
							@endif
							</label>
					
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-4 col-form-label">Comprehensive Insurance</label>
					<div class="row">
						
						<input type="file" name="ci_front_url" id="ci_front_url" style="display:none;"/>
						<label for="ci_front_url">
							@if(isset($Document) && isset($Document['2']->front_url) && $Document['2']->front_url != '')
							<img src="{{ asset('storage/'.$Document['2']->front_url) }}" alt="" height="100" width="100">
							@else
							<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
							@endif
						</label>
						
						<input type="file" name="ci_back_url" id="ci_back_url" style="display:none;"/>
						<label for="ci_back_url">
							@if(isset($Document) && isset($Document['2']->back_url) && $Document['2']->back_url != '')
							<img src="{{ asset('storage/'.$Document['2']->back_url) }}" alt="" height="100" width="100">
							@else
							<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
							@endif
							</label>
					
					</div>
				</div>


				<div class="form-group row">
					<label for="picture" class="col-xs-4 col-form-label">Third Party Insurance</label>
					<div class="row">
						
						<input type="file" name="tpi_front_url" id="tpi_front_url" style="display:none;"/>
						<label for="tpi_front_url">
							@if(isset($Document) && isset($Document['3']->front_url) && $Document['3']->front_url != '')
							<img src="{{ asset('storage/'.$Document['3']->front_url) }}" alt="" height="100" width="100">
							@else
							<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
							@endif
						</label>
						
						<input type="file" name="tpi_back_url" id="tpi_back_url" style="display:none;"/>
						<label for="tpi_back_url">
							@if(isset($Document) && isset($Document['3']->back_url) && $Document['3']->back_url != '')
							<img src="{{ asset('storage/'.$Document['3']->back_url) }}" alt="" height="100" width="100">
							@else
							<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
							@endif
							</label>
					
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-4 col-form-label">Shipx DECAL</label>
					<div class="row">
						
						<input type="file" name="decal_front_url" id="decal_front_url" style="display:none;"/>
						<label for="decal_front_url">
							@if(isset($Document) && isset($Document['4']->front_url) && $Document['4']->front_url != '')
							<img src="{{ asset('storage/'.$Document['4']->front_url) }}" alt="" height="100" width="100">
							@else
							<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
							@endif
						</label>
						
						<input type="file" name="decal_back_url" id="decal_back_url" style="display:none;"/>
						<label for="decal_back_url">
							@if(isset($Document) && isset($Document['4']->back_url) && $Document['4']->back_url != '')
							<img src="{{ asset('storage/'.$Document['4']->back_url) }}" alt="" height="100" width="100">
							@else
							<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
							@endif
							</label>
					
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-4 col-form-label">Proof Of Insurance</label>
					<div class="row">
						
						<input type="file" name="poi_front_url" id="poi_front_url" style="display:none;"/>
						<label for="poi_front_url">
							@if(isset($Document) && isset($Document['5']->front_url) && $Document['5']->front_url != '')
							<img src="{{ asset('storage/'.$Document['5']->front_url) }}" alt="" height="100" width="100">
							@else
							<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
							@endif
						</label>
						
						<input type="file" name="poi_back_url" id="poi_back_url" style="display:none;"/>
						<label for="poi_back_url">
							@if(isset($Document) && isset($Document['5']->back_url) && $Document['5']->back_url != '')
							<img src="{{ asset('storage/'.$Document['5']->back_url) }}" alt="" height="100" width="100">
							@else
							<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
							@endif
							</label>
					
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-4 col-form-label">Vehicle Image</label>
					<div class="row">
						
						<input type="file" name="vi_front_url" id="vi_front_url" style="display:none;"/>
						<label for="vi_front_url">
							@if(isset($Document) && isset($Document['6']->front_url) && $Document['6']->front_url != '')
							<img src="{{ asset('storage/'.$Document['6']->front_url) }}" alt="" height="100" width="100">
							@else
							<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
							@endif
						</label>
					</div>
				</div>



					
					
					

				</div>
				<div style="float:right;display:none;">
				<button type="button" class="btn btn-sm btn-info" onclick="show_docs();">Add Documents</button>
				</div>
                <div class="form-group row">
                    <label for="zipcode" class="col-xs-2 col-form-label"></label>
                    <div class="col-xs-10">
                        <button type="submit" class="btn btn-primary">Update Provider</button>
                        <a href="{{route('admin.provider.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
        function show_docs(){
			$('.documents').show();
		}
		function hide_docs(){
			$('.documents').hide();
		}
    </script>
@endsection
