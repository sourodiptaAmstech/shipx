@extends('admin.layout.base')

@section('title', 'Provider Documents ')

@section('content')
<style>
/* img {
	width:auto;        
	max-width:60%;
	height:auto;
} */
.doc_img img {
	width: 80% !important;

	margin: 0 15px 20px 30px;
}
</style>
<div class="content-area py-1">
    <div class="container-fluid">
        
        <div class="box box-block bg-white">
            <h5 class="mb-1">Provider Name: {{ $Document->provider->first_name }} {{ $Document->provider->last_name }}</h5>
            <h5 class="mb-1">Document: {{ $Document->document->name }}</h5>
            <!-- <embed src="{{ asset('storage/'.$Document->url) }}" width="100%" height="100%" /> -->

            <div class="row">
                <div class="col-xs-6">
                    <form action="{{ route('admin.provider.document.update', [$Document->provider->id, $Document->id]) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <button class="btn btn-block btn-primary" type="submit">Approve</button>
                    </form>
                </div>

                <div class="col-xs-6">
                    <form action="{{ route('admin.provider.document.destroy', [$Document->provider->id, $Document->id]) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-block btn-danger" type="submit">Delete</button>
                    </form>
                </div>
				 <div class="row" style="margin-top:15px;">				
				 </BR>
				 </BR>
				 </BR>
                  <div class="col-xs-12 doc_img" style="text-align: center;"/>
                  
                  <img src="{{ asset('storage/'.$Document->front_url)}}"/>
                  <img src="{{asset('storage/'.$Document->back_url)}}"/>
            </div>
            
         </div>
            </div>
        </div>
        
    </div>
</div>
@endsection