@extends('admin.layout.base')

@section('title', 'Add Provider ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
            <a href="{{ route('admin.provider.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Add Provider</h5>

            <form class="form-horizontal" action="{{route('admin.provider.store')}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
				<div class="form-group row">
					<label for="first_name" class="col-xs-12 col-form-label">First Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ old('first_name') }}" name="first_name" required id="first_name" placeholder="First Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="last_name" class="col-xs-12 col-form-label">Last Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ old('last_name') }}" name="last_name" required id="last_name" placeholder="Last Name">
					</div>
				</div>



				<div class="form-group row">
					<label for="email" class="col-xs-12 col-form-label">Email</label>
					<div class="col-xs-10">
						<input class="form-control" type="email" required name="email" value="{{old('email')}}" id="email" placeholder="Email">
					</div>
				</div>

				<div class="form-group row">
					<label for="password" class="col-xs-12 col-form-label">Password</label>
					<div class="col-xs-10">
						<input class="form-control" type="password" name="password" id="password" placeholder="Password">
					</div>
				</div>

				<div class="form-group row">
					<label for="password_confirmation" class="col-xs-12 col-form-label">Password Confirmation</label>
					<div class="col-xs-10">
						<input class="form-control" type="password" name="password_confirmation" id="password_confirmation" placeholder="Re-type Password">
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-12 col-form-label">Picture</label>
					<div class="col-xs-10">
						<input type="file" accept="image/*" name="avatar" class="dropify form-control-file" id="picture" aria-describedby="fileHelp">
					</div>
				</div>

				<div class="form-group row">
					<label for="mobile" class="col-xs-12 col-form-label">Mobile</label>
					<div class="col-xs-10">
						<input class="form-control" type="number" value="{{ old('mobile') }}" name="mobile" required id="mobile" placeholder="Mobile">
					</div>
				</div>
				<div class="documents" style="display:none;">
				<div style="float:right;">
				<button type="button" class="btn btn-sm btn-info" onclick="hide_docs();">Less</button>
				</div>
				<h5>Driver Document's</h5>
					
				<div class="form-group row">
					<label for="picture" class="col-xs-4 col-form-label">Driver's License</label>
					<div class="row">
						<input type="file" name="dl_front_url" id="dl_front_url" style="display:none;"/>
						<label for="dl_front_url">
						<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
						</label>
						<input type="file" name="dl_back_url" id="dl_back_url" style="display:none;"/>
						<label for="dl_back_url">
						<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
						</label>
					
					</div>
				</div>
					
				<div class="form-group row">
					<label for="picture" class="col-xs-4 col-form-label">Road Worthy Certificate</label>
					<div class="col-md-4">
						<input type="file" name="road_front_url" id="road_front_url" style="display:none;"/>
						<label for="road_front_url">
						<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
						</label>
						<input type="file" name="road_back_url" id="road_back_url" style="display:none;"/>
						<label for="road_back_url">
						<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
						</label>
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-4 col-form-label">Shipx DECAL</label>
					<div class="col-md-4">
						<input type="file" name="decal_front_url" id="decal_front_url" style="display:none;"/>
						<label for="decal_front_url">
						<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
						</label>
						<input type="file" name="decal_back_url" id="decal_back_url" style="display:none;"/>
						<label for="decal_back_url">
						<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
						</label>
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-4 col-form-label">Proof Of Insurance</label>
					<div class="col-md-4">
						<input type="file" name="poi_front_url" id="poi_front_url" style="display:none;"/>
						
						<label for="poi_front_url">
						<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
						</label>
						<input type="file" name="poi_back_url" id="poi_back_url" style="display:none;"/>
						<label for="poi_back_url">
						<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
						</label>
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-4 col-form-label">Comprehensive Insurance</label>
					<div class="col-md-4">
						<input type="file" name="ci_front_url" id="ci_front_url" style="display:none;"/>
						<label for="ci_front_url">
						<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
						</label>
						<input type="file" name="ci_back_url" id="ci_back_url" style="display:none;"/>
						<label for="ci_back_url">
						<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
						</label>
					</div>
				</div>


				<div class="form-group row">
					<label for="picture" class="col-xs-4 col-form-label">Third Party Insurance</label>
					<div class="col-md-4">
						<input type="file" name="tpi_front_url" id="tpi_front_url" style="display:none;"/>
						<label for="tpi_front_url">
						<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
						</label>
						<input type="file" name="tpi_back_url" id="tpi_back_url" style="display:none;"/>
						<label for="tpi_back_url">
						<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
						</label>
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-4 col-form-label">Vehicle Image</label>
					<div class="col-md-4">
						<input type="file" name="vi_front_url" id="vi_front_url" style="display:none;"/>
						<label for="vi_front_url">
						<img src="{{ url('asset/img/file_upload.png') }}" alt="" height="100" width="100">
						</label>
					</div>
				</div>

				</div>
				<div style="float:right; display:none;">
				<button type="button" class="btn btn-sm btn-info" onclick="show_docs();">Add Documents</button>
				</div>
					<div class="form-group row">
					<label for="zipcode" class="col-xs-12 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Add Provider</button>
						<a href="{{route('admin.provider.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
<script type="text/javascript">
        function show_docs(){
			$('.documents').show();
		}
		function hide_docs(){
			$('.documents').hide();
		}
    </script>
@endsection


