@extends('admin.layout.base')

@section('title', 'Add News or Event ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.newsandevent.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Add News or Event</h5>

            <form class="form-horizontal" action="{{route('admin.newsandevent.store')}}" method="POST" enctype="multipart/form-data" role="form">
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="title" class="col-xs-12 col-form-label">Title</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('title') }}" name="title" required id="title" placeholder="Title">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="description" class="col-xs-12 col-form-label">Description</label>
                    <div class="col-xs-10">
                        <textarea rows="6" class="form-control" name="description" required id="description" placeholder="Description">{{ old('description') }}</textarea>
                    </div>
                </div>
				<div class="form-group row">
                    <label for="description" class="col-xs-10 col-form-label">News & Events For-</label>
                    <div class="col-xs-10">
                        <input type="checkbox" rows="6" class="btn btn-primary" name="driver"  id="driver" placeholder="Driver"> Driver</input>
                        <input type="checkbox" rows="6" class="btn btn-primary" name="rider"  id="rider" placeholder="Requester">Requester</input>
						@if ($errors->has('driver') || $errors->has('rider'))
							<div >Must be checked either Driver or Requester</div>
						@endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="zipcode" class="col-xs-12 col-form-label"></label>
                    <div class="col-xs-10">
                        <button type="submit" class="btn btn-primary">Add News or Event</button>
                        <a href="{{route('admin.newsandevent.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
