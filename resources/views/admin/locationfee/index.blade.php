@extends('admin.layout.base')

@section('title', 'Location Fees ')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                <h5 class="mb-1">Location Fees</h5>
                <a href="{{ route('admin.locationfee.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Location Fee</a>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Location</th>
                            <th>Fee ({{ currency() }})</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($locations as $index => $location)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$location->location_desc}}</td>
                            <td>{{$location->fee}}</td>
                            <td>
                                <form action="{{ route('admin.locationfee.destroy', $location->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <a href="{{ route('admin.locationfee.edit', $location->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                                    <button class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Location</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection