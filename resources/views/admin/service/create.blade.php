@extends('admin.layout.base')

@section('title', 'Add Service Type ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.service.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Add Service Type</h5>

            <form class="form-horizontal" action="{{route('admin.service.store')}}" method="POST" enctype="multipart/form-data" role="form">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label for="name" class="col-xs-2 col-form-label">Service Name</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('name') }}" name="name" required id="name" placeholder="Service Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="provider_name" class="col-xs-2 col-form-label">Provider Name</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('provider_name') }}" name="provider_name" required id="provider_name" placeholder="Provider Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="picture" class="col-xs-2 col-form-label">Service Image</label>
                    <div class="col-xs-10">
                        <input type="file" accept="image/*" name="image" class="dropify form-control-file" id="picture" aria-describedby="fileHelp">
                    </div>
                </div>

                <div class="form-group row" style="display:none">
                    <label for="fixed" class="col-xs-2 col-form-label">Base Price ({{ $currency }})</label>
                    <div class="col-xs-10">
                        {{-- <input class="form-control" type="text" value="{{ old('fixed') }}" name="fixed" required id="fixed" placeholder="Base Price"> --}}
                        <input class="form-control" type="text" value="0" name="fixed" required id="fixed" placeholder="Base Price">
                    </div>
                </div>

                <div class="form-group row" style="display:none">
                    <label for="distance" class="col-xs-2 col-form-label">Base Distance ({{ $distance }})</label>
                    <div class="col-xs-10">
                        {{-- <input class="form-control" type="text" value="{{ old('distance') }}" name="distance" required id="distance" placeholder="Base Distance"> --}}
                        <input class="form-control" type="text" value="0" name="distance" required id="distance" placeholder="Base Distance">
                    </div>
                </div>

               
                <div class="form-group row">
                    <label for="price" class="col-xs-2 col-form-label">Fare Per Mile (Customer Rate)</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text"  value="{{ old('price') }}" name="price" required id="price" placeholder="Unit Distance Price">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="minute" class="col-xs-2 col-form-label">Fare Per Mile (Driver Rate)</label>
                    <div class="col-xs-10">
                        <input class="form-control" readonly  type="text" value="{{ old('minute') }}" name="minute" required id="minute" placeholder="Unit Distance Price">
                    </div>
                </div>

                <div class="form-group row" style="display:none">
                    <label for="capacity" class="col-xs-2 col-form-label">Shipment Capacity</label>
                    <div class="col-xs-10">
                        {{-- <input class="form-control" type="number" value="{{ old('capacity') }}" name="capacity" required id="capacity" placeholder="Capacity"> --}}
                        <input class="form-control" type="number" value="0" name="capacity" required id="capacity" placeholder="Capacity">
                    </div>
                </div>

                <div class="form-group row" style="display:none">
                    <label for="calculator" class="col-xs-2 col-form-label">Pricing Logic</label>
                    <div class="col-xs-10">
                        <select class="form-control" id="calculator" name="calculator">
                            <option value="MIN">@lang('servicetypes.MIN')</option>
                            <option value="HOUR">@lang('servicetypes.HOUR')</option>
                            <option selected value="DISTANCE">@lang('servicetypes.DISTANCE')</option>
                            <option  value="DISTANCEMIN">@lang('servicetypes.DISTANCEMIN')</option>
                            <option value="DISTANCEHOUR">@lang('servicetypes.DISTANCEHOUR')</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="description" class="col-xs-2 col-form-label">Description</label>
                    <div class="col-xs-10">
                        <textarea class="form-control" type="number" value="{{ old('description') }}" name="description" required id="description" placeholder="Description" rows="4"></textarea>
                    </div>
                </div>

                <div class="form-group row" style="display:none">
                    <label for="insure_price" class="col-xs-2 col-form-label">Safe Shipment Fare</label>
                    <div class="col-xs-10">
                        {{-- <input class="form-control" type="text" value="{{ old('insure_price') }}" name="insure_price" required id="insure_price" placeholder="Safe Shipment Fee"> --}}
                        <input class="form-control" type="text" value="0" name="insure_price" required id="insure_price" placeholder="Safe Shipment Fee">
                    </div>
                </div>
                
                <div class="form-group row" style="display:none">
                    <label for="min_price" class="col-xs-2 col-form-label">Minimum Fare</label>
                    <div class="col-xs-10">
                        {{-- <input class="form-control" type="text" value="{{ old('min_price') }}" name="min_price" required id="min_price" placeholder="Minimum Fare"> --}}
                        <input class="form-control" type="text" value="0" name="min_price" required id="min_price" placeholder="Minimum Fare">
                    </div>
                </div>

                <div class="form-group row" style="display:none">
                    <label for="min_price" class="col-xs-2 col-form-label">Minimum Waiting Time</label>
                    <div class="col-xs-10">
                        {{-- <input class="form-control" type="text" value="{{ old('min_waiting_time') }}" name="min_waiting_time" required id="min_waiting_time" placeholder="Minimum Waiting Time"> --}}
                        <input class="form-control" type="text" value="0" name="min_waiting_time" required id="min_waiting_time" placeholder="Minimum Waiting Time">
                    </div>
                </div>

                <div class="form-group row" style="display:none">
                    <label for="min_price" class="col-xs-2 col-form-label">Waiting Charge Per Minute</label>
                    <div class="col-xs-10">
                        {{-- <input class="form-control" type="text" value="{{ old('min_waiting_charge') }}" name="min_waiting_charge" required id="min_waiting_charge" placeholder="Waiting Charge Per Minutee"> --}}
                        <input class="form-control" type="text" value="0" name="min_waiting_charge" required id="min_waiting_charge" placeholder="Waiting Charge Per Minutee">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="{{ route('admin.service.index') }}" class="btn btn-danger btn-block">Cancel</a>
                            </div>
                            <div class="col-xs-12 col-sm-6 offset-md-6 col-md-3">
                                <button type="submit" class="btn btn-primary btn-block">Add Service Type</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('extra-script')
<script>
jQuery(document).on("change","#price",function(){
 var com="{{ $adminCommition }}";
 if(com!=="" && com!=="0"){
     com=parseFloat(com);
     amt=((parseFloat($("#price").val())*com)/100).toFixed(2);
     amt=(parseFloat($("#price").val())-amt).toFixed(2);
    $("#minute").val(amt);
 }

});
jQuery(document).on("blur","#price",function(){
 var com="{{ $adminCommition }}";
 if(com!=="" && com!=="0"){
     com=parseFloat(com);
     amt=((parseFloat($("#price").val())*com)/100).toFixed(2);
     amt=(parseFloat($("#price").val())-amt).toFixed(2);
    $("#minute").val(amt);
 }

});
</script>
@endsection

