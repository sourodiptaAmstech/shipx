@extends('user.layout.main')

@section('content')
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button> <a class="navbar-brand" href="/"><span class="logo-st">Shipx</span></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="{{ url('/login') }}">Request Now</a>
                    </li>
                    <li>
                        <a href="{{ url('/provider/login') }}">Driver Account</a>
                    </li>
                    <li>
                        <a href="{{ url('/provider/login') }}" class="bold"><span class="drive">BECOME A DRIVER</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container" style="margin-top: 100px;">
        <h1 class="text-center" id="blogHeader" style="margin-bottom: 70px; font-size: 40px; letter-spacing: 1.5px;">Shipx BLOG SPOT!</h1>
        <div class="well">
            <div class="row">
                <div class="col-md-4">
                    <img class="img-responsive post-image" src="#" alt="">
                </div>
                <div class="col-md-8">
                    <h4 class="post-title"><a href="#">Join Us in Saying Thank You
                        </a></h4>
                    <p class="post-meta"> ON <span class="post-date">DECEMBER 20, 2017</span> | By <span class="post-author"><a href="">Teddy</a></span></p>
                    <p>Every year, we receive thousands of stories of incredible drivers going above and beyond for their community. Whether large or small, these acts of kindness have a powerful impact — from saving a life to simply giving someone a reason to smile.

                        This holiday season, we’re showing a small token of appreciation to a few special drivers whose names we’ve heard from passengers. Meet them below.</p>

                </div>
            </div>

        </div>
        <div class="well" style="margin-top: 80px;">
            <div class="row">

                <div class="col-md-8">
                    <h4 class="post-title"><a href="#">Join Us in Saying Thank You
                        </a></h4>
                    <p class="post-meta"> ON <span class="post-date">DECEMBER 20, 2017</span> | By <span class="post-author"><a href="">Teddy</a></span></p>
                    <p>Every year, we receive thousands of stories of incredible drivers going above and beyond for their community. Whether large or small, these acts of kindness have a powerful impact — from saving a life to simply giving someone a reason to smile.

                        This holiday season, we’re showing a small token of appreciation to a few special drivers whose names we’ve heard from passengers. Meet them below.</p>


                </div>
                <div class="col-md-4">
                    <img class="img-responsive post-image" src="#" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="footer">
        <div class="foot-top row m-0">
            <div class="container">
                <div class="col-md-12 text-center">
                    {{-- <a style="margin-right: 10px" href="https://m.facebook.com/"><img src="facebookLg.png"></a>
                    <a style="margin-right: 10px" href="https://twitter.com/"><img src="twit.png"></a>
                    <a href="https://www.instagram.com/"><img src="instagram.png"></a> --}}
                    <a style="margin-right: 10px" href="#"><img src="facebookLg.png"></a>
                    <a style="margin-right: 10px" href="#"><img src="twit.png"></a>
                    <a href="#"><img src="instagram.png"></a>
                </div>

            </div>
        </div>
        <div class=" text-center" style="padding-top: 20px;">
            <div class="container">
                <p > Shipx  2017-All rights reserved</p>
            </div>
        </div>
    </div>

    @endsection