<?php

return array(

    'IOSUser'     => array(
    //   'environment' => 'production',
      // 'certificate' => app_path().'/apns/user/shipx-user-production-no-passphrase.pem',
        'environment' => 'development',
        'certificate' => app_path().'/apns/user/shipx-user-development-no-passphrase.pem',
        'passPhrase'  => '',
        'service'     => 'apns'
    ),
    'IOSProvider' => array(
     //'environment' => 'production',
     //'certificate' => app_path().'/apns/provider/shipx-driver-production-no-passphrase.pem',
      'environment' => 'development',
      'certificate' => app_path().'/apns/provider/shipx-driver-development-no-passphrase.pem',
       'passPhrase'  => '',
        'service'     => 'apns'
    ),
    'AndroidUser' => array(
        'environment' => 'production',
        'apiKey'      => 'AAAAXfkv_4s:APA91bGz3QUG9bgKIlyQ-U_f3W0vfw3pdso6RyXlB0bI-Y-l6kbNLlRU17klLjrHh1vEJM4iqRuq-RpNIcMEBu6IDBdv4qoFMTSvlv1VyZXjCDKhrDzqbLtuWRB7uICEHXoFK6sLJltm',
        'service'     => 'gcm'
    ),
    'AndroidProvider' => array(
        'environment' => 'production',
        'apiKey'      => 'AAAAXfkv_4s:APA91bGz3QUG9bgKIlyQ-U_f3W0vfw3pdso6RyXlB0bI-Y-l6kbNLlRU17klLjrHh1vEJM4iqRuq-RpNIcMEBu6IDBdv4qoFMTSvlv1VyZXjCDKhrDzqbLtuWRB7uICEHXoFK6sLJltm',
        'service'     => 'gcm'
    )

);